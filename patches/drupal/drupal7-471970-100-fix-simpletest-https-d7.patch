diff --git a/modules/simpletest/drupal_web_test_case.php b/modules/simpletest/drupal_web_test_case.php
index 0853c7d..bc7ac9f 100644
--- a/modules/simpletest/drupal_web_test_case.php
+++ b/modules/simpletest/drupal_web_test_case.php
@@ -2024,7 +2024,7 @@ class DrupalWebTestCase extends DrupalTestCase {
         $submit_matches = $this->handleForm($post, $edit, $upload, $ajax ? NULL : $submit, $form);
         $action = isset($form['action']) ? $this->getAbsoluteUrl((string) $form['action']) : $this->getUrl();
         if ($ajax) {
-          $action = $this->getAbsoluteUrl(!empty($submit['path']) ? $submit['path'] : 'system/ajax');
+          $action = $this->getAbsoluteUrl(!empty($submit['path']) ? $submit['path'] : url('system/ajax'));
           // Ajax callbacks verify the triggering element if necessary, so while
           // we may eventually want extra code that verifies it in the
           // handleForm() function, it's not currently a requirement.
@@ -2679,27 +2679,45 @@ class DrupalWebTestCase extends DrupalTestCase {
    *
    * @param $path
    *   A path from the internal browser content.
+   *
    * @return
-   *   The $path with $base_url prepended, if necessary.
+   *   If $path is an absolute URL, it will not be changed. Otherwise, it will
+   *   be converted to an absolute URL within the context of the page the
+   *   internal browser is currently pointed at.
    */
   protected function getAbsoluteUrl($path) {
-    global $base_url, $base_path;
-
     $parts = parse_url($path);
     if (empty($parts['host'])) {
       // Ensure that we have a string (and no xpath object).
       $path = (string) $path;
-      // Strip $base_path, if existent.
-      $length = strlen($base_path);
-      if (substr($path, 0, $length) === $base_path) {
-        $path = substr($path, $length);
-      }
       // Ensure that we have an absolute path.
-      if ($path[0] !== '/') {
-        $path = '/' . $path;
+      if (strpos($path, $GLOBALS['base_path']) !== 0) {
+        $path = $GLOBALS['base_path'] . $path;
+      }
+      // If the internal browser is currently pointed at a particular URL,
+      // strip everything off the end of it until we're left with the
+      // equivalent of Drupal's $base_root global variable (for example,
+      // http://example.com). It is important to derive this from the browser's
+      // URL (rather than using the parent site's $base_root directly) because
+      // the internal browser might be pointed to a different location, e.g.
+      // the https:// version of the site.
+      if ($url = $this->getUrl()) {
+        $parts = parse_url($url);
+        $url_part_prefixes = array('fragment' => '#', 'query' => '?', 'path' => '');
+        foreach ($url_part_prefixes as $part => $prefix) {
+          if (isset($parts[$part])) {
+            $url = substr($url, 0, -strlen($prefix . $parts[$part]));
+          }
+        }
+        $base_root = $url;
+      }
+      // If the internal browser hasn't visited a URL yet, fall back on using
+      // the parent site's $base_root instead.
+      else {
+        $base_root = $GLOBALS['base_root'];
       }
-      // Finally, prepend the $base_url.
-      $path = $base_url . $path;
+      // Finally, prepend the base root to the path to form an absolute URL.
+      $path = $base_root . $path;
     }
     return $path;
   }
@@ -2846,10 +2864,18 @@ class DrupalWebTestCase extends DrupalTestCase {
    * A good example would be when testing drupal_http_request(). After fetching
    * the page the content can be set and page elements can be checked to ensure
    * that the function worked properly.
+   *
+   * @param $content
+   *   A string representing the raw HTML content to set.
+   * @param $url
+   *   (optional) The URL to set as the internal browser URL associated with
+   *   this content. If not provided, the browser URL will not be changed.
    */
-  protected function drupalSetContent($content, $url = 'internal:') {
+  protected function drupalSetContent($content, $url = NULL) {
     $this->content = $content;
-    $this->url = $url;
+    if (isset($url)) {
+      $this->url = $url;
+    }
     $this->plainTextContent = FALSE;
     $this->elements = FALSE;
     $this->drupalSettings = array();
diff --git a/modules/simpletest/simpletest.test b/modules/simpletest/simpletest.test
index dde162e..f06b0eb 100644
--- a/modules/simpletest/simpletest.test
+++ b/modules/simpletest/simpletest.test
@@ -322,6 +322,16 @@ class SimpleTestFunctionalTest extends DrupalWebTestCase {
  * Test internal testing framework browser.
  */
 class SimpleTestBrowserTestCase extends DrupalWebTestCase {
+  /**
+   * Stores the value of $GLOBALS['base_root'] before the test modifies it.
+   */
+  protected $base_root;
+
+  /**
+   * Stores the value of $GLOBALS['base_url'] before the test modifies it.
+   */
+  protected $base_url;
+
   public static function getInfo() {
     return array(
       'name' => 'SimpleTest browser',
@@ -333,30 +343,62 @@ class SimpleTestBrowserTestCase extends DrupalWebTestCase {
   function setUp() {
     parent::setUp();
     variable_set('user_register', USER_REGISTER_VISITORS);
+    // Store the original values of the global $base_root and $base_url
+    // variables so we can restore them after they are modified by the test.
+    $this->base_root = $GLOBALS['base_root'];
+    $this->base_url = $GLOBALS['base_url'];
+  }
+
+  function tearDown() {
+    // Restore the original values of the global $base_root and $base_url
+    // variables.
+    $GLOBALS['base_root'] = $this->base_root;
+    $GLOBALS['base_url'] = $this->base_url;
+    parent::tearDown();
   }
 
   /**
    * Test DrupalWebTestCase::getAbsoluteUrl().
    */
   function testGetAbsoluteUrl() {
-    // Testbed runs with Clean URLs disabled, so disable it here.
-    variable_set('clean_url', 0);
-    $url = 'user/login';
-
-    $this->drupalGet($url);
-    $absolute = url($url, array('absolute' => TRUE));
-    $this->assertEqual($absolute, $this->url, 'Passed and requested URL are equal.');
-    $this->assertEqual($this->url, $this->getAbsoluteUrl($this->url), 'Requested and returned absolute URL are equal.');
-
-    $this->drupalPost(NULL, array(), t('Log in'));
-    $this->assertEqual($absolute, $this->url, 'Passed and requested URL are equal.');
-    $this->assertEqual($this->url, $this->getAbsoluteUrl($this->url), 'Requested and returned absolute URL are equal.');
-
-    $this->clickLink('Create new account');
-    $url = 'user/register';
-    $absolute = url($url, array('absolute' => TRUE));
-    $this->assertEqual($absolute, $this->url, 'Passed and requested URL are equal.');
-    $this->assertEqual($this->url, $this->getAbsoluteUrl($this->url), 'Requested and returned absolute URL are equal.');
+    // Store the URLs of the pages we will direct the internal browser to visit
+    // during this test. Force them to be http:// URLs regardless of the
+    // environment the test is running in.
+    variable_set('https', TRUE);
+    $user_login_absolute_url = url('user/login', array('absolute' => TRUE, 'https' => FALSE));
+    $user_register_absolute_url = url('user/register', array('absolute' => TRUE, 'https' => FALSE));
+
+    // Test that the internal browser always stores the correct URLs for the
+    // location it is pointed at. For example, the URLs should use http:// if
+    // the internal browser is visiting an http:// version of the site,
+    // regardless of whether the parent site (that is running the tests) is on
+    // http:// or https://. This behavior is tested here by modifying the
+    // global $base_root and $base_url variables on the parent site and
+    // verifying that this does not affect the URLs seen by the internal
+    // browser.
+    foreach (array(TRUE, FALSE) as $https_on) {
+      // Simulate the parent site as either http:// or https://.
+      $scheme = $https_on ? 'https' : 'http';
+      $GLOBALS['base_root'] = preg_replace('@^https?://@', "$scheme://", $GLOBALS['base_root']);
+      $GLOBALS['base_url'] = preg_replace('@^https?://@', "$scheme://", $GLOBALS['base_url']);
+
+      // Visit the http:// version of the user/login page and verify that's
+      // where the browser thinks it's at.
+      $this->drupalGet('user/login', array('https' => FALSE));
+      $this->assertEqual($user_login_absolute_url, $this->url, t('Passed and requested URL are equal.'));
+      $this->assertEqual($this->url, $this->getAbsoluteUrl($this->url), t('Requested and returned absolute URL are equal.'));
+
+      // Submit a form and verify that we remain on the correct http:// URL.
+      $this->drupalPost(NULL, array(), t('Log in'));
+      $this->assertEqual($user_login_absolute_url, $this->url, t('Passed and requested URL are equal.'));
+      $this->assertEqual($this->url, $this->getAbsoluteUrl($this->url), t('Requested and returned absolute URL are equal.'));
+
+      // Click a link to go to user/register, and verify that we are on the
+      // correct http:// URL there too.
+      $this->clickLink('Create new account');
+      $this->assertEqual($user_register_absolute_url, $this->url, t('Passed and requested URL are equal.'));
+      $this->assertEqual($this->url, $this->getAbsoluteUrl($this->url), t('Requested and returned absolute URL are equal.'));
+    }
   }
 
   /**
diff --git a/modules/simpletest/tests/ajax.test b/modules/simpletest/tests/ajax.test
index 664d520..0e7913a 100644
--- a/modules/simpletest/tests/ajax.test
+++ b/modules/simpletest/tests/ajax.test
@@ -488,7 +488,7 @@ class AJAXMultiFormTestCase extends AJAXTestCase {
     // page update, ensure the same as above.
     foreach ($field_xpaths as $form_html_id => $field_xpath) {
       for ($i = 0; $i < 2; $i++) {
-        $this->drupalPostAJAX(NULL, array(), array($button_name => $button_value), 'system/ajax', array(), array(), $form_html_id);
+        $this->drupalPostAJAX(NULL, array(), array($button_name => $button_value), url('system/ajax'), array(), array(), $form_html_id);
         $this->assert(count($this->xpath($field_xpath . $field_items_xpath_suffix)) == $i+2, t('Found the correct number of field items after an AJAX submission.'));
         $this->assertFieldByXPath($field_xpath . $button_xpath_suffix, NULL, t('Found the "add more" button after an AJAX submission.'));
         $this->assertNoDuplicateIds(t('Updated page contains unique IDs'), 'Other');
