server {
	server_name dev.online.unitedway.org;
	server_name staging.online.unitedway.org;
        listen 8080;
	rewrite ^/(.*) http://online.unitedway.org/$1 permanent;
}


server {
	server_name online.unitedway.org;
        server_name 66.111.111.180;
        server_name localhost;
	server_name 127.0.0.1;
	server_name _;
	listen 8080;

	port_in_redirect off;

	# global includes
	include conf.d/global.inc;

        #root /var/www/vhosts/uwonline.live/public; ## <-- Your only path reference.
        root /var/www/html;
	access_log  /var/log/nginx/uwlive.log  main;
        error_log  /var/log/nginx/uwlive.error.log error;

        index index.html index.php;
 
        location = /favicon.ico {
                log_not_found off;
                access_log off;
        }
 
        location = /robots.txt {
                allow all;
                log_not_found off;
                access_log off;
        }
 
        # This matters if you use drush
        location = /backup {
                deny all;
        }
 
        # Very rarely should these ever be accessed outside of your lan
        location ~* \.(txt|log)$ {
                allow 192.168.0.0/16;
                deny all;
        }
	 
	# Support for http://drupal.org/project/js
	location ^~ /js/ {
	  location ~* ^/js/ {
	    rewrite ^/(.*)$ /js.php?q=$1 last;
	  }
	}

        location ~ ^/simplesaml(/.*)? {
		alias /var/www/current/simplesamlphp/www/;
		index index.php;


		location ~ /simplesaml/(.*\.php)$ {
			#root /var/www/current/simplesamlphp/www;
			fastcgi_split_path_info ^(.+\.php)(/.+)$;
			fastcgi_param PATH_INFO       $fastcgi_path_info;
			fastcgi_param SCRIPT_FILENAME $document_root$1;
			#NOTE: You should have "cgi.fix_pathinfo = 0;" in php.ini
			include fastcgi_params;
			fastcgi_intercept_errors on;
			fastcgi_pass unix:/var/run/php-fpm/uwstage.sock;
			fastcgi_read_timeout 600;
		}

		location ~ ^/simplesaml/(.*) {
		  alias /var/www/current/simplesamlphp/www/$1;
		}
	
        }
         
        location / {
                # This is cool because no php is touched for static content
                try_files $uri @rewrite;
        }
        
        location ~ \..*/.*\.php$ {
                return 403;
        }
 
        location @rewrite {
                # Some modules enforce no slash (/) at the end of the URL
                # Else this rewrite block wouldn't be needed (GlobalRedirect)
                rewrite ^/(.*)$ /index.php?q=$1;
        }
 
        location ~ \.php$ {
                fastcgi_split_path_info ^(.+\.php)(/.+)$;
                #NOTE: You should have "cgi.fix_pathinfo = 0;" in php.ini
                include fastcgi_params;
                fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
                fastcgi_intercept_errors on;
                fastcgi_pass unix:/var/run/php-fpm/uwstage.sock;
		fastcgi_read_timeout 1200;
        }
 
        # Fighting with ImageCache? This little gem is amazing.
        location ~ ^/sites/.*/files/imagecache/ {
                try_files $uri @rewrite;
        }
        # Catch image styles for D7 too.
        location ~ ^/sites/.*/files/styles/ {
                try_files $uri @rewrite;
        }
 
        location ~* \.(js|css|png|jpg|jpeg|gif|ico)$ {
                expires max;
                log_not_found off;
        }
}
