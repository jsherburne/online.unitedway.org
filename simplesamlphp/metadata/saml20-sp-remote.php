<?php
/**
 * SAML 2.0 remote SP metadata for simpleSAMLphp.
 *
 * See: https://simplesamlphp.org/docs/stable/simplesamlphp-reference-sp-remote
 */

/*
 * Example simpleSAMLphp SAML 2.0 SP
 */
$metadata['https://uww.saepio.com/marketport/saml/metadata/alias/saepioalias'] = array (
  'entityid' => 'https://uww.saepio.com/marketport/saml/metadata/alias/saepioalias',
  'contacts' => 
  array (
  ),
  'metadata-set' => 'saml20-sp-remote',
  'AssertionConsumerService' => 
  array (
    0 => 
    array (
      'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Artifact',
      'Location' => 'https://uww.saepio.com/marketport/saml/sso/alias/saepioalias',
      'index' => 0,
    ),
    1 => 
    array (
      'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST',
      'Location' => 'https://uww.saepio.com/marketport/saml/sso/alias/saepioalias',
      'index' => 1,
      'isDefault' => true,
    ),
    2 => 
    array (
      'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:PAOS',
      'Location' => 'https://uww.saepio.com/marketport/saml/sso/alias/saepioalias',
      'index' => 2,
    ),
    3 => 
    array (
      'Binding' => 'urn:oasis:names:tc:SAML:2.0:profiles:holder-of-key:SSO:browser',
      'Location' => 'https://uww.saepio.com/marketport/saml/HoKSSO/alias/saepioalias',
      'index' => 3,
    ),
    4 => 
    array (
      'Binding' => 'urn:oasis:names:tc:SAML:2.0:profiles:holder-of-key:SSO:browser',
      'Location' => 'https://uww.saepio.com/marketport/saml/HoKSSO/alias/saepioalias',
      'index' => 4,
    ),
  ),
  'SingleLogoutService' => 
  array (
    0 => 
    array (
      'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST',
      'Location' => 'https://uww.saepio.com/marketport/saml/SingleLogout/alias/saepioalias',
    ),
    1 => 
    array (
      'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect',
      'Location' => 'https://uww.saepio.com/marketport/saml/SingleLogout/alias/saepioalias',
    ),
    2 => 
    array (
      'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:SOAP',
      'Location' => 'https://uww.saepio.com/marketport/saml/SingleLogout/alias/saepioalias',
    ),
  ),
  'NameIDFormat' => 'urn:oasis:names:tc:SAML:1.1:nameid-format:emailAddress',
  'simplesaml.nameidattribute' => 'username',
  'keys' => 
  array (
    0 => 
    array (
      'encryption' => false,
      'signing' => true,
      'type' => 'X509Certificate',
      'X509Certificate' => 'MIIEnDCCA4SgAwIBAgIQA0CsNizcGNNdC1H4Rth6vDANBgkqhkiG9w0BAQUFADA8MQswCQYDVQQG
EwJVUzEVMBMGA1UEChMMVGhhd3RlLCBJbmMuMRYwFAYDVQQDEw1UaGF3dGUgU1NMIENBMB4XDTE0
MDQxNTAwMDAwMFoXDTE2MDcxNDIzNTk1OVowcTELMAkGA1UEBhMCVVMxETAPBgNVBAgTCE1pc3Nv
dXJpMRQwEgYDVQQHFAtLYW5zYXMgQ2l0eTEiMCAGA1UEChQZU2FlcGlvIFRlY2hub2xvZ2llcywg
SW5jLjEVMBMGA1UEAxQMKi5zYWVwaW8uY29tMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKC
AQEAp/V8sNGB3RwQlNhen8oVURpNsNeDCN4DlmJTGRnCQIAmoM7gyn0UvnDhSnQI5fYcHLPtNrBy
XVQ1kA8lPlJPp1g1F3n+Ur9968ARNedVYutKDrhmPXeBIapkWZ904xybzAUS70J7Wx8992R2C0/n
FB7rgM0m23k/isst/ePu9k3IIuGgGlN4oDIlLebg8BUPMHERkS9q7R8R58xQy3VuUpUofbcKOzT9
HRBadbMXzSUy0GaE6l7wBhx55jworvt171pULPycuevHm03mfy1KseO59TK7AXQ0S0ka/ZO2F6gl
jfyx2FQYUQ15oq0TKvx9BjLB41rbCt6Blr5jsozMZwIDAQABo4IBYzCCAV8wFwYDVR0RBBAwDoIM
Ki5zYWVwaW8uY29tMAkGA1UdEwQCMAAwQgYDVR0gBDswOTA3BgpghkgBhvhFAQc2MCkwJwYIKwYB
BQUHAgEWG2h0dHBzOi8vd3d3LnRoYXd0ZS5jb20vY3BzLzAOBgNVHQ8BAf8EBAMCBaAwHwYDVR0j
BBgwFoAUp6KDuzRFQD381TBPErk+oQGf9tswOgYDVR0fBDMwMTAvoC2gK4YpaHR0cDovL3N2ci1v
di1jcmwudGhhd3RlLmNvbS9UaGF3dGVPVi5jcmwwHQYDVR0lBBYwFAYIKwYBBQUHAwEGCCsGAQUF
BwMCMGkGCCsGAQUFBwEBBF0wWzAiBggrBgEFBQcwAYYWaHR0cDovL29jc3AudGhhd3RlLmNvbTA1
BggrBgEFBQcwAoYpaHR0cDovL3N2ci1vdi1haWEudGhhd3RlLmNvbS9UaGF3dGVPVi5jZXIwDQYJ
KoZIhvcNAQEFBQADggEBAAmzN26LN4spYi0/bPZGIQk86DoIk5QGsARaKVlnkddvNqO4+C4L30F6
OFHPYrD8+7V/cvVYCMx6O+asikqBoUQi3el+lo5mCRH0CQqqWnUc5fI7lCAwuPvpusgIbQZ2MPjO
Hh4fJAFbPgxT0C54OaVpyYFfQZLuVkAmpTVefj02syvPxlyqSl0fbuYvhdk2WCrjLE9ICisQ5bIf
F+SFQaKJXrOiL18QxvoMd7m3ff2KB+7tgUzBmKxL3wqxIwko6tqhGuvU3zqUvWCFOmnXQmF7RjE8
U4cibZ7d+sMvQXdQymmZe+UK+jFkwgD1G5jMhmLUaRsLxl4qxbn3PYq9o6Y=',
    ),
    1 => 
    array (
      'encryption' => true,
      'signing' => false,
      'type' => 'X509Certificate',
      'X509Certificate' => 'MIIEnDCCA4SgAwIBAgIQA0CsNizcGNNdC1H4Rth6vDANBgkqhkiG9w0BAQUFADA8MQswCQYDVQQG
EwJVUzEVMBMGA1UEChMMVGhhd3RlLCBJbmMuMRYwFAYDVQQDEw1UaGF3dGUgU1NMIENBMB4XDTE0
MDQxNTAwMDAwMFoXDTE2MDcxNDIzNTk1OVowcTELMAkGA1UEBhMCVVMxETAPBgNVBAgTCE1pc3Nv
dXJpMRQwEgYDVQQHFAtLYW5zYXMgQ2l0eTEiMCAGA1UEChQZU2FlcGlvIFRlY2hub2xvZ2llcywg
SW5jLjEVMBMGA1UEAxQMKi5zYWVwaW8uY29tMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKC
AQEAp/V8sNGB3RwQlNhen8oVURpNsNeDCN4DlmJTGRnCQIAmoM7gyn0UvnDhSnQI5fYcHLPtNrBy
XVQ1kA8lPlJPp1g1F3n+Ur9968ARNedVYutKDrhmPXeBIapkWZ904xybzAUS70J7Wx8992R2C0/n
FB7rgM0m23k/isst/ePu9k3IIuGgGlN4oDIlLebg8BUPMHERkS9q7R8R58xQy3VuUpUofbcKOzT9
HRBadbMXzSUy0GaE6l7wBhx55jworvt171pULPycuevHm03mfy1KseO59TK7AXQ0S0ka/ZO2F6gl
jfyx2FQYUQ15oq0TKvx9BjLB41rbCt6Blr5jsozMZwIDAQABo4IBYzCCAV8wFwYDVR0RBBAwDoIM
Ki5zYWVwaW8uY29tMAkGA1UdEwQCMAAwQgYDVR0gBDswOTA3BgpghkgBhvhFAQc2MCkwJwYIKwYB
BQUHAgEWG2h0dHBzOi8vd3d3LnRoYXd0ZS5jb20vY3BzLzAOBgNVHQ8BAf8EBAMCBaAwHwYDVR0j
BBgwFoAUp6KDuzRFQD381TBPErk+oQGf9tswOgYDVR0fBDMwMTAvoC2gK4YpaHR0cDovL3N2ci1v
di1jcmwudGhhd3RlLmNvbS9UaGF3dGVPVi5jcmwwHQYDVR0lBBYwFAYIKwYBBQUHAwEGCCsGAQUF
BwMCMGkGCCsGAQUFBwEBBF0wWzAiBggrBgEFBQcwAYYWaHR0cDovL29jc3AudGhhd3RlLmNvbTA1
BggrBgEFBQcwAoYpaHR0cDovL3N2ci1vdi1haWEudGhhd3RlLmNvbS9UaGF3dGVPVi5jZXIwDQYJ
KoZIhvcNAQEFBQADggEBAAmzN26LN4spYi0/bPZGIQk86DoIk5QGsARaKVlnkddvNqO4+C4L30F6
OFHPYrD8+7V/cvVYCMx6O+asikqBoUQi3el+lo5mCRH0CQqqWnUc5fI7lCAwuPvpusgIbQZ2MPjO
Hh4fJAFbPgxT0C54OaVpyYFfQZLuVkAmpTVefj02syvPxlyqSl0fbuYvhdk2WCrjLE9ICisQ5bIf
F+SFQaKJXrOiL18QxvoMd7m3ff2KB+7tgUzBmKxL3wqxIwko6tqhGuvU3zqUvWCFOmnXQmF7RjE8
U4cibZ7d+sMvQXdQymmZe+UK+jFkwgD1G5jMhmLUaRsLxl4qxbn3PYq9o6Y=',
    ),
  ),
  'validate.authnrequest' => true,
  'saml20.sign.assertion' => true,
);

/*
 * Saepio SAML 2.0 SP
 *//*
$metadata['https://saml2sp.example.org'] = array(
    'AssertionConsumerService' => 'https://saml2sp.example.org/simplesaml/module.php/saml/sp/saml2-acs.php/default-sp',
    'SingleLogoutService' => 'https://saml2sp.example.org/simplesaml/module.php/saml/sp/saml2-logout.php/default-sp',
);*/

/*
 * This example shows an example config that works with Google Apps for education.
 * What is important is that you have an attribute in your IdP that maps to the local part of the email address
 * at Google Apps. In example, if your google account is foo.com, and you have a user that has an email john@foo.com, then you
 * must set the simplesaml.nameidattribute to be the name of an attribute that for this user has the value of 'john'.
 */
$metadata['google.com'] = array(
	'AssertionConsumerService' => 'https://www.google.com/a/g.feide.no/acs',
	'NameIDFormat' => 'urn:oasis:names:tc:SAML:1.1:nameid-format:emailAddress',
	'simplesaml.nameidattribute' => 'uid',
	'simplesaml.attributes' => FALSE,
);
