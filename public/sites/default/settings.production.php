<?php
/**
 * Some distributions of Linux (most notably Debian) ship their PHP
 * installations with garbage collection (gc) disabled. Since Drupal depends on
 * PHP's garbage collection for clearing sessions, ensure that garbage
 * collection occurs by using the most common settings.
 */
ini_set('session.gc_probability', 1);
ini_set('session.gc_divisor', 100);

/**
 * Set session lifetime (in seconds), i.e. the time from the user's last visit
 * to the active session may be deleted by the session garbage collector. When
 * a session is deleted, authenticated users are logged out, and the contents
 * of the user's $_SESSION variable is discarded.
*/
ini_set('session.gc_maxlifetime', 200000);

/**
 * Set session cookie lifetime (in seconds), i.e. the time from the session is
 * created to the cookie expires, i.e. when the browser is expected to discard
 * the cookie. The value 0 means "until the browser is closed".
*/
ini_set('session.cookie_lifetime', 2000000);

$databases['default']['default'] = array(
  'driver' => 'mysql',
  'database' => 'uwonline2',
  'username' => 'uw_online',
  'password' => 'Vv938Pv574',
  'host' => '66.111.111.186',
  'port' => '3306',
  'prefix' => '',
);

$conf['search_api_solr_overrides'] = array(
  'default' => array( //name of the server, often 'default' for the first server
    'name' => t('Solr Server (Overridden)'),
    'options' => array(
      //'host' => '127.0.0.1',
      'host' => '66.111.111.180',
      'port' => 8984,
      'path' => '/solr/uwstage',
    ),
  ),
);

$base_url = 'https://online.unitedway.org';

// Private, tmp filesystems
$conf['file_private_path'] = '/var/www/private';
$conf['file_temporary_path'] = '/tmp';

ini_set('memory_limit', '300M');

$cookie_domain = '.online.unitedway.org';
$conf['services_sso_server_helper_cookie_domain'] = $cookie_domain; // For services sso helper module

$_SERVER['HTTPS'] = 'on';
$conf['https'] = TRUE;

$conf['securepages_basepath'] = 'http://online.unitedway.org';
$conf['securepages_basepath_ssl'] = 'https://online.unitedway.org';


// Salesforce LIVE
$conf['salesforce_consumer_key'] = '3MVG9WQsPp5nH_ErJDip.6X2n589XN2.RlpWY2me.R6kawFDYtH.rtdkchAoUFlafMT70vrKHKqRQ.JuG3Chh';
$conf['salesforce_consumer_secret'] = '743773375765443196';
$conf['salesforce_endpoint'] = 'https://login.salesforce.com';

/**
 * Caching configuration
 */
// Enable Drupal page cache, set lifetime, max age, and compression.
$conf['cache'] = '1';

// Cache lifetimes maxed out; Expire module will expire individual nodes/URLS sooner as necessary.
$conf['cache_lifetime'] = '86400';
$conf['page_cache_maximum_age'] = '86400';
$conf['page_compression'] = '1';


// Don't bootstrap anonymous users.
$conf['page_cache_invoke_hooks'] = FALSE;

// We are behind a reverse proxy.
$conf['reverse_proxy'] = TRUE;
$conf['reverse_proxy_addresses'] = array('127.0.0.1', '66.111.111.180');

// Varnish specific.
$conf['varnish_control_terminal'] = '66.111.111.180:6082';
$conf['varnish_control_key'] = 'NS{vld00d';

$conf['varnish_version'] = '3';
$conf['varnish_flush_cron'] = '0';
$conf['varnish_cache_clear'] = '2';

/**
 * Backends and bins.
 */

// Cache backends.
$conf['cache_backends'][] = 'sites/all/modules/contrib/memcache/memcache.inc';
$conf['cache_backends'][] = 'sites/all/modules/contrib/varnish/varnish.cache.inc';

// Memcache server specified by IP so it works from any machine in the cluster.
$conf['memcache_servers'] = array('66.111.111.180:11211' => 'default');

// Cache Bins.
$conf['cache_default_class'] = 'MemCacheDrupal'; // Default bin.
$conf['cache_class_cache_page'] = 'VarnishCache'; // Varnish gets the page cache
$conf['cache_class_external_varnish_page'] = 'VarnishCache';  // for Expire module support.
$conf['cache_class_cache_form'] = 'DrupalDatabaseCache'; // Form cache must be non-volatile.


/**
 * Include UW specific redirects.
 */
@include('inc/settings.redirects.php');