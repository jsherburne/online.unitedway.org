<?php

/**
 * UWC specific redirect conditions.
 * If extension is "cfm" and query string contains a "FID" or "AID" value,
 */
if (isset($_SERVER['REQUEST_URI']) &&
  isset($_SERVER['QUERY_STRING'])) {
  $request_uri = explode('?', $_SERVER['REQUEST_URI']);
  if (preg_match('/\.cfm$/i', array_shift($request_uri)) &&
    preg_match('/(FID\=[\d]+)|(AID\=[\d]+)/i', $_SERVER['QUERY_STRING'])) {
    $uwc_query_string = $_SERVER['QUERY_STRING'];
    if (!$uwc_new_url = preg_filter('/(.*[\?|\&]?)(FID)\=([\d]+)(.*)/i','uwc_redirects/$2/$3/', $uwc_query_string))
    {
      $uwc_new_url = preg_filter('/(.*[\?|\&]?)(AID)\=([\d]+)(.*)/i','uwc_redirects/$2/$3/', $uwc_query_string);
    }
    if ($uwc_new_url) {
      header('HTTP/1.0 301 Moved Permanently');
      header('Location: /'. $uwc_new_url ); exit();
    }
  }
}