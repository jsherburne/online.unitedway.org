<?php
$databases['default']['default'] = array(
  'driver' => 'mysql',
  'database' => 'web',
  'username' => 'web',
  'password' => 'web',
  'host' => 'localhost',
);

/**
 * Solr Search API overrides
 *
 * Configuration setting per Solr instance
 */
$conf['search_api_solr_overrides'] = array(
  'solr_server' => array(
    'name' => t('Solr Server (Overridden)'),
    'options' => array(
      'host' => 'localhost',
      'port' => 8983,
      'path' => '/solr/drupal'
    )
  )
);

$base_url = 'http://localhost:8080';

$conf['https'] = FALSE;
$conf['securepages_enable'] = FALSE;

/**
 * Disable asset compression during development.
 */
$conf['css_gzip_compression'] = FALSE;
$conf['js_gzip_compression'] = FALSE;

// Disable drupal's built in cron trigger during development. There's a big feed we don't want to import!
$conf['cron_safe_threshold'] = 0;

$conf['mail_system'] = array( 'default-system' => 'DevelMailLog', );

// Enable xhprof
$conf['xhprof_enabled'] = 1;

/**
 * Caching configuration
 */
// Enable Drupal page cache, set lifetime, max age, and compression.
$conf['cache'] = '1';

// Cache lifetimes maxed out; Expire module will expire individual nodes/URLS sooner as necessary.
$conf['cache_lifetime'] = '86400';
$conf['page_cache_maximum_age'] = '86400';
$conf['page_compression'] = '1';


// Don't bootstrap anonymous users.
$conf['page_cache_invoke_hooks'] = FALSE;
$conf['page_cache_without_database'] = TRUE;

// We are behind a reverse proxy.
$conf['reverse_proxy'] = TRUE;
$conf['reverse_proxy_addresses'] = array('127.0.0.1');

// Varnish specific.
$conf['varnish_control_terminal'] = '127.0.0.1:6082';
$conf['varnish_control_key'] = '3411e44a-f791-4f66-a17e-72a734bf8ff9';
$conf['varnish_version'] = '3';
$conf['varnish_flush_cron'] = '0';
$conf['varnish_cache_clear'] = '2';

/**
 * Backends and bins.
 */

// Cache backends.
$conf['cache_backends'][] = 'sites/all/modules/contrib/memcache/memcache.inc';
$conf['cache_backends'][] = 'sites/all/modules/contrib/varnish/varnish.cache.inc';

// Memcache server specified by IP so it works from any machine in the cluster.
$conf['memcache_servers'] = array('127.0.0.1:11211' => 'default');

// Cache Bins.
$conf['cache_default_class'] = 'MemCacheDrupal'; // Default bin.
$conf['cache_class_cache_page'] = 'VarnishCache'; // Varnish gets the page cache
$conf['cache_class_external_varnish_page'] = 'VarnishCache';  // for Expire module support.
$conf['cache_class_cache_form'] = 'DrupalDatabaseCache'; // Form cache must be non-volatile.

/**
 * Include UW specific redirects.
 */
@include('inc/settings.redirects.php');


// Allow optional settings.local.php file
if (file_exists('./' . conf_path() . '/settings.local.php')) {
  require './' . conf_path() . '/settings.local.php';
}