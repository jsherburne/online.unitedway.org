<?php

// Database has the same credentials as prod, but on a different host.
$databases['default']['default'] = array(
  'driver' => 'mysql',
  'database' => 'uwonline2',
  'username' => 'uw_online',
  'password' => 'Vv938Pv574',
  'host' => '66.111.111.188',
  'port' => '3306',
  'prefix' => '',
);

$conf['search_api_solr_overrides'] = array(
  'default' => array( //name of the server, often 'default' for the first server
    'name' => t('Solr Server (Overridden)'),
    'options' => array(
      'host' => '127.0.0.1',
      'port' => 8984,
      'path' => '/solr/uwstage',
    ),
  ),
);

$base_url = 'https://staging2.online.unitedway.org';

// Private, tmp filesystems
$conf['file_private_path'] = '/var/www/private';
$conf['file_temporary_path'] = '/tmp';

$cookie_domain = '.staging2.online.unitedway.org';
$conf['services_sso_server_helper_cookie_domain'] = $cookie_domain; // For services sso helper module

$_SERVER['HTTPS'] = 'on';
$conf['https'] = TRUE;

$conf['securepages_basepath'] = 'http://staging2.online.unitedway.org';
$conf['securepages_basepath_ssl'] = 'https://staging2.online.unitedway.org';

// Disable drupal's built in cron trigger during development. There's a big feed we don't want to import!
$conf['cron_safe_threshold'] = 0;

/**
 * Caching configuration
 */
// Enable Drupal page cache, set lifetime, max age, and compression.
$conf['cache'] = '1';

// Cache lifetimes maxed out; Expire module will expire individual nodes/URLS sooner as necessary.
$conf['cache_lifetime'] = '86400';
$conf['page_cache_maximum_age'] = '86400';
$conf['page_compression'] = '1';


// Don't bootstrap anonymous users.
$conf['page_cache_invoke_hooks'] = FALSE;

// We are behind a reverse proxy.
$conf['reverse_proxy'] = TRUE;
$conf['reverse_proxy_addresses'] = array('127.0.0.1', '66.111.111.182');

// Varnish specific.
$conf['varnish_control_terminal'] = '66.111.111.182:6082';
$conf['varnish_control_key'] = 'NS{vld00d';

$conf['varnish_version'] = '3';
$conf['varnish_flush_cron'] = '0';
$conf['varnish_cache_clear'] = '2';

/**
 * Backends and bins.
 */

// Cache backends.
$conf['cache_backends'][] = 'sites/all/modules/contrib/memcache/memcache.inc';
$conf['cache_backends'][] = 'sites/all/modules/contrib/varnish/varnish.cache.inc';

// Memcache server specified by IP so it works from any machine in the cluster.
$conf['memcache_servers'] = array('66.111.111.182:11211' => 'default');

// Cache Bins.
$conf['cache_default_class'] = 'MemCacheDrupal'; // Default bin.
$conf['cache_class_cache_page'] = 'VarnishCache'; // Varnish gets the page cache
$conf['cache_class_external_varnish_page'] = 'VarnishCache';  // for Expire module support.
$conf['cache_class_cache_form'] = 'DrupalDatabaseCache'; // Form cache must be non-volatile.

/**
 * Include UW specific redirects.
 */
@include('inc/settings.redirects.php');