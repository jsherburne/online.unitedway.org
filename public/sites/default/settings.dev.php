<?php
$databases['default']['default'] = array(
  'driver' => 'mysql',
  'database' => 'uwonline',
  'username' => 'uwonline',
  'password' => 'v0w3gT1S8S',
  'host' => 'db1.dev.forumone.com',
);

$conf['search_api_solr_overrides'] = array(
  'default' => array( //name of the server, often 'default' for the first server
    'name' => t('Solr Server (Dev)'),
    'options' => array(
      'host' => 'solr1.dev.forumone.com',
      'port' => 8984,
      'path' => '/solr/uwonline',
    ),
  ),
);

$base_url = 'https://uwonline.dev.forumone.com';

ini_set('memory_limit', '1024M');

$conf['https'] = TRUE;

// Disable drupal's built in cron trigger during development. There's a big feed we don't want to import!
$conf['cron_safe_threshold'] = 0;

/**
 * Disable asset compression during development.
 */
$conf['css_gzip_compression'] = FALSE;
$conf['js_gzip_compression'] = FALSE;

/**
 * Include UW specific redirects.
 */
@include('inc/settings.redirects.php');
