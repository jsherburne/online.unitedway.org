<?php
$aliases['uwo.dev'] = array(
  'uri' => 'uwonline.dev.forumone.com',
  'root' => '/var/www/vhosts/uwonline.dev/public',
  'remote-host' => 'dev.forumone.com',
  'remote-user' => 'uwonline',
);

$aliases['uwo.stage'] = array(
  'uri' => 'staging2.online.unitedway.org',
  'root' => '/var/www/html',
  'remote-host' => 'staging2.online.unitedway.org',
  'remote-user' => 'deploy',
);

$aliases['uwo.live'] = array(
  'uri' => 'online.unitedway.org',
  'root' => '/var/www/html',
  'remote-host' => 'online.unitedway.org',
  'remote-user' => 'deploy',
);
$aliases['uwo.local'] = array(
  'uri' => 'http://10.11.12.14',
  'root' => '/vagrant/public',
  'remote-host' => '10.11.12.14',
  'remote-user' => 'vagrant',
  'ssh-options' => "-i ~/.vagrant.d/insecure_private_key -l vagrant",
  'databases' => array (
    'default' => array (
      'default' => array (
        'database' => 'web',
        'username' => 'web',
        'password' => 'web',
        'host' => '10.11.12.14',
        'port' => '',
        'driver' => 'mysql',
        'prefix' => '',
      ),
    ),
  ),
);
