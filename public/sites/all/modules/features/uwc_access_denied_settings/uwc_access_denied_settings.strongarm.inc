<?php
/**
 * @file
 * uwc_access_denied_settings.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function uwc_access_denied_settings_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'r4032login_access_denied_message';
  $strongarm->value = 'You must log in to use UW Online. If you have received this message in error, <a href="/sessionfix">click here</a> and try again';
  $export['r4032login_access_denied_message'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'r4032login_display_denied_message';
  $strongarm->value = 0;
  $export['r4032login_display_denied_message'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'r4032login_redirect_authenticated_users_to';
  $strongarm->value = 'access-denied';
  $export['r4032login_redirect_authenticated_users_to'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'r4032login_user_login_path';
  $strongarm->value = 'user/login';
  $export['r4032login_user_login_path'] = $strongarm;

  return $export;
}
