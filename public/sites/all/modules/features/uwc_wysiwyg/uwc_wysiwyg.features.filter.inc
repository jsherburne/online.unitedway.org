<?php
/**
 * @file
 * uwc_wysiwyg.features.filter.inc
 */

/**
 * Implements hook_filter_default_formats().
 */
function uwc_wysiwyg_filter_default_formats() {
  $formats = array();

  // Exported format: Filtered HTML.
  $formats['filtered_html'] = array(
    'format' => 'filtered_html',
    'name' => 'Filtered HTML',
    'cache' => 1,
    'status' => 1,
    'weight' => -10,
    'filters' => array(
      'filter_html' => array(
        'weight' => 1,
        'status' => 1,
        'settings' => array(
          'allowed_html' => '<a> <em> <strong> <u> <cite> <blockquote> <code> <ul> <ol> <li> <dl> <dt> <dd> <h4> <h5> <h6>  <p> <br> <img> <strike>',
          'filter_html_help' => 1,
          'filter_html_nofollow' => 0,
        ),
      ),
      'pathologic' => array(
        'weight' => 50,
        'status' => 1,
        'settings' => array(
          'local_paths' => 'http://online.unitedway.org
http://uwonline.dev.forumone.com
http://dev.online.unitedway.org',
          'protocol_style' => 'path',
          'local_paths_exploded' => array(
            0 => array(
              'scheme' => 'http',
              'host' => 'online.unitedway.org',
            ),
            1 => array(
              'scheme' => 'http',
              'host' => 'uwonline.dev.forumone.com',
            ),
            2 => array(
              'scheme' => 'http',
              'host' => 'dev.online.unitedway.org',
            ),
            3 => array(
              'path' => '/',
            ),
            4 => array(
              'path' => '/',
              'host' => 'localhost',
            ),
          ),
          'base_url_host' => 'localhost',
        ),
      ),
    ),
  );

  // Exported format: Full HTML.
  $formats['full_html'] = array(
    'format' => 'full_html',
    'name' => 'Full HTML',
    'cache' => 1,
    'status' => 1,
    'weight' => 1,
    'filters' => array(
      'pathologic' => array(
        'weight' => 50,
        'status' => 1,
        'settings' => array(
          'local_paths' => 'http://online.unitedway.org
http://uwonline.dev.forumone.com
http://dev.online.unitedway.org',
          'protocol_style' => 'path',
          'local_paths_exploded' => array(
            0 => array(
              'scheme' => 'http',
              'host' => 'online.unitedway.org',
            ),
            1 => array(
              'scheme' => 'http',
              'host' => 'uwonline.dev.forumone.com',
            ),
            2 => array(
              'scheme' => 'http',
              'host' => 'dev.online.unitedway.org',
            ),
            3 => array(
              'path' => '/',
            ),
            4 => array(
              'path' => '/',
              'host' => 'localhost',
            ),
          ),
          'base_url_host' => 'localhost',
        ),
      ),
    ),
  );

  // Exported format: Plain text.
  $formats['plain_text'] = array(
    'format' => 'plain_text',
    'name' => 'Plain text',
    'cache' => 1,
    'status' => 1,
    'weight' => 10,
    'filters' => array(
      'filter_html_escape' => array(
        'weight' => 0,
        'status' => 1,
        'settings' => array(),
      ),
      'filter_url' => array(
        'weight' => 1,
        'status' => 1,
        'settings' => array(
          'filter_url_length' => 72,
        ),
      ),
      'filter_autop' => array(
        'weight' => 2,
        'status' => 1,
        'settings' => array(),
      ),
    ),
  );

  return $formats;
}
