<?php
/**
 * @file
 * uwc_landing_page.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function uwc_landing_page_field_group_info() {
  $export = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_lp_type_settings_content|field_collection_item|field_landing_page_section|form';
  $field_group->group_name = 'group_lp_type_settings_content';
  $field_group->entity_type = 'field_collection_item';
  $field_group->bundle = 'field_landing_page_section';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_lp_type_settings';
  $field_group->data = array(
    'label' => 'Content Type Settings',
    'weight' => '38',
    'children' => array(
      0 => 'field_lp_type_settings_content_r',
    ),
    'format_type' => 'htab',
    'format_settings' => array(
      'label' => 'Content Type Settings',
      'instance_settings' => array(
        'required_fields' => 0,
        'id' => '',
        'classes' => ' group-lp-type-settings-content field-group-htab',
        'description' => '',
      ),
      'formatter' => 'closed',
    ),
  );
  $export['group_lp_type_settings_content|field_collection_item|field_landing_page_section|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_lp_type_settings_image|field_collection_item|field_landing_page_section|form';
  $field_group->group_name = 'group_lp_type_settings_image';
  $field_group->entity_type = 'field_collection_item';
  $field_group->bundle = 'field_landing_page_section';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_lp_type_settings';
  $field_group->data = array(
    'label' => 'Image Type Settings',
    'weight' => '36',
    'children' => array(
      0 => 'field_lp_type_settings_image_img',
      1 => 'field_lp_type_settings_image_lnk',
    ),
    'format_type' => 'htab',
    'format_settings' => array(
      'label' => 'Image Type Settings',
      'instance_settings' => array(
        'required_fields' => 0,
        'id' => '',
        'classes' => 'group-lp-type-settings-image field-group-htab',
        'description' => '',
      ),
      'formatter' => 'closed',
    ),
  );
  $export['group_lp_type_settings_image|field_collection_item|field_landing_page_section|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_lp_type_settings_links|field_collection_item|field_landing_page_section|form';
  $field_group->group_name = 'group_lp_type_settings_links';
  $field_group->entity_type = 'field_collection_item';
  $field_group->bundle = 'field_landing_page_section';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_lp_type_settings';
  $field_group->data = array(
    'label' => 'Links Type Settings',
    'weight' => '37',
    'children' => array(
      0 => 'field_lp_type_settings_links_lnk',
    ),
    'format_type' => 'htab',
    'format_settings' => array(
      'label' => 'Links Type Settings',
      'instance_settings' => array(
        'required_fields' => 0,
        'id' => '',
        'classes' => 'group-lp-type-settings-links field-group-htab',
        'description' => '',
      ),
      'formatter' => 'closed',
    ),
  );
  $export['group_lp_type_settings_links|field_collection_item|field_landing_page_section|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_lp_type_settings|field_collection_item|field_landing_page_section|form';
  $field_group->group_name = 'group_lp_type_settings';
  $field_group->entity_type = 'field_collection_item';
  $field_group->bundle = 'field_landing_page_section';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Horizontal Tabs Group',
    'weight' => '36',
    'children' => array(
      0 => 'group_lp_type_settings_content',
      1 => 'group_lp_type_settings_image',
      2 => 'group_lp_type_settings_links',
    ),
    'format_type' => 'htabs',
    'format_settings' => array(
      'label' => 'Horizontal Tabs Group',
      'instance_settings' => array(
        'classes' => 'group-lp-type-settings field-group-htabs',
      ),
    ),
  );
  $export['group_lp_type_settings|field_collection_item|field_landing_page_section|form'] = $field_group;

  return $export;
}
