<?php
/**
 * @file
 * uwc_salesforce.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function uwc_salesforce_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'salesforce_endpoint';
  $strongarm->value = 'https://test.salesforce.com';
  $export['salesforce_endpoint'] = $strongarm;

  return $export;
}
