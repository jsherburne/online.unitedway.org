<?php
/**
 * @file
 * uwc_community_of_practice_panels.quicktabs.inc
 */

/**
 * Implements hook_quicktabs_default_quicktabs().
 */
function uwc_community_of_practice_panels_quicktabs_default_quicktabs() {
  $export = array();

  $quicktabs = new stdClass();
  $quicktabs->disabled = FALSE; /* Edit this to true to make a default quicktabs disabled initially */
  $quicktabs->api_version = 1;
  $quicktabs->machine_name = 'community_of_practice';
  $quicktabs->ajax = 0;
  $quicktabs->hide_empty_tabs = FALSE;
  $quicktabs->default_tab = 0;
  $quicktabs->title = 'Get Connected';
  $quicktabs->tabs = array(
    0 => array(
      'machine_name' => 'cop_recent',
      'title' => 'Recent',
      'weight' => '-100',
      'type' => 'qtabs',
    ),
    1 => array(
      'machine_name' => 'cop_popular',
      'title' => 'Popular',
      'weight' => '-99',
      'type' => 'qtabs',
    ),
    2 => array(
      'machine_name' => 'cop_recommended',
      'title' => 'Recommended',
      'weight' => '-98',
      'type' => 'qtabs',
    ),
    3 => array(
      'vid' => 'commons_activity_streams_activity',
      'display' => 'panel_pane_2',
      'args' => '',
      'title' => 'Member Activity',
      'weight' => '-97',
      'type' => 'view',
    ),
  );
  $quicktabs->renderer = 'quicktabs';
  $quicktabs->style = 'nostyle';
  $quicktabs->options = array();

  // Translatables
  // Included for use with string extractors like potx.
  t('Get Connected');
  t('Member Activity');
  t('Popular');
  t('Recent');
  t('Recommended');

  $export['community_of_practice'] = $quicktabs;

  $quicktabs = new stdClass();
  $quicktabs->disabled = FALSE; /* Edit this to true to make a default quicktabs disabled initially */
  $quicktabs->api_version = 1;
  $quicktabs->machine_name = 'cop_popular';
  $quicktabs->ajax = 0;
  $quicktabs->hide_empty_tabs = TRUE;
  $quicktabs->default_tab = 0;
  $quicktabs->title = 'Popular';
  $quicktabs->tabs = array(
    0 => array(
      'vid' => 'uwc_cop_recent_content_tabs',
      'display' => 'panel_pane_1',
      'args' => '',
      'title' => 'All',
      'weight' => '-100',
      'type' => 'view',
    ),
    1 => array(
      'vid' => 'uwc_cop_recent_content_tabs',
      'display' => 'panel_pane_2',
      'args' => '',
      'title' => 'Discussions',
      'weight' => '-99',
      'type' => 'view',
    ),
    2 => array(
      'vid' => 'uwc_cop_recent_content_tabs',
      'display' => 'panel_pane_3',
      'args' => '',
      'title' => 'Resources',
      'weight' => '-98',
      'type' => 'view',
    ),
    3 => array(
      'vid' => 'uwc_cop_recent_content_tabs',
      'display' => 'panel_pane_4',
      'args' => '',
      'title' => 'Topic Guides',
      'weight' => '-97',
      'type' => 'view',
    ),
    4 => array(
      'vid' => 'uwc_cop_recent_content_tabs',
      'display' => 'panel_pane_5',
      'args' => '',
      'title' => 'News',
      'weight' => '-96',
      'type' => 'view',
    ),
    5 => array(
      'vid' => 'uwc_cop_recent_content_tabs',
      'display' => 'panel_pane_11',
      'args' => '',
      'title' => 'Polls',
      'weight' => '-95',
      'type' => 'view',
    ),
  );
  $quicktabs->renderer = 'quicktabs';
  $quicktabs->style = 'nostyle';
  $quicktabs->options = array();

  // Translatables
  // Included for use with string extractors like potx.
  t('All');
  t('Discussions');
  t('News');
  t('Polls');
  t('Popular');
  t('Resources');
  t('Topic Guides');

  $export['cop_popular'] = $quicktabs;

  $quicktabs = new stdClass();
  $quicktabs->disabled = FALSE; /* Edit this to true to make a default quicktabs disabled initially */
  $quicktabs->api_version = 1;
  $quicktabs->machine_name = 'cop_recent';
  $quicktabs->ajax = 0;
  $quicktabs->hide_empty_tabs = TRUE;
  $quicktabs->default_tab = 0;
  $quicktabs->title = 'Recent';
  $quicktabs->tabs = array(
    0 => array(
      'vid' => 'uwc_cop_recent_content_tabs',
      'display' => 'block',
      'args' => '',
      'title' => 'All',
      'weight' => '-100',
      'type' => 'view',
    ),
    1 => array(
      'vid' => 'uwc_cop_recent_content_tabs',
      'display' => 'block_1',
      'args' => '',
      'title' => 'Discussions',
      'weight' => '-99',
      'type' => 'view',
    ),
    2 => array(
      'vid' => 'uwc_cop_recent_content_tabs',
      'display' => 'block_2',
      'args' => '',
      'title' => 'Resources',
      'weight' => '-98',
      'type' => 'view',
    ),
    3 => array(
      'vid' => 'uwc_cop_recent_content_tabs',
      'display' => 'block_3',
      'args' => '',
      'title' => 'Topic Guides',
      'weight' => '-97',
      'type' => 'view',
    ),
    4 => array(
      'vid' => 'uwc_cop_recent_content_tabs',
      'display' => 'block_4',
      'args' => '',
      'title' => 'News',
      'weight' => '-96',
      'type' => 'view',
    ),
    5 => array(
      'vid' => 'uwc_cop_recent_content_tabs',
      'display' => 'block_5',
      'args' => '',
      'title' => 'Polls',
      'weight' => '-95',
      'type' => 'view',
    ),
  );
  $quicktabs->renderer = 'quicktabs';
  $quicktabs->style = 'nostyle';
  $quicktabs->options = array();

  // Translatables
  // Included for use with string extractors like potx.
  t('All');
  t('Discussions');
  t('News');
  t('Polls');
  t('Recent');
  t('Resources');
  t('Topic Guides');

  $export['cop_recent'] = $quicktabs;

  $quicktabs = new stdClass();
  $quicktabs->disabled = FALSE; /* Edit this to true to make a default quicktabs disabled initially */
  $quicktabs->api_version = 1;
  $quicktabs->machine_name = 'cop_recommended';
  $quicktabs->ajax = 0;
  $quicktabs->hide_empty_tabs = TRUE;
  $quicktabs->default_tab = 0;
  $quicktabs->title = 'Recommended';
  $quicktabs->tabs = array(
    0 => array(
      'vid' => 'uwc_cop_recent_content_tabs',
      'display' => 'panel_pane_6',
      'args' => '',
      'title' => 'All',
      'weight' => '-100',
      'type' => 'view',
    ),
    1 => array(
      'vid' => 'uwc_cop_recent_content_tabs',
      'display' => 'panel_pane_7',
      'args' => '',
      'title' => 'Discussions',
      'weight' => '-99',
      'type' => 'view',
    ),
    2 => array(
      'vid' => 'uwc_cop_recent_content_tabs',
      'display' => 'panel_pane_8',
      'args' => '',
      'title' => 'Resources',
      'weight' => '-98',
      'type' => 'view',
    ),
    3 => array(
      'vid' => 'uwc_cop_recent_content_tabs',
      'display' => 'panel_pane_9',
      'args' => '',
      'title' => 'Topic Guides',
      'weight' => '-97',
      'type' => 'view',
    ),
    4 => array(
      'vid' => 'uwc_cop_recent_content_tabs',
      'display' => 'panel_pane_10',
      'args' => '',
      'title' => 'News',
      'weight' => '-96',
      'type' => 'view',
    ),
    5 => array(
      'vid' => 'uwc_cop_recent_content_tabs',
      'display' => 'panel_pane_12',
      'args' => '',
      'title' => 'Polls',
      'weight' => '-95',
      'type' => 'view',
    ),
  );
  $quicktabs->renderer = 'quicktabs';
  $quicktabs->style = 'nostyle';
  $quicktabs->options = array();

  // Translatables
  // Included for use with string extractors like potx.
  t('All');
  t('Discussions');
  t('News');
  t('Polls');
  t('Recommended');
  t('Resources');
  t('Topic Guides');

  $export['cop_recommended'] = $quicktabs;

  return $export;
}
