<?php
/**
 * @file
 * uwc_community_of_practice_panels.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function uwc_community_of_practice_panels_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "page_manager" && $api == "pages_default") {
    return array("version" => "1");
  }
  if ($module == "panels_mini" && $api == "panels_default") {
    return array("version" => "1");
  }
  if ($module == "quicktabs" && $api == "quicktabs") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function uwc_community_of_practice_panels_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}
