<?php
/**
 * @file
 * uwc_events.ds.inc
 */

/**
 * Implements hook_ds_field_settings_info().
 */
function uwc_events_ds_field_settings_info() {
  $export = array();

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'node|event|default';
  $ds_fieldsetting->entity_type = 'node';
  $ds_fieldsetting->bundle = 'event';
  $ds_fieldsetting->view_mode = 'default';
  $ds_fieldsetting->settings = array(
    'links' => array(
      'weight' => '17',
      'label' => 'hidden',
      'format' => 'default',
    ),
    'comments' => array(
      'weight' => '18',
      'label' => 'hidden',
      'format' => 'default',
    ),
    'rate_integration_1' => array(
      'weight' => '14',
      'label' => 'hidden',
      'format' => 'default',
    ),
    'og_group_ref' => array(
      'formatter_settings' => array(
        'ft' => array(
          'lb' => 'Communities of Practice',
        ),
      ),
    ),
    'field_date' => array(
      'formatter_settings' => array(
        'ft' => array(),
      ),
    ),
  );
  $export['node|event|default'] = $ds_fieldsetting;

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'node|event|full';
  $ds_fieldsetting->entity_type = 'node';
  $ds_fieldsetting->bundle = 'event';
  $ds_fieldsetting->view_mode = 'full';
  $ds_fieldsetting->settings = array(
    'private_group_flag' => array(
      'weight' => '1',
      'label' => 'hidden',
      'format' => 'default',
    ),
    'links' => array(
      'weight' => '14',
      'label' => 'hidden',
      'format' => 'default',
    ),
    'comments' => array(
      'weight' => '16',
      'label' => 'hidden',
      'format' => 'default',
    ),
    'flag_breakfast_recommendation' => array(
      'weight' => '5',
      'label' => 'hidden',
      'format' => 'default',
    ),
    'rate_integration_1' => array(
      'weight' => '4',
      'label' => 'hidden',
      'format' => 'default',
    ),
    'og_group_ref' => array(
      'formatter_settings' => array(
        'ft' => array(
          'lb' => 'Communities of Practice',
        ),
      ),
    ),
    'field_event_type' => array(
      'formatter_settings' => array(
        'ft' => array(
          'func' => 'theme_field',
          'classes' => 'type',
        ),
      ),
    ),
  );
  $export['node|event|full'] = $ds_fieldsetting;

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'node|event|related_content';
  $ds_fieldsetting->entity_type = 'node';
  $ds_fieldsetting->bundle = 'event';
  $ds_fieldsetting->view_mode = 'related_content';
  $ds_fieldsetting->settings = array(
    'author' => array(
      'weight' => '2',
      'label' => 'hidden',
      'format' => 'author_linked',
    ),
    'post_date' => array(
      'weight' => '1',
      'label' => 'hidden',
      'format' => 'ds_post_date_rich_snippets_published_date',
    ),
    'profile2' => array(
      'weight' => '0',
      'label' => 'hidden',
      'format' => 'related_content',
    ),
  );
  $export['node|event|related_content'] = $ds_fieldsetting;

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'node|event|search_result';
  $ds_fieldsetting->entity_type = 'node';
  $ds_fieldsetting->bundle = 'event';
  $ds_fieldsetting->view_mode = 'search_result';
  $ds_fieldsetting->settings = array(
    'links' => array(
      'weight' => '7',
      'label' => 'hidden',
      'format' => 'default',
    ),
    'rate_integration_1' => array(
      'weight' => '6',
      'label' => 'hidden',
      'format' => 'default',
    ),
  );
  $export['node|event|search_result'] = $ds_fieldsetting;

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'node|event|teaser';
  $ds_fieldsetting->entity_type = 'node';
  $ds_fieldsetting->bundle = 'event';
  $ds_fieldsetting->view_mode = 'teaser';
  $ds_fieldsetting->settings = array(
    'private_group_flag' => array(
      'weight' => '3',
      'label' => 'hidden',
      'format' => 'default',
    ),
    'comment_count' => array(
      'weight' => '11',
      'label' => 'hidden',
      'format' => 'default',
    ),
    'profile2' => array(
      'weight' => '0',
      'label' => 'hidden',
      'format' => 'compact_teaser',
    ),
    'rate_integration_1' => array(
      'weight' => '10',
      'label' => 'hidden',
      'format' => 'default',
    ),
    'og_group_ref' => array(
      'formatter_settings' => array(
        'ft' => array(
          'lb' => 'Communities of practice',
        ),
      ),
    ),
    'field_date' => array(
      'formatter_settings' => array(
        'ft' => array(
          'func' => 'theme_ds_field_expert',
          'fi' => TRUE,
          'fi-el' => 'div',
          'fi-cl' => 'field-name-post-date',
          'fi-at' => '',
          'fi-def-at' => FALSE,
          'fi-odd-even' => FALSE,
        ),
      ),
    ),
    'field_event_type' => array(
      'formatter_settings' => array(
        'ft' => array(
          'func' => 'theme_field',
          'classes' => 'type',
        ),
      ),
    ),
  );
  $export['node|event|teaser'] = $ds_fieldsetting;

  return $export;
}

/**
 * Implements hook_ds_layout_settings_info().
 */
function uwc_events_ds_layout_settings_info() {
  $export = array();

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|event|compact_teaser';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'event';
  $ds_layout->view_mode = 'compact_teaser';
  $ds_layout->layout = 'ds_1col';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'field_date',
        1 => 'field_address',
        2 => 'title_field',
        3 => 'field_offsite_url',
      ),
    ),
    'fields' => array(
      'field_date' => 'ds_content',
      'field_address' => 'ds_content',
      'title_field' => 'ds_content',
      'field_offsite_url' => 'ds_content',
    ),
    'classes' => array(),
    'wrappers' => array(
      'ds_content' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
  );
  $export['node|event|compact_teaser'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|event|default';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'event';
  $ds_layout->view_mode = 'default';
  $ds_layout->layout = 'ds_1col';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'title_field',
        1 => 'field_event_type',
        2 => 'field_date',
        3 => 'og_group_ref',
        4 => 'field_topics',
        5 => 'field_event_contact',
        6 => 'field_logo',
        7 => 'body',
        8 => 'field_event_location_misc',
        9 => 'field_reg_info',
        10 => 'field_reg_early_date',
        11 => 'field_related_content',
        12 => 'field_related_organizations',
        13 => 'rate_integration_1',
        14 => 'field_offsite_url',
        15 => 'links',
        16 => 'comments',
      ),
    ),
    'fields' => array(
      'title_field' => 'ds_content',
      'field_event_type' => 'ds_content',
      'field_date' => 'ds_content',
      'og_group_ref' => 'ds_content',
      'field_topics' => 'ds_content',
      'field_event_contact' => 'ds_content',
      'field_logo' => 'ds_content',
      'body' => 'ds_content',
      'field_event_location_misc' => 'ds_content',
      'field_reg_info' => 'ds_content',
      'field_reg_early_date' => 'ds_content',
      'field_related_content' => 'ds_content',
      'field_related_organizations' => 'ds_content',
      'rate_integration_1' => 'ds_content',
      'field_offsite_url' => 'ds_content',
      'links' => 'ds_content',
      'comments' => 'ds_content',
    ),
    'classes' => array(
      'layout_class' => array(
        '' => '',
      ),
    ),
    'wrappers' => array(
      'ds_content' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
  );
  $export['node|event|default'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|event|full';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'event';
  $ds_layout->view_mode = 'full';
  $ds_layout->layout = 'ds_1col';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'field_event_type',
        1 => 'private_group_flag',
        2 => 'field_date',
        3 => 'title_field',
        4 => 'og_group_ref',
        5 => 'rate_integration_1',
        6 => 'flag_breakfast_recommendation',
        7 => 'field_topics',
        8 => 'group_node_tax',
        9 => 'field_logo',
        10 => 'field_event_contact',
        11 => 'field_offsite_url',
        12 => 'body',
        13 => 'field_event_file',
        14 => 'field_event_location_misc',
        15 => 'field_radioactivity',
        16 => 'links',
        17 => 'field_related_content',
        18 => 'comments',
        19 => 'field_related_organizations',
      ),
    ),
    'fields' => array(
      'field_event_type' => 'ds_content',
      'private_group_flag' => 'ds_content',
      'field_date' => 'ds_content',
      'title_field' => 'ds_content',
      'og_group_ref' => 'ds_content',
      'rate_integration_1' => 'ds_content',
      'flag_breakfast_recommendation' => 'ds_content',
      'field_topics' => 'ds_content',
      'group_node_tax' => 'ds_content',
      'field_logo' => 'ds_content',
      'field_event_contact' => 'ds_content',
      'field_offsite_url' => 'ds_content',
      'body' => 'ds_content',
      'field_event_file' => 'ds_content',
      'field_event_location_misc' => 'ds_content',
      'field_radioactivity' => 'ds_content',
      'links' => 'ds_content',
      'field_related_content' => 'ds_content',
      'comments' => 'ds_content',
      'field_related_organizations' => 'ds_content',
    ),
    'classes' => array(
      'layout_class' => array(
        '' => '',
      ),
    ),
    'wrappers' => array(
      'ds_content' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
  );
  $export['node|event|full'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|event|related_content';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'event';
  $ds_layout->view_mode = 'related_content';
  $ds_layout->layout = 'ds_3col';
  $ds_layout->settings = array(
    'regions' => array(
      'left' => array(
        0 => 'profile2',
      ),
      'middle' => array(
        1 => 'post_date',
        2 => 'author',
        3 => 'title_field',
      ),
      'right' => array(
        4 => 'field_event_type',
      ),
    ),
    'fields' => array(
      'profile2' => 'left',
      'post_date' => 'middle',
      'author' => 'middle',
      'title_field' => 'middle',
      'field_event_type' => 'right',
    ),
    'classes' => array(
      'layout_class' => array(
        '' => '',
      ),
    ),
    'wrappers' => array(
      'left' => 'div',
      'middle' => 'div',
      'right' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
  );
  $export['node|event|related_content'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|event|search_result';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'event';
  $ds_layout->view_mode = 'search_result';
  $ds_layout->layout = 'ds_1col';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'field_logo',
        1 => 'field_date',
        2 => 'field_location',
        3 => 'title_field',
        4 => 'body',
        5 => 'field_topics',
        6 => 'rate_integration_1',
        7 => 'links',
      ),
    ),
    'fields' => array(
      'field_logo' => 'ds_content',
      'field_date' => 'ds_content',
      'field_location' => 'ds_content',
      'title_field' => 'ds_content',
      'body' => 'ds_content',
      'field_topics' => 'ds_content',
      'rate_integration_1' => 'ds_content',
      'links' => 'ds_content',
    ),
    'classes' => array(),
    'wrappers' => array(
      'ds_content' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
  );
  $export['node|event|search_result'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|event|teaser';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'event';
  $ds_layout->view_mode = 'teaser';
  $ds_layout->layout = 'ds_2col';
  $ds_layout->settings = array(
    'regions' => array(
      'left' => array(
        0 => 'profile2',
      ),
      'right' => array(
        1 => 'field_date',
        2 => 'field_event_type',
        3 => 'private_group_flag',
        4 => 'title_field',
        5 => 'body',
        6 => 'field_location',
        7 => 'og_group_ref',
        8 => 'field_topics',
        9 => 'group_node_actions',
        10 => 'rate_integration_1',
        11 => 'comment_count',
      ),
    ),
    'fields' => array(
      'profile2' => 'left',
      'field_date' => 'right',
      'field_event_type' => 'right',
      'private_group_flag' => 'right',
      'title_field' => 'right',
      'body' => 'right',
      'field_location' => 'right',
      'og_group_ref' => 'right',
      'field_topics' => 'right',
      'group_node_actions' => 'right',
      'rate_integration_1' => 'right',
      'comment_count' => 'right',
    ),
    'classes' => array(
      'layout_class' => array(
        '' => '',
      ),
      'left' => array(
        'teaser-left' => 'teaser-left',
      ),
      'right' => array(
        'teaser-right' => 'teaser-right',
      ),
    ),
    'wrappers' => array(
      'left' => 'div',
      'right' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
  );
  $export['node|event|teaser'] = $ds_layout;

  return $export;
}
