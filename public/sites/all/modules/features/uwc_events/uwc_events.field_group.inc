<?php
/**
 * @file
 * uwc_events.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function uwc_events_field_group_info() {
  $export = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_event_basic|node|event|form';
  $field_group->group_name = 'group_event_basic';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'event';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_event_nav';
  $field_group->data = array(
    'label' => 'Basic event information',
    'weight' => '1',
    'children' => array(
      0 => 'body',
      1 => 'field_date',
      2 => 'field_logo',
      3 => 'field_offsite_url',
      4 => 'title_field',
      5 => 'field_event_contact',
      6 => 'field_event_type',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-event-basic field-group-tab',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_event_basic|node|event|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_event_location|node|event|form';
  $field_group->group_name = 'group_event_location';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'event';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_event_nav';
  $field_group->data = array(
    'label' => 'Location information',
    'weight' => '2',
    'children' => array(
      0 => 'field_event_location_misc',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-event-location field-group-tab',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_event_location|node|event|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_event_nav|node|event|form';
  $field_group->group_name = 'group_event_nav';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'event';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Event nav',
    'weight' => '0',
    'children' => array(
      0 => 'group_event_basic',
      1 => 'group_event_location',
      2 => 'group_event_registration',
      3 => 'group_event_relationships',
    ),
    'format_type' => 'tabs',
    'format_settings' => array(
      'formatter' => '',
      'instance_settings' => array(
        'classes' => 'group-event-nav field-group-tabs',
      ),
    ),
  );
  $export['group_event_nav|node|event|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_event_registration|node|event|form';
  $field_group->group_name = 'group_event_registration';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'event';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_event_nav';
  $field_group->data = array(
    'label' => 'Registration information',
    'weight' => '3',
    'children' => array(
      0 => 'field_reg_info',
      1 => 'field_reg_early_date',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-event-registration field-group-tab',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_event_registration|node|event|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_event_relationships|node|event|form';
  $field_group->group_name = 'group_event_relationships';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'event';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_event_nav';
  $field_group->data = array(
    'label' => 'Related items',
    'weight' => '4',
    'children' => array(
      0 => 'og_group_ref',
      1 => 'field_topics',
      2 => 'field_related_organizations',
      3 => 'field_related_content',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-event-relationships field-group-tab',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_event_relationships|node|event|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_node_actions|node|event|teaser';
  $field_group->group_name = 'group_node_actions';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'event';
  $field_group->mode = 'teaser';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Node Actions',
    'weight' => '9',
    'children' => array(
      0 => 'comment_count',
      1 => 'rate_integration_1',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'Node Actions',
      'instance_settings' => array(
        'classes' => 'group-node-actions field-group-div',
        'description' => '',
        'show_label' => '0',
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'fast',
      ),
      'formatter' => 'open',
    ),
  );
  $export['group_node_actions|node|event|teaser'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_node_tax|node|event|full';
  $field_group->group_name = 'group_node_tax';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'event';
  $field_group->mode = 'full';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Taxonomy',
    'weight' => '6',
    'children' => array(
      0 => 'og_group_ref',
      1 => 'field_topics',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'Taxonomy',
      'instance_settings' => array(
        'classes' => 'group-node-tax field-group-div',
        'description' => '',
        'show_label' => '0',
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'fast',
      ),
      'formatter' => 'open',
    ),
  );
  $export['group_node_tax|node|event|full'] = $field_group;

  return $export;
}
