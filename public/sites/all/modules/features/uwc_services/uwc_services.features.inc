<?php
/**
 * @file
 * uwc_services.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function uwc_services_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "services" && $api == "services") {
    return array("version" => "3");
  }
}
