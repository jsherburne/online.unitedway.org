<?php
/**
 * @file
 * uwc_simplesaml_drupalauth_settings.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function uwc_simplesaml_drupalauth_settings_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'drupalauth4ssp_authsource';
  $strongarm->value = 'drupal-userpass';
  $export['drupalauth4ssp_authsource'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'drupalauth4ssp_installdir';
  $strongarm->value = '/var/www/current/simplesamlphp';
  $export['drupalauth4ssp_installdir'] = $strongarm;

  return $export;
}
