<?php
/**
 * @file
 * uwc_news_service.feeds_tamper_default.inc
 */

/**
 * Implements hook_feeds_tamper_default().
 */
function uwc_news_service_feeds_tamper_default() {
  $export = array();

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'uwc_meltwater_feed-blank_source_1-default_value';
  $feeds_tamper->importer = 'uwc_meltwater_feed';
  $feeds_tamper->source = 'Blank source 1';
  $feeds_tamper->plugin_id = 'default_value';
  $feeds_tamper->settings = array(
    'default_value' => 'In the News',
  );
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = 'Set default value';
  $export['uwc_meltwater_feed-blank_source_1-default_value'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'uwc_meltwater_feed-xpathparser_6-keyword_filter';
  $feeds_tamper->importer = 'uwc_meltwater_feed';
  $feeds_tamper->source = 'xpathparser:6';
  $feeds_tamper->plugin_id = 'keyword_filter';
  $feeds_tamper->settings = array(
    'words' => 'English',
    'word_boundaries' => 0,
    'exact' => 1,
    'case_sensitive' => 0,
    'invert' => 0,
    'word_list' => array(
      0 => '/^\\QEnglish\\E$/ui',
    ),
    'regex' => TRUE,
  );
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = 'Keyword filter';
  $export['uwc_meltwater_feed-xpathparser_6-keyword_filter'] = $feeds_tamper;

  return $export;
}
