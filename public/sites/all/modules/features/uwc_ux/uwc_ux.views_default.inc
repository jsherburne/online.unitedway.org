<?php
/**
 * @file
 * uwc_ux.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function uwc_ux_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'uwc_slideshow';
  $view->description = 'Slideshow display for UW featured content.';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'UWC: Slideshow';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'views_content_cache';
  $handler->display->display_options['cache']['results_min_lifespan'] = '60';
  $handler->display->display_options['cache']['results_max_lifespan'] = '518400';
  $handler->display->display_options['cache']['output_min_lifespan'] = '60';
  $handler->display->display_options['cache']['output_max_lifespan'] = '518400';
  $handler->display->display_options['cache']['keys'] = array(
    'comment' => array(
      'changed' => 0,
    ),
    'node' => array(
      'post' => 'post',
      'news' => 'news',
      'page' => 'page',
      'poll' => 'poll',
      'child_event' => 0,
      'event' => 0,
      'group' => 0,
      'guide' => 0,
      'landing_page' => 0,
      'organization' => 0,
      'document' => 0,
      'wrapped_system' => 0,
    ),
    'node_only' => array(
      'node_changed' => 0,
    ),
    'og' => array(
      'current' => 0,
      'user' => 0,
    ),
    'votingapi' => array(
      'tag:vote' => 0,
      'tag:commons_like' => 0,
      'function:count' => 0,
      'function:average' => 0,
      'function:sum' => 0,
    ),
  );
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'some';
  $handler->display->display_options['pager']['options']['items_per_page'] = '3';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['style_plugin'] = 'flexslider';
  $handler->display->display_options['row_plugin'] = 'fields';
  $handler->display->display_options['row_options']['default_field_elements'] = FALSE;
  /* Relationship: Homepage Slideshow Queue */
  $handler->display->display_options['relationships']['nodequeue_rel']['id'] = 'nodequeue_rel';
  $handler->display->display_options['relationships']['nodequeue_rel']['table'] = 'node';
  $handler->display->display_options['relationships']['nodequeue_rel']['field'] = 'nodequeue_rel';
  $handler->display->display_options['relationships']['nodequeue_rel']['ui_name'] = 'Homepage Slideshow Queue';
  $handler->display->display_options['relationships']['nodequeue_rel']['required'] = TRUE;
  $handler->display->display_options['relationships']['nodequeue_rel']['limit'] = 1;
  $handler->display->display_options['relationships']['nodequeue_rel']['names'] = array(
    'homepage_slideshow' => 'homepage_slideshow',
  );
  /* Relationship: Content: Slideshow Collection (field_slideshow_collection) */
  $handler->display->display_options['relationships']['field_slideshow_collection_value']['id'] = 'field_slideshow_collection_value';
  $handler->display->display_options['relationships']['field_slideshow_collection_value']['table'] = 'field_data_field_slideshow_collection';
  $handler->display->display_options['relationships']['field_slideshow_collection_value']['field'] = 'field_slideshow_collection_value';
  $handler->display->display_options['relationships']['field_slideshow_collection_value']['delta'] = '-1';
  /* Field: Content: Nid */
  $handler->display->display_options['fields']['nid']['id'] = 'nid';
  $handler->display->display_options['fields']['nid']['table'] = 'node';
  $handler->display->display_options['fields']['nid']['field'] = 'nid';
  $handler->display->display_options['fields']['nid']['label'] = '';
  $handler->display->display_options['fields']['nid']['exclude'] = TRUE;
  $handler->display->display_options['fields']['nid']['element_label_colon'] = FALSE;
  /* Field: Content: Slideshow Collection */
  $handler->display->display_options['fields']['field_slideshow_collection']['id'] = 'field_slideshow_collection';
  $handler->display->display_options['fields']['field_slideshow_collection']['table'] = 'field_data_field_slideshow_collection';
  $handler->display->display_options['fields']['field_slideshow_collection']['field'] = 'field_slideshow_collection';
  $handler->display->display_options['fields']['field_slideshow_collection']['label'] = '';
  $handler->display->display_options['fields']['field_slideshow_collection']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_slideshow_collection']['alter']['make_link'] = TRUE;
  $handler->display->display_options['fields']['field_slideshow_collection']['alter']['path'] = 'node/[nid]';
  $handler->display->display_options['fields']['field_slideshow_collection']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_slideshow_collection']['settings'] = array(
    'view_mode' => 'full',
  );
  /* Field: Field collection item: Slide Image */
  $handler->display->display_options['fields']['field_slide_image']['id'] = 'field_slide_image';
  $handler->display->display_options['fields']['field_slide_image']['table'] = 'field_data_field_slide_image';
  $handler->display->display_options['fields']['field_slide_image']['field'] = 'field_slide_image';
  $handler->display->display_options['fields']['field_slide_image']['relationship'] = 'field_slideshow_collection_value';
  $handler->display->display_options['fields']['field_slide_image']['label'] = '';
  $handler->display->display_options['fields']['field_slide_image']['alter']['make_link'] = TRUE;
  $handler->display->display_options['fields']['field_slide_image']['alter']['path'] = 'node/[nid]';
  $handler->display->display_options['fields']['field_slide_image']['element_type'] = 'div';
  $handler->display->display_options['fields']['field_slide_image']['element_class'] = 'field-slide-image';
  $handler->display->display_options['fields']['field_slide_image']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_slide_image']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_slide_image']['settings'] = array(
    'image_style' => 'homepage_slideshow',
    'image_link' => '',
  );
  /* Field: Field collection item: Slide Title */
  $handler->display->display_options['fields']['field_slide_title']['id'] = 'field_slide_title';
  $handler->display->display_options['fields']['field_slide_title']['table'] = 'field_data_field_slide_title';
  $handler->display->display_options['fields']['field_slide_title']['field'] = 'field_slide_title';
  $handler->display->display_options['fields']['field_slide_title']['relationship'] = 'field_slideshow_collection_value';
  $handler->display->display_options['fields']['field_slide_title']['label'] = '';
  $handler->display->display_options['fields']['field_slide_title']['alter']['make_link'] = TRUE;
  $handler->display->display_options['fields']['field_slide_title']['alter']['path'] = 'node/[nid]';
  $handler->display->display_options['fields']['field_slide_title']['element_type'] = 'h3';
  $handler->display->display_options['fields']['field_slide_title']['element_class'] = 'field-slide-title';
  $handler->display->display_options['fields']['field_slide_title']['element_label_colon'] = FALSE;
  /* Sort criterion: Nodequeue: Position */
  $handler->display->display_options['sorts']['position']['id'] = 'position';
  $handler->display->display_options['sorts']['position']['table'] = 'nodequeue_nodes';
  $handler->display->display_options['sorts']['position']['field'] = 'position';
  $handler->display->display_options['sorts']['position']['relationship'] = 'nodequeue_rel';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;

  /* Display: Homepage Slideshow */
  $handler = $view->new_display('panel_pane', 'Homepage Slideshow', 'homepage_pane');
  $export['uwc_slideshow'] = $view;

  return $export;
}
