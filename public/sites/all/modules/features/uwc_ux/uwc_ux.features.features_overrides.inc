<?php
/**
 * @file
 * uwc_ux.features.features_overrides.inc
 */

/**
 * Implements hook_features_override_default_overrides().
 */
function uwc_ux_features_override_default_overrides() {
  // This code is only used for UI in features. Exported alters hooks do the magic.
  $overrides = array();

  // Exported overrides for: filter
  $overrides["filter.filtered_html.weight"] = -8;

  // Exported overrides for: page_manager_handlers
  $overrides["page_manager_handlers.node_view_panelizer.weight"] = -30;

  // Exported overrides for: variable

 return $overrides;
}
