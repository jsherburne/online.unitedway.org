<?php
/**
 * @file
 * uwc_users.ds.inc
 */

/**
 * Implements hook_ds_field_settings_info().
 */
function uwc_users_ds_field_settings_info() {
  $export = array();

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'profile2|main|account';
  $ds_fieldsetting->entity_type = 'profile2';
  $ds_fieldsetting->bundle = 'main';
  $ds_fieldsetting->view_mode = 'account';
  $ds_fieldsetting->settings = array(
    'member_overview_text' => array(
      'weight' => '0',
      'label' => 'hidden',
      'format' => 'default',
    ),
    'user_full_name' => array(
      'weight' => '0',
      'label' => 'hidden',
      'format' => 'uwc_users_user_full_name',
    ),
    'user_recent_activity' => array(
      'weight' => '0',
      'label' => 'above',
      'format' => 'quicktabs',
      'formatter_settings' => array(
        'ft' => array(
          'func' => 'theme_ds_field_expert',
          'lb-el' => 'h2',
          'lb-col' => TRUE,
        ),
        'field_delimiter' => '',
      ),
    ),
    'user_follow_count' => array(
      'weight' => '3',
      'label' => 'hidden',
      'format' => 'default',
    ),
    'user_email' => array(
      'weight' => '8',
      'label' => 'hidden',
      'format' => 'default',
    ),
    'flag_commons_follow_user' => array(
      'weight' => '2',
      'label' => 'hidden',
      'format' => 'default',
      'formatter_settings' => array(
        'ft' => array(),
      ),
    ),
    'field_bio' => array(
      'formatter_settings' => array(
        'ft' => array(
          'lb-col' => TRUE,
        ),
      ),
    ),
    'field_affiliations' => array(
      'formatter_settings' => array(
        'ft' => array(
          'lb-col' => TRUE,
        ),
      ),
    ),
    'field_personal_interests' => array(
      'formatter_settings' => array(
        'ft' => array(
          'lb-col' => TRUE,
        ),
      ),
    ),
  );
  $export['profile2|main|account'] = $ds_fieldsetting;

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'profile2|main|compact_teaser';
  $ds_fieldsetting->entity_type = 'profile2';
  $ds_fieldsetting->bundle = 'main';
  $ds_fieldsetting->view_mode = 'compact_teaser';
  $ds_fieldsetting->settings = array(
    'user_full_name' => array(
      'weight' => '1',
      'label' => 'hidden',
      'format' => 'uwc_users_user_full_name',
    ),
  );
  $export['profile2|main|compact_teaser'] = $ds_fieldsetting;

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'profile2|main|default';
  $ds_fieldsetting->entity_type = 'profile2';
  $ds_fieldsetting->bundle = 'main';
  $ds_fieldsetting->view_mode = 'default';
  $ds_fieldsetting->settings = array(
    'user_full_name' => array(
      'weight' => '0',
      'label' => 'hidden',
      'format' => 'uwc_users_user_full_name',
      'formatter_settings' => array(
        'link' => 1,
        'ft' => array(),
      ),
    ),
  );
  $export['profile2|main|default'] = $ds_fieldsetting;

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'profile2|main|search_result';
  $ds_fieldsetting->entity_type = 'profile2';
  $ds_fieldsetting->bundle = 'main';
  $ds_fieldsetting->view_mode = 'search_result';
  $ds_fieldsetting->settings = array(
    'user_full_name' => array(
      'weight' => '1',
      'label' => 'hidden',
      'format' => 'uwc_users_user_full_name',
      'formatter_settings' => array(
        'link' => 1,
        'ft' => array(
          'func' => 'theme_ds_field_expert',
          'ow' => TRUE,
          'ow-el' => 'div',
          'ow-cl' => 'field field-name-title-field',
          'ow-def-cl' => FALSE,
          'ow-at' => '',
          'ow-def-at' => FALSE,
          'fi' => TRUE,
          'fi-el' => 'h3',
          'fi-cl' => '',
          'fi-at' => '',
          'fi-def-at' => FALSE,
          'fi-odd-even' => FALSE,
        ),
        'field_delimiter' => '',
      ),
    ),
  );
  $export['profile2|main|search_result'] = $ds_fieldsetting;

  return $export;
}

/**
 * Implements hook_ds_custom_fields_info().
 */
function uwc_users_ds_custom_fields_info() {
  $export = array();

  $ds_field = new stdClass();
  $ds_field->api_version = 1;
  $ds_field->field = 'member_overview_text';
  $ds_field->label = 'member overview text';
  $ds_field->field_type = 5;
  $ds_field->entities = array(
    'profile2' => 'profile2',
  );
  $ds_field->ui_limit = '';
  $ds_field->properties = array(
    'code' => array(
      'value' => '<div class="member-overview">Member Overview</div>',
      'format' => 'full_html',
    ),
    'use_token' => 0,
  );
  $export['member_overview_text'] = $ds_field;

  return $export;
}

/**
 * Implements hook_ds_layout_settings_info().
 */
function uwc_users_ds_layout_settings_info() {
  $export = array();

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'field_collection_item|field_social_media_contact|default';
  $ds_layout->entity_type = 'field_collection_item';
  $ds_layout->bundle = 'field_social_media_contact';
  $ds_layout->view_mode = 'default';
  $ds_layout->layout = 'ds_1col';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'field_social_contact_type',
        1 => 'field_social_contact_value',
      ),
    ),
    'fields' => array(
      'field_social_contact_type' => 'ds_content',
      'field_social_contact_value' => 'ds_content',
    ),
    'classes' => array(),
    'wrappers' => array(
      'ds_content' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
  );
  $export['field_collection_item|field_social_media_contact|default'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'field_collection_item|field_social_media_contact|full';
  $ds_layout->entity_type = 'field_collection_item';
  $ds_layout->bundle = 'field_social_media_contact';
  $ds_layout->view_mode = 'full';
  $ds_layout->layout = 'ds_1col';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'field_social_contact_type',
        1 => 'field_social_contact_value',
      ),
    ),
    'fields' => array(
      'field_social_contact_type' => 'ds_content',
      'field_social_contact_value' => 'ds_content',
    ),
    'classes' => array(),
    'wrappers' => array(),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => TRUE,
    'layout_link_attribute' => FALSE,
    'layout_link_custom' => '',
  );
  $export['field_collection_item|field_social_media_contact|full'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'profile2|main|account';
  $ds_layout->entity_type = 'profile2';
  $ds_layout->bundle = 'main';
  $ds_layout->view_mode = 'account';
  $ds_layout->layout = 'profile';
  $ds_layout->settings = array(
    'regions' => array(
      'footer' => array(
        0 => 'user_recent_activity',
        6 => 'uwc_user_professional_positions_past_positions',
        11 => 'group_uw_positions',
        12 => 'group_contact',
        14 => 'user_email',
        15 => 'field_bio',
        16 => 'group_general_contact',
        17 => 'group_expertise',
        18 => 'group_area_ie',
        19 => 'group_social_media',
        21 => 'field_affiliations',
        22 => 'field_phone',
        23 => 'group_professional_profile',
        24 => 'field_social_media_contact',
        25 => 'group_activity_feed',
        26 => 'group_interests',
        27 => 'group_left_nav',
        28 => 'uwc_profile_fields_uwc_profile_field_skils',
        29 => 'field_fax',
        30 => 'commons_follow_user_followers_uwc_profile_followers_eva',
        31 => 'uwc_profile_fields_uwc_profile_field_topic_expertise',
        32 => 'field_personal_interests',
        33 => 'group_follows',
        34 => 'commons_follow_user_following_uwc_profile_following_eva',
        35 => 'subscribe_taxonomy_term_uwc_profile_following_topics',
        36 => 'group_groups',
        37 => 'uwc_user_groups_entity_view_1',
      ),
      'aside' => array(
        1 => 'member_overview_text',
        3 => 'field_profile_picture',
        4 => 'flag_commons_follow_user',
        5 => 'user_follow_count',
      ),
      'main_content' => array(
        2 => 'user_full_name',
        7 => 'field_intro_stmt',
      ),
      'left_content' => array(
        8 => 'field_primary_role',
        9 => 'field_primary_role_other',
        10 => 'field_uw_since',
      ),
      'right_content' => array(
        13 => 'field_current_position',
        20 => 'field_local_united_way',
      ),
    ),
    'fields' => array(
      'user_recent_activity' => 'footer',
      'member_overview_text' => 'aside',
      'user_full_name' => 'main_content',
      'field_profile_picture' => 'aside',
      'flag_commons_follow_user' => 'aside',
      'user_follow_count' => 'aside',
      'uwc_user_professional_positions_past_positions' => 'footer',
      'field_intro_stmt' => 'main_content',
      'field_primary_role' => 'left_content',
      'field_primary_role_other' => 'left_content',
      'field_uw_since' => 'left_content',
      'group_uw_positions' => 'footer',
      'group_contact' => 'footer',
      'field_current_position' => 'right_content',
      'user_email' => 'footer',
      'field_bio' => 'footer',
      'group_general_contact' => 'footer',
      'group_expertise' => 'footer',
      'group_area_ie' => 'footer',
      'group_social_media' => 'footer',
      'field_local_united_way' => 'right_content',
      'field_affiliations' => 'footer',
      'field_phone' => 'footer',
      'group_professional_profile' => 'footer',
      'field_social_media_contact' => 'footer',
      'group_activity_feed' => 'footer',
      'group_interests' => 'footer',
      'group_left_nav' => 'footer',
      'uwc_profile_fields_uwc_profile_field_skils' => 'footer',
      'field_fax' => 'footer',
      'commons_follow_user_followers_uwc_profile_followers_eva' => 'footer',
      'uwc_profile_fields_uwc_profile_field_topic_expertise' => 'footer',
      'field_personal_interests' => 'footer',
      'group_follows' => 'footer',
      'commons_follow_user_following_uwc_profile_following_eva' => 'footer',
      'subscribe_taxonomy_term_uwc_profile_following_topics' => 'footer',
      'group_groups' => 'footer',
      'uwc_user_groups_entity_view_1' => 'footer',
    ),
    'classes' => array(
      'layout_class' => array(
        '' => '',
      ),
    ),
    'wrappers' => array(
      'header' => 'div',
      'aside' => 'div',
      'main_content' => 'div',
      'left_content' => 'div',
      'right_content' => 'div',
      'footer' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
  );
  $export['profile2|main|account'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'profile2|main|compact_teaser';
  $ds_layout->entity_type = 'profile2';
  $ds_layout->bundle = 'main';
  $ds_layout->view_mode = 'compact_teaser';
  $ds_layout->layout = 'ds_1col';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'field_profile_picture',
        1 => 'user_full_name',
      ),
    ),
    'fields' => array(
      'field_profile_picture' => 'ds_content',
      'user_full_name' => 'ds_content',
    ),
    'classes' => array(),
    'wrappers' => array(
      'ds_content' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
  );
  $export['profile2|main|compact_teaser'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'profile2|main|default';
  $ds_layout->entity_type = 'profile2';
  $ds_layout->bundle = 'main';
  $ds_layout->view_mode = 'default';
  $ds_layout->layout = 'ds_1col';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'user_full_name',
        1 => 'field_bio',
        2 => 'field_profile_picture',
        3 => 'field_professional_information',
      ),
    ),
    'fields' => array(
      'user_full_name' => 'ds_content',
      'field_bio' => 'ds_content',
      'field_profile_picture' => 'ds_content',
      'field_professional_information' => 'ds_content',
    ),
    'classes' => array(
      'layout_class' => array(
        '' => '',
      ),
    ),
    'wrappers' => array(
      'ds_content' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
  );
  $export['profile2|main|default'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'profile2|main|related_content';
  $ds_layout->entity_type = 'profile2';
  $ds_layout->bundle = 'main';
  $ds_layout->view_mode = 'related_content';
  $ds_layout->layout = 'ds_1col';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'field_profile_picture',
      ),
    ),
    'fields' => array(
      'field_profile_picture' => 'ds_content',
    ),
    'classes' => array(
      'layout_class' => array(
        '' => '',
      ),
    ),
    'wrappers' => array(
      'ds_content' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
  );
  $export['profile2|main|related_content'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'profile2|main|search_result';
  $ds_layout->entity_type = 'profile2';
  $ds_layout->bundle = 'main';
  $ds_layout->view_mode = 'search_result';
  $ds_layout->layout = 'ds_2col';
  $ds_layout->settings = array(
    'regions' => array(
      'left' => array(
        0 => 'field_profile_picture',
      ),
      'right' => array(
        1 => 'user_full_name',
        2 => 'field_current_position',
        3 => 'field_primary_role',
        4 => 'field_local_united_way',
      ),
    ),
    'fields' => array(
      'field_profile_picture' => 'left',
      'user_full_name' => 'right',
      'field_current_position' => 'right',
      'field_primary_role' => 'right',
      'field_local_united_way' => 'right',
    ),
    'classes' => array(
      'layout_class' => array(
        '' => '',
      ),
      'left' => array(
        'teaser-left' => 'teaser-left',
      ),
      'right' => array(
        'teaser-right' => 'teaser-right',
      ),
    ),
    'wrappers' => array(
      'left' => 'div',
      'right' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
  );
  $export['profile2|main|search_result'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'profile2|main|teaser';
  $ds_layout->entity_type = 'profile2';
  $ds_layout->bundle = 'main';
  $ds_layout->view_mode = 'teaser';
  $ds_layout->layout = 'ds_1col';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'field_name_first',
        1 => 'field_name_last',
        2 => 'field_bio',
      ),
    ),
    'fields' => array(
      'field_name_first' => 'ds_content',
      'field_name_last' => 'ds_content',
      'field_bio' => 'ds_content',
    ),
    'classes' => array(),
    'wrappers' => array(
      'ds_content' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
  );
  $export['profile2|main|teaser'] = $ds_layout;

  return $export;
}
