<?php
/**
 * @file
 * uwc_users.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function uwc_users_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'profile2_edit';
  $context->description = '';
  $context->tag = '';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'user/*/edit/main' => 'user/*/edit/main',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'uwc_misc-profile2_edit_page_title' => array(
          'module' => 'uwc_misc',
          'delta' => 'profile2_edit_page_title',
          'region' => 'preface',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;
  $export['profile2_edit'] = $context;

  return $export;
}
