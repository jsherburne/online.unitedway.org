<?php
/**
 * @file
 * uwc_users.quicktabs.inc
 */

/**
 * Implements hook_quicktabs_default_quicktabs().
 */
function uwc_users_quicktabs_default_quicktabs() {
  $export = array();

  $quicktabs = new stdClass();
  $quicktabs->disabled = FALSE; /* Edit this to true to make a default quicktabs disabled initially */
  $quicktabs->api_version = 1;
  $quicktabs->machine_name = 'user_activity';
  $quicktabs->ajax = 0;
  $quicktabs->hide_empty_tabs = FALSE;
  $quicktabs->default_tab = 0;
  $quicktabs->title = 'User Activity';
  $quicktabs->tabs = array(
    0 => array(
      'vid' => 'commons_activity_streams_user_activity',
      'display' => 'panel_pane_3',
      'args' => '%1',
      'title' => 'All activity',
      'weight' => '-100',
      'type' => 'view',
    ),
    1 => array(
      'vid' => 'commons_activity_streams_user_activity',
      'display' => 'uwc_user_activity_likes_pane',
      'args' => '%1',
      'title' => 'Likes',
      'weight' => '-99',
      'type' => 'view',
    ),
    2 => array(
      'vid' => 'commons_activity_streams_user_activity',
      'display' => 'uwc_user_activity_comments_pane',
      'args' => '%1',
      'title' => 'Comments',
      'weight' => '-98',
      'type' => 'view',
    ),
  );
  $quicktabs->renderer = 'quicktabs';
  $quicktabs->style = 'default';
  $quicktabs->options = array();

  // Translatables
  // Included for use with string extractors like potx.
  t('All activity');
  t('Comments');
  t('Likes');
  t('User Activity');

  $export['user_activity'] = $quicktabs;

  $quicktabs = new stdClass();
  $quicktabs->disabled = FALSE; /* Edit this to true to make a default quicktabs disabled initially */
  $quicktabs->api_version = 1;
  $quicktabs->machine_name = 'user_following';
  $quicktabs->ajax = 0;
  $quicktabs->hide_empty_tabs = FALSE;
  $quicktabs->default_tab = 0;
  $quicktabs->title = 'User Following';
  $quicktabs->tabs = array(
    0 => array(
      'vid' => 'subscribe_taxonomy_term',
      'display' => 'default',
      'args' => '%1',
      'title' => 'Topics',
      'weight' => '-100',
      'type' => 'view',
    ),
    1 => array(
      'vid' => 'commons_follow_user_following',
      'display' => 'default',
      'args' => '%1',
      'title' => 'Users',
      'weight' => '-99',
      'type' => 'view',
    ),
  );
  $quicktabs->renderer = 'quicktabs';
  $quicktabs->style = 'default';
  $quicktabs->options = array();

  // Translatables
  // Included for use with string extractors like potx.
  t('Topics');
  t('User Following');
  t('Users');

  $export['user_following'] = $quicktabs;

  return $export;
}
