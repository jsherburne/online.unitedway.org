<?php
/**
 * @file
 * uwc_users.pm_existing_pages.inc
 */

/**
 * Implements hook_pm_existing_pages_info().
 */
function uwc_users_pm_existing_pages_info() {
  $export = array();

  $pm_existing_page = new stdClass();
  $pm_existing_page->api_version = 1;
  $pm_existing_page->name = 'login_pages';
  $pm_existing_page->label = 'Login pages';
  $pm_existing_page->context = '';
  $pm_existing_page->paths = 'user
user/login
user/password';
  $export['login_pages'] = $pm_existing_page;

  return $export;
}
