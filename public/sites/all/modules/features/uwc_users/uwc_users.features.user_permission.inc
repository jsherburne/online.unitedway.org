<?php
/**
 * @file
 * uwc_users.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function uwc_users_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'access private fields'.
  $permissions['access private fields'] = array(
    'name' => 'access private fields',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'administer field permissions'.
  $permissions['administer field permissions'] = array(
    'name' => 'administer field permissions',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'create field_current_position'.
  $permissions['create field_current_position'] = array(
    'name' => 'create field_current_position',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'create field_local_united_way'.
  $permissions['create field_local_united_way'] = array(
    'name' => 'create field_local_united_way',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'create field_name_first'.
  $permissions['create field_name_first'] = array(
    'name' => 'create field_name_first',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'create field_name_last'.
  $permissions['create field_name_last'] = array(
    'name' => 'create field_name_last',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'create field_name_suffix'.
  $permissions['create field_name_suffix'] = array(
    'name' => 'create field_name_suffix',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'create field_personal_title'.
  $permissions['create field_personal_title'] = array(
    'name' => 'create field_personal_title',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'create field_uw_since'.
  $permissions['create field_uw_since'] = array(
    'name' => 'create field_uw_since',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'edit field_current_position'.
  $permissions['edit field_current_position'] = array(
    'name' => 'edit field_current_position',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'edit field_local_united_way'.
  $permissions['edit field_local_united_way'] = array(
    'name' => 'edit field_local_united_way',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'edit field_name_first'.
  $permissions['edit field_name_first'] = array(
    'name' => 'edit field_name_first',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'edit field_name_last'.
  $permissions['edit field_name_last'] = array(
    'name' => 'edit field_name_last',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'edit field_name_suffix'.
  $permissions['edit field_name_suffix'] = array(
    'name' => 'edit field_name_suffix',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'edit field_personal_title'.
  $permissions['edit field_personal_title'] = array(
    'name' => 'edit field_personal_title',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'edit field_uw_since'.
  $permissions['edit field_uw_since'] = array(
    'name' => 'edit field_uw_since',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'edit own field_current_position'.
  $permissions['edit own field_current_position'] = array(
    'name' => 'edit own field_current_position',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'edit own field_local_united_way'.
  $permissions['edit own field_local_united_way'] = array(
    'name' => 'edit own field_local_united_way',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'edit own field_name_first'.
  $permissions['edit own field_name_first'] = array(
    'name' => 'edit own field_name_first',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'edit own field_name_last'.
  $permissions['edit own field_name_last'] = array(
    'name' => 'edit own field_name_last',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'edit own field_name_suffix'.
  $permissions['edit own field_name_suffix'] = array(
    'name' => 'edit own field_name_suffix',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'edit own field_personal_title'.
  $permissions['edit own field_personal_title'] = array(
    'name' => 'edit own field_personal_title',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'edit own field_uw_since'.
  $permissions['edit own field_uw_since'] = array(
    'name' => 'edit own field_uw_since',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'view field_current_position'.
  $permissions['view field_current_position'] = array(
    'name' => 'view field_current_position',
    'roles' => array(
      'administrator' => 'administrator',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'view field_local_united_way'.
  $permissions['view field_local_united_way'] = array(
    'name' => 'view field_local_united_way',
    'roles' => array(
      'administrator' => 'administrator',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'view field_name_first'.
  $permissions['view field_name_first'] = array(
    'name' => 'view field_name_first',
    'roles' => array(
      'administrator' => 'administrator',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'view field_name_last'.
  $permissions['view field_name_last'] = array(
    'name' => 'view field_name_last',
    'roles' => array(
      'administrator' => 'administrator',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'view field_name_suffix'.
  $permissions['view field_name_suffix'] = array(
    'name' => 'view field_name_suffix',
    'roles' => array(
      'administrator' => 'administrator',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'view field_personal_title'.
  $permissions['view field_personal_title'] = array(
    'name' => 'view field_personal_title',
    'roles' => array(
      'administrator' => 'administrator',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'view field_uw_since'.
  $permissions['view field_uw_since'] = array(
    'name' => 'view field_uw_since',
    'roles' => array(
      'administrator' => 'administrator',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'view own field_current_position'.
  $permissions['view own field_current_position'] = array(
    'name' => 'view own field_current_position',
    'roles' => array(
      'administrator' => 'administrator',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'view own field_local_united_way'.
  $permissions['view own field_local_united_way'] = array(
    'name' => 'view own field_local_united_way',
    'roles' => array(
      'administrator' => 'administrator',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'view own field_name_first'.
  $permissions['view own field_name_first'] = array(
    'name' => 'view own field_name_first',
    'roles' => array(
      'administrator' => 'administrator',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'view own field_name_last'.
  $permissions['view own field_name_last'] = array(
    'name' => 'view own field_name_last',
    'roles' => array(
      'administrator' => 'administrator',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'view own field_name_suffix'.
  $permissions['view own field_name_suffix'] = array(
    'name' => 'view own field_name_suffix',
    'roles' => array(
      'administrator' => 'administrator',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'view own field_personal_title'.
  $permissions['view own field_personal_title'] = array(
    'name' => 'view own field_personal_title',
    'roles' => array(
      'administrator' => 'administrator',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'view own field_uw_since'.
  $permissions['view own field_uw_since'] = array(
    'name' => 'view own field_uw_since',
    'roles' => array(
      'administrator' => 'administrator',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'field_permissions',
  );

  return $permissions;
}
