<?php
/**
 * @file
 * uwc_users.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function uwc_users_field_group_info() {
  $export = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_activity_feed|profile2|main|account';
  $field_group->group_name = 'group_activity_feed';
  $field_group->entity_type = 'profile2';
  $field_group->bundle = 'main';
  $field_group->mode = 'account';
  $field_group->parent_name = 'group_left_nav';
  $field_group->data = array(
    'label' => 'Connect activity feed',
    'weight' => '10',
    'children' => array(
      0 => 'user_recent_activity',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-activity-feed field-group-tab',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_activity_feed|profile2|main|account'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_activity_feed|profile2|main|default';
  $field_group->group_name = 'group_activity_feed';
  $field_group->entity_type = 'profile2';
  $field_group->bundle = 'main';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Connect activity feed',
    'weight' => '13',
    'children' => array(),
    'format_type' => 'tab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-activity-feed field-group-tab',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_activity_feed|profile2|main|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_activity_feed|profile2|main|form';
  $field_group->group_name = 'group_activity_feed';
  $field_group->entity_type = 'profile2';
  $field_group->bundle = 'main';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_left_nav';
  $field_group->data = array(
    'label' => 'Connect Activity Feed',
    'weight' => '5',
    'children' => array(),
    'format_type' => 'tab',
    'format_settings' => array(
      'label' => 'Connect Activity Feed',
      'instance_settings' => array(
        'required_fields' => 1,
        'classes' => 'group-activity-feed field-group-tab',
        'description' => '',
      ),
      'formatter' => 'closed',
    ),
  );
  $export['group_activity_feed|profile2|main|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_area_ie|profile2|main|account';
  $field_group->group_name = 'group_area_ie';
  $field_group->entity_type = 'profile2';
  $field_group->bundle = 'main';
  $field_group->mode = 'account';
  $field_group->parent_name = 'group_left_nav';
  $field_group->data = array(
    'label' => 'Interests & expertise',
    'weight' => '8',
    'children' => array(
      0 => 'group_expertise',
      1 => 'group_interests',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-area-ie field-group-tab',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_area_ie|profile2|main|account'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_area_ie|profile2|main|default';
  $field_group->group_name = 'group_area_ie';
  $field_group->entity_type = 'profile2';
  $field_group->bundle = 'main';
  $field_group->mode = 'default';
  $field_group->parent_name = 'group_left_nav';
  $field_group->data = array(
    'label' => 'Interests & expertise',
    'weight' => '8',
    'children' => array(
      0 => 'group_expertise',
      1 => 'group_interests',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-area-ie field-group-tab',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_area_ie|profile2|main|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_area_ie|profile2|main|form';
  $field_group->group_name = 'group_area_ie';
  $field_group->entity_type = 'profile2';
  $field_group->bundle = 'main';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_left_nav';
  $field_group->data = array(
    'label' => 'Interests & Expertise',
    'weight' => '3',
    'children' => array(
      0 => 'group_expertise',
      1 => 'group_interests',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'label' => 'Interests & Expertise',
      'instance_settings' => array(
        'required_fields' => 1,
        'classes' => 'group-area-ie field-group-tab',
        'description' => '',
      ),
      'formatter' => 'closed',
    ),
  );
  $export['group_area_ie|profile2|main|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_contact|profile2|main|account';
  $field_group->group_name = 'group_contact';
  $field_group->entity_type = 'profile2';
  $field_group->bundle = 'main';
  $field_group->mode = 'account';
  $field_group->parent_name = 'group_left_nav';
  $field_group->data = array(
    'label' => 'Contact information',
    'weight' => '7',
    'children' => array(
      0 => 'group_general_contact',
      1 => 'group_social_media',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-contact field-group-tab',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_contact|profile2|main|account'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_contact|profile2|main|default';
  $field_group->group_name = 'group_contact';
  $field_group->entity_type = 'profile2';
  $field_group->bundle = 'main';
  $field_group->mode = 'default';
  $field_group->parent_name = 'group_left_nav';
  $field_group->data = array(
    'label' => 'Contact information',
    'weight' => '7',
    'children' => array(
      0 => 'group_general_contact',
      1 => 'group_social_media',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-contact field-group-tab',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_contact|profile2|main|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_contact|profile2|main|form';
  $field_group->group_name = 'group_contact';
  $field_group->entity_type = 'profile2';
  $field_group->bundle = 'main';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_left_nav';
  $field_group->data = array(
    'label' => 'Contact Information',
    'weight' => '2',
    'children' => array(
      0 => 'group_general_contact',
      1 => 'group_social_media',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'label' => 'Contact Information',
      'instance_settings' => array(
        'required_fields' => 1,
        'classes' => 'group-contact field-group-tab',
        'description' => '',
      ),
      'formatter' => 'closed',
    ),
  );
  $export['group_contact|profile2|main|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_expertise|profile2|main|account';
  $field_group->group_name = 'group_expertise';
  $field_group->entity_type = 'profile2';
  $field_group->bundle = 'main';
  $field_group->mode = 'account';
  $field_group->parent_name = 'group_area_ie';
  $field_group->data = array(
    'label' => 'My expertise',
    'weight' => '8',
    'children' => array(
      0 => 'uwc_profile_fields_uwc_profile_field_topic_expertise',
      1 => 'uwc_profile_fields_uwc_profile_field_skils',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'My expertise',
      'instance_settings' => array(
        'classes' => 'group-expertise field-group-div',
        'description' => '',
        'show_label' => '1',
        'label_element' => 'h2',
        'effect' => 'none',
        'speed' => 'fast',
      ),
      'formatter' => 'open',
    ),
  );
  $export['group_expertise|profile2|main|account'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_expertise|profile2|main|default';
  $field_group->group_name = 'group_expertise';
  $field_group->entity_type = 'profile2';
  $field_group->bundle = 'main';
  $field_group->mode = 'default';
  $field_group->parent_name = 'group_area_ie';
  $field_group->data = array(
    'label' => 'My expertise',
    'weight' => '8',
    'children' => array(),
    'format_type' => 'div',
    'format_settings' => array(
      'formatter' => 'open',
      'instance_settings' => array(
        'description' => '',
        'show_label' => 1,
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'fast',
        'classes' => 'group-expertise field-group-div',
        'required_fields' => 1,
        'id' => '',
      ),
    ),
  );
  $export['group_expertise|profile2|main|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_expertise|profile2|main|form';
  $field_group->group_name = 'group_expertise';
  $field_group->entity_type = 'profile2';
  $field_group->bundle = 'main';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_area_ie';
  $field_group->data = array(
    'label' => 'My Expertise',
    'weight' => '37',
    'children' => array(
      0 => 'field_skills',
      1 => 'field_topic_expertise',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'My Expertise',
      'instance_settings' => array(
        'required_fields' => 1,
        'id' => '',
        'classes' => 'group-expertise field-group-div',
        'description' => '',
        'show_label' => '1',
        'label_element' => 'h2',
        'effect' => 'none',
        'speed' => 'fast',
      ),
      'formatter' => 'open',
    ),
  );
  $export['group_expertise|profile2|main|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_follows|profile2|main|account';
  $field_group->group_name = 'group_follows';
  $field_group->entity_type = 'profile2';
  $field_group->bundle = 'main';
  $field_group->mode = 'account';
  $field_group->parent_name = 'group_left_nav';
  $field_group->data = array(
    'label' => 'Followers & following',
    'weight' => '11',
    'children' => array(
      0 => 'commons_follow_user_followers_uwc_profile_followers_eva',
      1 => 'commons_follow_user_following_uwc_profile_following_eva',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-follows field-group-tab',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_follows|profile2|main|account'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_follows|profile2|main|default';
  $field_group->group_name = 'group_follows';
  $field_group->entity_type = 'profile2';
  $field_group->bundle = 'main';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Followers & following',
    'weight' => '14',
    'children' => array(),
    'format_type' => 'tab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-follows field-group-tab',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_follows|profile2|main|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_follows|profile2|main|form';
  $field_group->group_name = 'group_follows';
  $field_group->entity_type = 'profile2';
  $field_group->bundle = 'main';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_left_nav';
  $field_group->data = array(
    'label' => 'Followers & Following',
    'weight' => '6',
    'children' => array(),
    'format_type' => 'tab',
    'format_settings' => array(
      'label' => 'Followers & Following',
      'instance_settings' => array(
        'required_fields' => 1,
        'classes' => 'group-follows field-group-tab',
        'description' => '',
      ),
      'formatter' => 'closed',
    ),
  );
  $export['group_follows|profile2|main|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_general_contact|profile2|main|account';
  $field_group->group_name = 'group_general_contact';
  $field_group->entity_type = 'profile2';
  $field_group->bundle = 'main';
  $field_group->mode = 'account';
  $field_group->parent_name = 'group_contact';
  $field_group->data = array(
    'label' => 'General contact information',
    'weight' => '8',
    'children' => array(
      0 => 'field_fax',
      1 => 'field_phone',
      2 => 'user_email',
    ),
    'format_type' => 'table',
    'format_settings' => array(
      'label' => 'General contact information',
      'instance_settings' => array(
        'label_visibility' => '3',
        'desc' => '',
        'first_column' => '',
        'second_column' => '',
        'empty_label_behavior' => '1',
        'table_row_striping' => 1,
        'always_show_field_label' => 0,
        'classes' => 'group-general-contact field-group-div field-group-table',
      ),
    ),
  );
  $export['group_general_contact|profile2|main|account'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_general_contact|profile2|main|default';
  $field_group->group_name = 'group_general_contact';
  $field_group->entity_type = 'profile2';
  $field_group->bundle = 'main';
  $field_group->mode = 'default';
  $field_group->parent_name = 'group_contact';
  $field_group->data = array(
    'label' => 'General contact information',
    'weight' => '8',
    'children' => array(),
    'format_type' => 'div',
    'format_settings' => array(
      'formatter' => 'open',
      'instance_settings' => array(
        'description' => '',
        'show_label' => 1,
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'fast',
        'classes' => 'group-general-contact field-group-div',
        'required_fields' => 1,
        'id' => '',
      ),
    ),
  );
  $export['group_general_contact|profile2|main|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_general_contact|profile2|main|form';
  $field_group->group_name = 'group_general_contact';
  $field_group->entity_type = 'profile2';
  $field_group->bundle = 'main';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_contact';
  $field_group->data = array(
    'label' => 'General Contact Information',
    'weight' => '37',
    'children' => array(
      0 => 'field_fax',
      1 => 'field_phone',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'General Contact Information',
      'instance_settings' => array(
        'required_fields' => 1,
        'id' => '',
        'classes' => 'group-general-contact field-group-div',
        'description' => '',
        'show_label' => '1',
        'label_element' => 'h2',
        'effect' => 'none',
        'speed' => 'fast',
      ),
      'formatter' => 'open',
    ),
  );
  $export['group_general_contact|profile2|main|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_groups|profile2|main|account';
  $field_group->group_name = 'group_groups';
  $field_group->entity_type = 'profile2';
  $field_group->bundle = 'main';
  $field_group->mode = 'account';
  $field_group->parent_name = 'group_left_nav';
  $field_group->data = array(
    'label' => 'Groups',
    'weight' => '12',
    'children' => array(
      0 => 'uwc_user_groups_entity_view_1',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-groups field-group-tab',
      ),
    ),
  );
  $export['group_groups|profile2|main|account'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_interests|profile2|main|account';
  $field_group->group_name = 'group_interests';
  $field_group->entity_type = 'profile2';
  $field_group->bundle = 'main';
  $field_group->mode = 'account';
  $field_group->parent_name = 'group_area_ie';
  $field_group->data = array(
    'label' => 'My interests',
    'weight' => '10',
    'children' => array(
      0 => 'field_personal_interests',
      1 => 'subscribe_taxonomy_term_uwc_profile_following_topics',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'My interests',
      'instance_settings' => array(
        'classes' => 'group-interests field-group-div',
        'description' => '',
        'show_label' => '1',
        'label_element' => 'h2',
        'effect' => 'none',
        'speed' => 'fast',
      ),
      'formatter' => 'open',
    ),
  );
  $export['group_interests|profile2|main|account'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_interests|profile2|main|default';
  $field_group->group_name = 'group_interests';
  $field_group->entity_type = 'profile2';
  $field_group->bundle = 'main';
  $field_group->mode = 'default';
  $field_group->parent_name = 'group_area_ie';
  $field_group->data = array(
    'label' => 'My interests',
    'weight' => '9',
    'children' => array(),
    'format_type' => 'div',
    'format_settings' => array(
      'formatter' => 'open',
      'instance_settings' => array(
        'description' => '',
        'show_label' => 1,
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'fast',
        'classes' => 'group-interests field-group-div',
        'required_fields' => 1,
        'id' => '',
      ),
    ),
  );
  $export['group_interests|profile2|main|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_interests|profile2|main|form';
  $field_group->group_name = 'group_interests';
  $field_group->entity_type = 'profile2';
  $field_group->bundle = 'main';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_area_ie';
  $field_group->data = array(
    'label' => 'My Interests',
    'weight' => '38',
    'children' => array(
      0 => 'field_personal_interests',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'My Interests',
      'instance_settings' => array(
        'required_fields' => 1,
        'id' => '',
        'classes' => 'group-interests field-group-div',
        'description' => '',
        'show_label' => '1',
        'label_element' => 'h2',
        'effect' => 'none',
        'speed' => 'fast',
      ),
      'formatter' => 'open',
    ),
  );
  $export['group_interests|profile2|main|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_left_nav|profile2|main|account';
  $field_group->group_name = 'group_left_nav';
  $field_group->entity_type = 'profile2';
  $field_group->bundle = 'main';
  $field_group->mode = 'account';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Left nav',
    'weight' => '10',
    'children' => array(
      0 => 'group_activity_feed',
      1 => 'group_area_ie',
      2 => 'group_contact',
      3 => 'group_follows',
      4 => 'group_groups',
      5 => 'group_professional_profile',
    ),
    'format_type' => 'tabs',
    'format_settings' => array(
      'label' => 'Left nav',
      'instance_settings' => array(
        'classes' => 'group-left-nav field-group-tabs tabs-purple',
      ),
    ),
  );
  $export['group_left_nav|profile2|main|account'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_left_nav|profile2|main|default';
  $field_group->group_name = 'group_left_nav';
  $field_group->entity_type = 'profile2';
  $field_group->bundle = 'main';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Left nav',
    'weight' => '6',
    'children' => array(
      0 => 'group_area_ie',
      1 => 'group_contact',
      2 => 'group_professional_profile',
    ),
    'format_type' => 'tabs',
    'format_settings' => array(
      'formatter' => '',
      'instance_settings' => array(
        'classes' => 'group-left-nav field-group-tabs',
      ),
    ),
  );
  $export['group_left_nav|profile2|main|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_left_nav|profile2|main|form';
  $field_group->group_name = 'group_left_nav';
  $field_group->entity_type = 'profile2';
  $field_group->bundle = 'main';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Left nav',
    'weight' => '0',
    'children' => array(
      0 => 'group_activity_feed',
      1 => 'group_area_ie',
      2 => 'group_contact',
      3 => 'group_follows',
      4 => 'group_member_overview',
      5 => 'group_professional_profile',
    ),
    'format_type' => 'tabs',
    'format_settings' => array(
      'formatter' => '',
      'instance_settings' => array(
        'classes' => 'group-left-nav field-group-tabs',
      ),
    ),
  );
  $export['group_left_nav|profile2|main|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_member_overview|profile2|main|form';
  $field_group->group_name = 'group_member_overview';
  $field_group->entity_type = 'profile2';
  $field_group->bundle = 'main';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_left_nav';
  $field_group->data = array(
    'label' => 'Member Overview',
    'weight' => '1',
    'children' => array(
      0 => 'field_name_first',
      1 => 'field_name_last',
      2 => 'field_name_suffix',
      3 => 'field_personal_title',
      4 => 'field_profile_picture',
      5 => 'field_current_position',
      6 => 'field_local_united_way',
      7 => 'field_primary_role',
      8 => 'field_primary_role_other',
      9 => 'field_uw_since',
      10 => 'field_intro_stmt',
      11 => 'field_receive_breakfast',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => ' group-member-overview field-group-tab',
      ),
    ),
  );
  $export['group_member_overview|profile2|main|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_professional_profile|profile2|main|account';
  $field_group->group_name = 'group_professional_profile';
  $field_group->entity_type = 'profile2';
  $field_group->bundle = 'main';
  $field_group->mode = 'account';
  $field_group->parent_name = 'group_left_nav';
  $field_group->data = array(
    'label' => 'Professional profile',
    'weight' => '9',
    'children' => array(
      0 => 'field_bio',
      1 => 'field_affiliations',
      2 => 'group_uw_positions',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-professional-profile field-group-tab',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_professional_profile|profile2|main|account'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_professional_profile|profile2|main|default';
  $field_group->group_name = 'group_professional_profile';
  $field_group->entity_type = 'profile2';
  $field_group->bundle = 'main';
  $field_group->mode = 'default';
  $field_group->parent_name = 'group_left_nav';
  $field_group->data = array(
    'label' => 'Professional profile',
    'weight' => '9',
    'children' => array(
      0 => 'group_uw_positions',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-professional-profile field-group-tab',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_professional_profile|profile2|main|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_professional_profile|profile2|main|form';
  $field_group->group_name = 'group_professional_profile';
  $field_group->entity_type = 'profile2';
  $field_group->bundle = 'main';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_left_nav';
  $field_group->data = array(
    'label' => 'Professional Profile',
    'weight' => '4',
    'children' => array(
      0 => 'field_bio',
      1 => 'field_affiliations',
      2 => 'group_uw_positions',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'label' => 'Professional Profile',
      'instance_settings' => array(
        'required_fields' => 1,
        'classes' => 'group-professional-profile field-group-tab',
        'description' => '',
      ),
      'formatter' => 'closed',
    ),
  );
  $export['group_professional_profile|profile2|main|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_social_media|profile2|main|account';
  $field_group->group_name = 'group_social_media';
  $field_group->entity_type = 'profile2';
  $field_group->bundle = 'main';
  $field_group->mode = 'account';
  $field_group->parent_name = 'group_contact';
  $field_group->data = array(
    'label' => 'Social media contact information',
    'weight' => '9',
    'children' => array(
      0 => 'field_social_media_contact',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'Social media contact information',
      'instance_settings' => array(
        'classes' => 'group-social-media field-group-div',
        'description' => '',
        'show_label' => '1',
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'fast',
      ),
      'formatter' => 'open',
    ),
  );
  $export['group_social_media|profile2|main|account'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_social_media|profile2|main|default';
  $field_group->group_name = 'group_social_media';
  $field_group->entity_type = 'profile2';
  $field_group->bundle = 'main';
  $field_group->mode = 'default';
  $field_group->parent_name = 'group_contact';
  $field_group->data = array(
    'label' => 'Social media contact information',
    'weight' => '9',
    'children' => array(
      0 => 'field_social_media_contact',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'Social media contact information',
      'instance_settings' => array(
        'classes' => 'group-social-media field-group-div',
        'description' => '',
        'show_label' => '1',
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'none',
      ),
      'formatter' => 'open',
    ),
  );
  $export['group_social_media|profile2|main|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_social_media|profile2|main|form';
  $field_group->group_name = 'group_social_media';
  $field_group->entity_type = 'profile2';
  $field_group->bundle = 'main';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_contact';
  $field_group->data = array(
    'label' => 'Social Media Contact Information',
    'weight' => '38',
    'children' => array(
      0 => 'field_social_media_contact',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'Social Media Contact Information',
      'instance_settings' => array(
        'required_fields' => 1,
        'id' => '',
        'classes' => ' group-social-media field-group-div',
        'description' => '',
        'show_label' => '1',
        'label_element' => 'h2',
        'effect' => 'none',
        'speed' => 'fast',
      ),
      'formatter' => 'open',
    ),
  );
  $export['group_social_media|profile2|main|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_uw_positions|profile2|main|account';
  $field_group->group_name = 'group_uw_positions';
  $field_group->entity_type = 'profile2';
  $field_group->bundle = 'main';
  $field_group->mode = 'account';
  $field_group->parent_name = 'group_professional_profile';
  $field_group->data = array(
    'label' => 'My past positions with united way',
    'weight' => '7',
    'children' => array(
      0 => 'uwc_user_professional_positions_past_positions',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'formatter' => 'open',
      'instance_settings' => array(
        'description' => '',
        'show_label' => 1,
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'fast',
        'classes' => 'group-uw-positions field-group-div',
        'required_fields' => 1,
        'id' => '',
      ),
    ),
  );
  $export['group_uw_positions|profile2|main|account'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_uw_positions|profile2|main|default';
  $field_group->group_name = 'group_uw_positions';
  $field_group->entity_type = 'profile2';
  $field_group->bundle = 'main';
  $field_group->mode = 'default';
  $field_group->parent_name = 'group_professional_profile';
  $field_group->data = array(
    'label' => 'My past positions with united way',
    'weight' => '7',
    'children' => array(),
    'format_type' => 'div',
    'format_settings' => array(
      'formatter' => 'open',
      'instance_settings' => array(
        'description' => '',
        'show_label' => 1,
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'fast',
        'classes' => 'group-uw-positions field-group-div',
        'required_fields' => 1,
        'id' => '',
      ),
    ),
  );
  $export['group_uw_positions|profile2|main|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_uw_positions|profile2|main|form';
  $field_group->group_name = 'group_uw_positions';
  $field_group->entity_type = 'profile2';
  $field_group->bundle = 'main';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_professional_profile';
  $field_group->data = array(
    'label' => 'My Past Positions with United Way',
    'weight' => '23',
    'children' => array(
      0 => 'field_professional_information',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'My Past Positions with United Way',
      'instance_settings' => array(
        'required_fields' => 1,
        'id' => '',
        'classes' => 'group-uw-positions field-group-div',
        'description' => '',
        'show_label' => '1',
        'label_element' => 'h2',
        'effect' => 'none',
        'speed' => 'fast',
      ),
      'formatter' => 'open',
    ),
  );
  $export['group_uw_positions|profile2|main|form'] = $field_group;

  return $export;
}
