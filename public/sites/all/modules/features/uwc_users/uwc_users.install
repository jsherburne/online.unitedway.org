<?php
/**
 * @file
 * Installation and update functionality for uwc_users.
 */

/**
 * Implements hook_install().
 */
function uwc_users_install() {
  // Disable core user pictures
  variable_set('user_pictures', '0');

  // Reset the user default picture path to our image
  variable_set('user_picture_default', drupal_get_path('module', 'uwc_users') . '/images/default_user.png');
}

/**
 * Add user organization field to profile.
 */
function uwc_users_update_7000() {
  features_revert_module('uwc_users');
}

/**
 * Disable and uninstall profile2_page module
 */
function uwc_users_update_7001() {
  module_disable(array('profile2_page'), TRUE);
  drupal_uninstall_modules(array('profile2_page'), TRUE);
}

/**
 * Remove old organization fields from the user profile
 */
function uwc_users_update_7002() {
  field_delete_instance(field_info_instance('profile2','uwc_organization','main'));
  field_delete_instance(field_info_instance('user','uwc_organization','user'));
}

/**
 * Add Field Collection Table module
 */
function uwc_users_update_7003() {
  module_enable(array('field_collection_table'), TRUE);
  features_revert_module('uwc_users');
}

/**
 * [#17579] Add EVA module and adjust profile page settings.
 */
function uwc_users_update_7004() {
  module_enable(array('eva'), TRUE);
  features_revert_module('uwc_users');
}

/**
 * [#17567] Revert uwc_users to reset views and active_employment field settings
 */
function uwc_users_update_7005() {
  features_revert_module('uwc_users');
}

/**
 * [#17579] Revert uwc_users to enable new full name fields.
 */
function uwc_users_update_7006() {
  features_revert_module('uwc_users');
}

/**
 * [#18377] Ensure user pictures (avatars) are disabled in favor of profile2 pictures.
 */
function uwc_users_update_7007() {
  variable_set('user_pictures', '0');
}

/**
 * [#18492] Revert after adding "follow" link to user pages.
 */
function uwc_users_update_7008() {
  features_revert_module('uwc_users');
}

/**
 * [#18753] Update user default picture path to use our image.
 */
function uwc_users_update_7009() {
  // Reset the user default picture path to our image
  variable_set('user_picture_default', drupal_get_path('module', 'uwc_users') . '/images/default_user.png');
}

/**
 * [#23788] Update user profile fileds.
 */
function uwc_users_update_7010() {
  $fieldsToDelete = array(
    'field_active_employment',
    'field_facebook_url',
    'field_linkedin_url',
    'field_twitter_url',
    'field_contact_information',
  );

  foreach($fieldsToDelete AS $fieldName) {
    $field = field_info_field($fieldName);
    if(!is_null($field)) {
      field_delete_field($fieldName);
    }
  }

  // Do a pass of purging on deleted Field API data, if any exists.
  $limit = variable_get('field_purge_batch_size', 10);
  field_purge_batch($limit);

  //features_revert_module('uwc_users');
}

/**
 * Replace field_intro_statement with field_intro_stmt
 */
function uwc_users_update_7011() {
  $field = field_info_field('field_intro_statement');
  if (!is_null($field)) {
    field_delete_field('field_intro_statement');
    field_purge_batch('10');
  }
}

