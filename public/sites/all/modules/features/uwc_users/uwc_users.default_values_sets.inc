<?php
/**
 * @file
 * uwc_users.default_values_sets.inc
 */

/**
 * Implements hook_default_values_values().
 */
function uwc_users_default_values_values() {
  $export = array();

  $values = new stdClass();
  $values->disabled = FALSE; /* Edit this to true to make a default values disabled initially */
  $values->api_version = 1;
  $values->name = 'contact_type';
  $values->title = 'Contact Type';
  $values->description = 'Types of contact';
  $values->data = array(
    0 => array(
      'key' => 'phone',
      'value' => 'Phone',
      'weight' => '0',
    ),
    1 => array(
      'key' => 'email',
      'value' => 'Email',
      'weight' => '1',
    ),
    2 => array(
      'key' => 'fax',
      'value' => 'Fax',
      'weight' => '2',
    ),
  );
  $export['contact_type'] = $values;

  $values = new stdClass();
  $values->disabled = FALSE; /* Edit this to true to make a default values disabled initially */
  $values->api_version = 1;
  $values->name = 'personal_title';
  $values->title = 'Personal Title';
  $values->description = 'Titles for a person (e.g. Mr., Mrs., etc.)';
  $values->data = array(
    0 => array(
      'key' => 'mr',
      'value' => 'Mr.',
      'weight' => '-10',
    ),
    1 => array(
      'key' => 'mrs',
      'value' => 'Mrs.',
      'weight' => '-9',
    ),
    2 => array(
      'key' => 'ms',
      'value' => 'Ms.',
      'weight' => '-8',
    ),
    3 => array(
      'key' => 'dr',
      'value' => 'Dr.',
      'weight' => '-7',
    ),
    4 => array(
      'key' => 'sen',
      'value' => 'Sen.',
      'weight' => '-6',
    ),
    5 => array(
      'key' => 'rep',
      'value' => 'Rep.',
      'weight' => '-5',
    ),
    6 => array(
      'key' => 'gen',
      'value' => 'Gen.',
      'weight' => '-4',
    ),
    7 => array(
      'key' => 'rev',
      'value' => 'Rev.',
      'weight' => '-3',
    ),
    8 => array(
      'key' => 'fr',
      'value' => 'Fr.',
      'weight' => '-2',
    ),
  );
  $export['personal_title'] = $values;

  return $export;
}
