<?php
/**
 * @file
 * uwc_users.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function uwc_users_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'date_format_uwc_month_year';
  $strongarm->value = 'M Y';
  $export['date_format_uwc_month_year'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'date_format_year';
  $strongarm->value = 'Y';
  $export['date_format_year'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'field_bundle_settings_profile2__main';
  $strongarm->value = array(
    'view_modes' => array(
      'teaser' => array(
        'custom_settings' => TRUE,
      ),
      'search_result' => array(
        'custom_settings' => TRUE,
      ),
      'account' => array(
        'custom_settings' => TRUE,
      ),
      'diff_standard' => array(
        'custom_settings' => FALSE,
      ),
      'page' => array(
        'custom_settings' => FALSE,
      ),
      'token' => array(
        'custom_settings' => FALSE,
      ),
      'compact_teaser' => array(
        'custom_settings' => TRUE,
      ),
      'related_content' => array(
        'custom_settings' => TRUE,
      ),
    ),
    'extra_fields' => array(
      'form' => array(
        'redirect' => array(
          'weight' => '9',
        ),
      ),
      'display' => array(
        'uwc_user_professional_positions_active_positions' => array(
          'account' => array(
            'weight' => '15',
            'visible' => FALSE,
          ),
          'search_result' => array(
            'weight' => '22',
            'visible' => FALSE,
          ),
          'default' => array(
            'weight' => '11',
            'visible' => FALSE,
          ),
          'compact_teaser' => array(
            'weight' => '13',
            'visible' => FALSE,
          ),
          'related_content' => array(
            'weight' => '11',
            'visible' => FALSE,
          ),
        ),
        'uwc_user_professional_positions_past_positions' => array(
          'account' => array(
            'weight' => '4',
            'visible' => TRUE,
          ),
          'default' => array(
            'weight' => '9',
            'visible' => FALSE,
          ),
          'search_result' => array(
            'weight' => '33',
            'visible' => FALSE,
          ),
          'compact_teaser' => array(
            'weight' => '8',
            'visible' => FALSE,
          ),
          'related_content' => array(
            'weight' => '4',
            'visible' => FALSE,
          ),
        ),
        'uwc_user_professional_positions_entity_view_1' => array(
          'search_result' => array(
            'weight' => '11',
            'visible' => FALSE,
          ),
          'account' => array(
            'weight' => '21',
            'visible' => FALSE,
          ),
          'default' => array(
            'weight' => '10',
            'visible' => FALSE,
          ),
          'compact_teaser' => array(
            'weight' => '12',
            'visible' => FALSE,
          ),
          'related_content' => array(
            'weight' => '10',
            'visible' => FALSE,
          ),
        ),
        'og_user_groups_entity_view_1' => array(
          'account' => array(
            'weight' => '10',
            'visible' => TRUE,
          ),
          'compact_teaser' => NULL,
          'related_content' => NULL,
        ),
        'og_user_groups_uwc_profile_og_user_groups' => array(
          'account' => array(
            'weight' => '10',
            'visible' => TRUE,
          ),
          'compact_teaser' => NULL,
          'related_content' => NULL,
        ),
        'subscribe_taxonomy_term_uwc_profile_following_topics' => array(
          'account' => array(
            'weight' => '11',
            'visible' => TRUE,
          ),
          'default' => array(
            'weight' => '10',
            'visible' => FALSE,
          ),
          'compact_teaser' => array(
            'weight' => '9',
            'visible' => FALSE,
          ),
          'search_result' => array(
            'weight' => '31',
            'visible' => FALSE,
          ),
          'related_content' => array(
            'weight' => '7',
            'visible' => FALSE,
          ),
        ),
        'commons_follow_user_followers_uwc_profile_followers_eva' => array(
          'account' => array(
            'weight' => '10',
            'visible' => TRUE,
          ),
          'default' => array(
            'weight' => '10',
            'visible' => FALSE,
          ),
          'compact_teaser' => array(
            'weight' => '10',
            'visible' => FALSE,
          ),
          'search_result' => array(
            'weight' => '30',
            'visible' => FALSE,
          ),
          'related_content' => array(
            'weight' => '5',
            'visible' => FALSE,
          ),
        ),
        'commons_follow_user_following_uwc_profile_following_eva' => array(
          'account' => array(
            'weight' => '11',
            'visible' => TRUE,
          ),
          'default' => array(
            'weight' => '10',
            'visible' => FALSE,
          ),
          'compact_teaser' => array(
            'weight' => '11',
            'visible' => FALSE,
          ),
          'search_result' => array(
            'weight' => '29',
            'visible' => FALSE,
          ),
          'related_content' => array(
            'weight' => '6',
            'visible' => FALSE,
          ),
        ),
        'uwc_profile_fields_uwc_profile_field_topic_expertise' => array(
          'account' => array(
            'weight' => '10',
            'visible' => TRUE,
          ),
          'search_result' => array(
            'weight' => '28',
            'visible' => FALSE,
          ),
          'default' => array(
            'weight' => '10',
            'visible' => FALSE,
          ),
          'related_content' => array(
            'weight' => '9',
            'visible' => FALSE,
          ),
        ),
        'uwc_profile_fields_uwc_profile_field_skils' => array(
          'account' => array(
            'weight' => '10',
            'visible' => TRUE,
          ),
          'search_result' => array(
            'weight' => '32',
            'visible' => FALSE,
          ),
          'default' => array(
            'weight' => '10',
            'visible' => FALSE,
          ),
          'related_content' => array(
            'weight' => '8',
            'visible' => FALSE,
          ),
        ),
        'uwc_user_groups_entity_view_1' => array(
          'account' => array(
            'weight' => '10',
            'visible' => TRUE,
          ),
        ),
      ),
    ),
  );
  $export['field_bundle_settings_profile2__main'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'page_manager_user_view_disabled';
  $strongarm->value = FALSE;
  $export['page_manager_user_view_disabled'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'panelizer_defaults_user_user';
  $strongarm->value = array(
    'status' => 0,
    'view modes' => array(
      'page_manager' => array(
        'status' => 1,
        'default' => 1,
        'choice' => 0,
      ),
      'default' => array(
        'status' => 0,
        'default' => 0,
        'choice' => 0,
      ),
    ),
  );
  $export['panelizer_defaults_user_user'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'pm_existing_pages_disabled_login_pages';
  $strongarm->value = FALSE;
  $export['pm_existing_pages_disabled_login_pages'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'realname_pattern';
  $strongarm->value = '[user:profile-main:field_name_first] [user:profile-main:field_name_last] [user:profile-main:field_name_suffix]';
  $export['realname_pattern'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'user_pictures';
  $strongarm->value = 0;
  $export['user_pictures'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'user_picture_default';
  $strongarm->value = 'sites/all/modules/features/uwc_users/images/default_user.png';
  $export['user_picture_default'] = $strongarm;

  return $export;
}
