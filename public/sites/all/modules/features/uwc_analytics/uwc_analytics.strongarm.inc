<?php
/**
 * @file
 * uwc_analytics.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function uwc_analytics_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'uwc_woopra_analytics_domain';
  $strongarm->value = 'online.unitedway.org';
  $export['uwc_woopra_analytics_domain'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'uwc_woopra_analytics_switch';
  $strongarm->value = 1;
  $export['uwc_woopra_analytics_switch'] = $strongarm;

  return $export;
}
