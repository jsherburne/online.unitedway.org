<?php
/**
 * @file
 * commons_follow_user.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function commons_follow_user_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function commons_follow_user_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_flag_default_flags().
 */
function commons_follow_user_flag_default_flags() {
  $flags = array();
  // Exported flag: "People".
  $flags['commons_follow_user'] = array(
    'entity_type' => 'user',
    'title' => 'People',
    'global' => 0,
    'types' => array(),
    'flag_short' => 'Follow',
    'flag_long' => '',
    'flag_message' => '',
    'unflag_short' => 'Following',
    'unflag_long' => '',
    'unflag_message' => 'Not following',
    'unflag_denied_text' => '',
    'link_type' => 'toggle',
    'weight' => -13,
    'show_in_links' => array(),
    'show_as_field' => FALSE,
    'show_on_form' => 0,
    'access_author' => '',
    'show_contextual_link' => FALSE,
    'show_on_profile' => 1,
    'access_uid' => 'others',
    'module' => 'commons_follow_user',
    'locked' => array(
      0 => 'name',
    ),
    'api_version' => 3,
  );
  return $flags;

}

/**
 * Implements hook_image_default_styles().
 */
function commons_follow_user_image_default_styles() {
  $styles = array();

  // Exported image style: 100x100_avatar.
  $styles['100x100_avatar'] = array(
    'name' => '100x100_avatar',
    'label' => '100x100_avatar',
    'effects' => array(
      3 => array(
        'label' => 'Scale and crop',
        'help' => 'Scale and crop will maintain the aspect-ratio of the original image, then crop the larger dimension. This is most useful for creating perfectly square thumbnails without stretching the image.',
        'effect callback' => 'image_scale_and_crop_effect',
        'dimensions callback' => 'image_resize_dimensions',
        'form callback' => 'image_resize_form',
        'summary theme' => 'image_resize_summary',
        'module' => 'image',
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 100,
          'height' => 100,
        ),
        'weight' => 1,
      ),
    ),
  );

  return $styles;
}

/**
 * Implements hook_default_message_type().
 */
function commons_follow_user_default_message_type() {
  $items = array();
  $items['commons_follow_user_user_followed'] = entity_import('message_type', '{
    "name" : "commons_follow_user_user_followed",
    "description" : "commons_follow_user_user_followed",
    "argument_keys" : [],
    "argument" : [],
    "category" : "message_type",
    "data" : {
      "token options" : { "clear" : 0 },
      "purge" : { "override" : 0, "enabled" : 0, "quota" : "", "days" : "" }
    },
    "language" : "",
    "arguments" : null,
    "message_text" : { "und" : [
        {
          "value" : "[message:user:picture:35x35]",
          "format" : "filtered_html",
          "safe_value" : "[message:user:picture:35x35]"
        },
        {
          "value" : "\\u003Ca href=[message:user:url:absolute]\\u003E[message:user:name]\\u003C\\/a\\u003E is now following \\u003Ca href=[message:field-target-users:0:url]\\u003E[message:field-target-users:0:name]\\u003C\\/a\\u003E",
          "format" : "full_html",
          "safe_value" : "\\u003Ca href=[message:user:url:absolute]\\u003E[message:user:name]\\u003C\\/a\\u003E is now following \\u003Ca href=[message:field-target-users:0:url]\\u003E[message:field-target-users:0:name]\\u003C\\/a\\u003E"
        }
      ]
    },
    "rdf_mapping" : []
  }');
  return $items;
}
