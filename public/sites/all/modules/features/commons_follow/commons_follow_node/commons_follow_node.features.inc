<?php
/**
 * @file
 * commons_follow_node.features.inc
 */

/**
 * Implements hook_views_api().
 */
function commons_follow_node_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_flag_default_flags().
 */
function commons_follow_node_flag_default_flags() {
  $flags = array();
  // Exported flag: "Other content".
  $flags['commons_follow_node'] = array(
    'entity_type' => 'node',
    'title' => 'Other content',
    'global' => 0,
    'types' => array(),
    'flag_short' => 'Follow',
    'flag_long' => '',
    'flag_message' => '',
    'unflag_short' => 'Following',
    'unflag_long' => '',
    'unflag_message' => 'Not following',
    'unflag_denied_text' => '',
    'link_type' => 'toggle',
    'weight' => -10,
    'show_in_links' => array(
      'full' => 'full',
      'teaser' => 'teaser',
      'rss' => 0,
      'diff_standard' => 0,
      'token' => 0,
      'search_result' => 0,
      'compact_teaser' => 0,
      'related_content' => 0,
      'revision' => 0,
    ),
    'show_as_field' => 0,
    'show_on_form' => 0,
    'access_author' => '',
    'show_contextual_link' => 0,
    'i18n' => 0,
    'module' => 'commons_follow_node',
    'locked' => array(
      0 => 'name',
    ),
    'api_version' => 3,
  );
  // Exported flag: "Email content".
  $flags['email_node'] = array(
    'entity_type' => 'node',
    'title' => 'Email content',
    'global' => 0,
    'types' => array(),
    'flag_short' => 'Send Email',
    'flag_long' => '',
    'flag_message' => '',
    'unflag_short' => 'Don\'t send Email',
    'unflag_long' => '',
    'unflag_message' => '',
    'unflag_denied_text' => '',
    'link_type' => 'toggle',
    'weight' => 0,
    'show_in_links' => array(),
    'show_as_field' => FALSE,
    'show_on_form' => 0,
    'access_author' => '',
    'show_contextual_link' => 0,
    'i18n' => 0,
    'api_version' => 3,
    'module' => 'commons_follow_node',
    'locked' => array(
      0 => 'name',
    ),
  );
  return $flags;

}
