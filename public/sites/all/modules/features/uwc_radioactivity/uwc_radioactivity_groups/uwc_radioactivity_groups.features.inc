<?php
/**
 * @file
 * uwc_radioactivity_groups.features.inc
 */

/**
 * Implements hook_views_api().
 */
function uwc_radioactivity_groups_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}
