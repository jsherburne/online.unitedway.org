<?php
/**
 * @file
 * uw_corporate_profile.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function uw_corporate_profile_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function uw_corporate_profile_node_info() {
  $items = array(
    'corporate_profile' => array(
      'name' => t('Corporate Profile'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
