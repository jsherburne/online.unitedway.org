<?php
/**
 * @file
 * uw_corporate_profile.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function uw_corporate_profile_field_group_info() {
  $export = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_address|node|corporate_profile|form';
  $field_group->group_name = 'group_address';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'corporate_profile';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Address',
    'weight' => '4',
    'children' => array(
      0 => 'title_field',
      1 => 'field_city',
      2 => 'field_state',
      3 => 'field_last_updated',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-address field-group-fieldset',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_address|node|corporate_profile|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_camp_dates|node|corporate_profile|form';
  $field_group->group_name = 'group_camp_dates';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'corporate_profile';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'campaign Dates',
    'weight' => '5',
    'children' => array(
      0 => 'field_campaign_start_date',
      1 => 'field_campaign_end_date',
      2 => 'field_company_flexibility_within',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-camp-dates field-group-fieldset',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_camp_dates|node|corporate_profile|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_camp_results|node|corporate_profile|form';
  $field_group->group_name = 'group_camp_results';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'corporate_profile';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Campaign Results',
    'weight' => '7',
    'children' => array(
      0 => 'field_employee_results_available',
      1 => 'field_corporate_results_availabl',
      2 => 'field_reports_available',
      3 => 'field_results_provided_by',
      4 => 'field_how_and_from_where_will_yo',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-camp-results field-group-fieldset',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_camp_results|node|corporate_profile|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_campaign_details|node|corporate_profile|form';
  $field_group->group_name = 'group_campaign_details';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'corporate_profile';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Campaign Details',
    'weight' => '8',
    'children' => array(
      0 => 'field_campaign_theme',
      1 => 'field_campaign_structure',
      2 => 'field_campaign_materials_provide',
      3 => 'field_provide_local_materials_',
      4 => 'field_use_local_pledge_card_',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-campaign-details field-group-fieldset',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_campaign_details|node|corporate_profile|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_corp_info|node|corporate_profile|form';
  $field_group->group_name = 'group_corp_info';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'corporate_profile';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Corporate Information',
    'weight' => '13',
    'children' => array(
      0 => 'field_company_overview',
      1 => 'field_employee_giving',
      2 => 'field_leadership_giving',
      3 => 'field_company_giving',
      4 => 'field_retiree_program',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-corp-info field-group-fieldset',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_corp_info|node|corporate_profile|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_finance_info|node|corporate_profile|form';
  $field_group->group_name = 'group_finance_info';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'corporate_profile';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Finance Information',
    'weight' => '10',
    'children' => array(
      0 => 'field_united_way_responsible_for',
      1 => 'field_employee_payment_paid_by',
      2 => 'field_corporate_payment_paid_by',
      3 => 'field_corporate_gift_reduced_for',
      4 => 'field_corporate_gift_determined_',
      5 => 'field_does_match_gift_follow_don',
      6 => 'field_corporate_gift_match_cap_p',
      7 => 'field_corporate_gift_comments',
      8 => 'field_when_to_expect_first_emplo',
      9 => 'field_payment_frequency',
      10 => 'field_payment_processing_details',
      11 => 'field_corporate_gift_match_rate_',
      12 => 'field_corporate_gift_',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-finance-info field-group-fieldset',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_finance_info|node|corporate_profile|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_giving_history|node|corporate_profile|form';
  $field_group->group_name = 'group_giving_history';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'corporate_profile';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Giving History',
    'weight' => '11',
    'children' => array(
      0 => 'field_campaign_year',
      1 => 'field_num_of_emp_',
      2 => 'field_corporate_gifts',
      3 => 'field_employee_gifts',
      4 => 'field_corp_per_capita',
      5 => 'field_emp_per_capita',
      6 => 'field_retiree_gifts',
      7 => 'field_special_event',
      8 => 'field_percent_participation',
      9 => 'field_campaign_total',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-giving-history field-group-fieldset',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_giving_history|node|corporate_profile|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_pledges|node|corporate_profile|form';
  $field_group->group_name = 'group_pledges';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'corporate_profile';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Pledges',
    'weight' => '9',
    'children' => array(
      0 => 'field_employee_pledges_tied_to',
      1 => 'field_employee_gift_reduced_for_',
      2 => 'field_pledge_method',
      3 => 'field_pledge_capture_by',
      4 => 'field_united_way_impact_areas_on',
      5 => 'field_giving_methods',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-pledges field-group-fieldset',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_pledges|node|corporate_profile|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_rel_building|node|corporate_profile|form';
  $field_group->group_name = 'group_rel_building';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'corporate_profile';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => '  Relationship Building',
    'weight' => '6',
    'children' => array(
      0 => 'field_provide_list_of_local_comp',
      1 => 'field_provide_donor_contact_info',
      2 => 'field_donor_e_mail_opt_in',
      3 => 'field_loyal_contributors_tracked',
      4 => 'field_account_staff',
      5 => 'field_uwo_taxonomy',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-rel-building field-group-fieldset',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_rel_building|node|corporate_profile|form'] = $field_group;

  return $export;
}
