<?php
/**
 * @file
 * uwc_child_events.ds.inc
 */

/**
 * Implements hook_ds_field_settings_info().
 */
function uwc_child_events_ds_field_settings_info() {
  $export = array();

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'node|child_event|compact_teaser';
  $ds_fieldsetting->entity_type = 'node';
  $ds_fieldsetting->bundle = 'child_event';
  $ds_fieldsetting->view_mode = 'compact_teaser';
  $ds_fieldsetting->settings = array(
    'field_date' => array(
      'formatter_settings' => array(
        'ft' => array(),
      ),
    ),
  );
  $export['node|child_event|compact_teaser'] = $ds_fieldsetting;

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'node|child_event|default';
  $ds_fieldsetting->entity_type = 'node';
  $ds_fieldsetting->bundle = 'child_event';
  $ds_fieldsetting->view_mode = 'default';
  $ds_fieldsetting->settings = array(
    'links' => array(
      'weight' => '12',
      'label' => 'hidden',
      'format' => 'default',
    ),
    'comments' => array(
      'weight' => '13',
      'label' => 'hidden',
      'format' => 'default',
    ),
    'author' => array(
      'weight' => '9',
      'label' => 'hidden',
      'format' => 'author_linked',
    ),
    'changed_date' => array(
      'weight' => '8',
      'label' => 'inline',
      'format' => 'ds_post_date_rich_snippets_published_date',
    ),
    'rate_integration_1' => array(
      'weight' => '10',
      'label' => 'hidden',
      'format' => 'default',
    ),
  );
  $export['node|child_event|default'] = $ds_fieldsetting;

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'node|child_event|full';
  $ds_fieldsetting->entity_type = 'node';
  $ds_fieldsetting->bundle = 'child_event';
  $ds_fieldsetting->view_mode = 'full';
  $ds_fieldsetting->settings = array(
    'links' => array(
      'weight' => '10',
      'label' => 'hidden',
      'format' => 'default',
    ),
    'comments' => array(
      'weight' => '11',
      'label' => 'hidden',
      'format' => 'default',
    ),
    'rate_integration_1' => array(
      'weight' => '9',
      'label' => 'hidden',
      'format' => 'default',
    ),
  );
  $export['node|child_event|full'] = $ds_fieldsetting;

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'node|child_event|search_result';
  $ds_fieldsetting->entity_type = 'node';
  $ds_fieldsetting->bundle = 'child_event';
  $ds_fieldsetting->view_mode = 'search_result';
  $ds_fieldsetting->settings = array(
    'links' => array(
      'weight' => '7',
      'label' => 'hidden',
      'format' => 'default',
    ),
    'rate_integration_1' => array(
      'weight' => '6',
      'label' => 'hidden',
      'format' => 'default',
    ),
  );
  $export['node|child_event|search_result'] = $ds_fieldsetting;

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'node|child_event|teaser';
  $ds_fieldsetting->entity_type = 'node';
  $ds_fieldsetting->bundle = 'child_event';
  $ds_fieldsetting->view_mode = 'teaser';
  $ds_fieldsetting->settings = array(
    'comment_count' => array(
      'weight' => '7',
      'label' => 'hidden',
      'format' => 'default',
    ),
    'profile2' => array(
      'weight' => '0',
      'label' => 'hidden',
      'format' => 'compact_teaser',
    ),
    'rate_integration_1' => array(
      'weight' => '6',
      'label' => 'hidden',
      'format' => 'default',
    ),
  );
  $export['node|child_event|teaser'] = $ds_fieldsetting;

  return $export;
}

/**
 * Implements hook_ds_layout_settings_info().
 */
function uwc_child_events_ds_layout_settings_info() {
  $export = array();

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|child_event|compact_teaser';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'child_event';
  $ds_layout->view_mode = 'compact_teaser';
  $ds_layout->layout = 'ds_1col';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'field_date',
        1 => 'title_field',
      ),
    ),
    'fields' => array(
      'field_date' => 'ds_content',
      'title_field' => 'ds_content',
    ),
    'classes' => array(
      'layout_class' => array(
        '' => '',
      ),
    ),
    'wrappers' => array(
      'ds_content' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
  );
  $export['node|child_event|compact_teaser'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|child_event|default';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'child_event';
  $ds_layout->view_mode = 'default';
  $ds_layout->layout = 'ds_1col';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'title_field',
        1 => 'field_event_parent',
        2 => 'field_logo',
        3 => 'field_event_contact',
        4 => 'field_date',
        5 => 'body',
        6 => 'field_related_content',
        7 => 'field_related_organizations',
        8 => 'changed_date',
        9 => 'author',
        10 => 'rate_integration_1',
        11 => 'links',
        12 => 'comments',
      ),
    ),
    'fields' => array(
      'title_field' => 'ds_content',
      'field_event_parent' => 'ds_content',
      'field_logo' => 'ds_content',
      'field_event_contact' => 'ds_content',
      'field_date' => 'ds_content',
      'body' => 'ds_content',
      'field_related_content' => 'ds_content',
      'field_related_organizations' => 'ds_content',
      'changed_date' => 'ds_content',
      'author' => 'ds_content',
      'rate_integration_1' => 'ds_content',
      'links' => 'ds_content',
      'comments' => 'ds_content',
    ),
    'classes' => array(
      'layout_class' => array(
        '' => '',
      ),
    ),
    'wrappers' => array(
      'ds_content' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
  );
  $export['node|child_event|default'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|child_event|full';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'child_event';
  $ds_layout->view_mode = 'full';
  $ds_layout->layout = 'ds_1col';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'field_date',
        1 => 'field_event_parent',
        2 => 'title_field',
        3 => 'field_logo',
        4 => 'field_event_contact',
        5 => 'body',
        6 => 'field_related_content',
        7 => 'field_related_organizations',
        8 => 'field_document_file',
        9 => 'rate_integration_1',
        10 => 'links',
        11 => 'comments',
      ),
    ),
    'fields' => array(
      'field_date' => 'ds_content',
      'field_event_parent' => 'ds_content',
      'title_field' => 'ds_content',
      'field_logo' => 'ds_content',
      'field_event_contact' => 'ds_content',
      'body' => 'ds_content',
      'field_related_content' => 'ds_content',
      'field_related_organizations' => 'ds_content',
      'field_document_file' => 'ds_content',
      'rate_integration_1' => 'ds_content',
      'links' => 'ds_content',
      'comments' => 'ds_content',
    ),
    'classes' => array(
      'layout_class' => array(
        '' => '',
      ),
    ),
    'wrappers' => array(
      'ds_content' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
  );
  $export['node|child_event|full'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|child_event|search_result';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'child_event';
  $ds_layout->view_mode = 'search_result';
  $ds_layout->layout = 'ds_1col';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'field_logo',
        1 => 'field_date',
        2 => 'title_field',
        3 => 'body',
        4 => 'rate_integration_1',
        5 => 'links',
      ),
    ),
    'fields' => array(
      'field_logo' => 'ds_content',
      'field_date' => 'ds_content',
      'title_field' => 'ds_content',
      'body' => 'ds_content',
      'rate_integration_1' => 'ds_content',
      'links' => 'ds_content',
    ),
    'classes' => array(
      'layout_class' => array(
        '' => '',
      ),
    ),
    'wrappers' => array(
      'ds_content' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
  );
  $export['node|child_event|search_result'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|child_event|teaser';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'child_event';
  $ds_layout->view_mode = 'teaser';
  $ds_layout->layout = 'ds_2col';
  $ds_layout->settings = array(
    'regions' => array(
      'left' => array(
        0 => 'profile2',
      ),
      'right' => array(
        1 => 'field_date',
        2 => 'field_event_parent',
        3 => 'title_field',
        4 => 'body',
        5 => 'group_node_actions',
        6 => 'rate_integration_1',
        7 => 'comment_count',
      ),
    ),
    'fields' => array(
      'profile2' => 'left',
      'field_date' => 'right',
      'field_event_parent' => 'right',
      'title_field' => 'right',
      'body' => 'right',
      'group_node_actions' => 'right',
      'rate_integration_1' => 'right',
      'comment_count' => 'right',
    ),
    'classes' => array(
      'layout_class' => array(
        '' => '',
      ),
    ),
    'wrappers' => array(
      'left' => 'div',
      'right' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
  );
  $export['node|child_event|teaser'] = $ds_layout;

  return $export;
}
