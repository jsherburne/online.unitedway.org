<?php
/**
 * @file
 * uwc_polls.ds.inc
 */

/**
 * Implements hook_ds_field_settings_info().
 */
function uwc_polls_ds_field_settings_info() {
  $export = array();

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'node|poll|compact_teaser';
  $ds_fieldsetting->entity_type = 'node';
  $ds_fieldsetting->bundle = 'poll';
  $ds_fieldsetting->view_mode = 'compact_teaser';
  $ds_fieldsetting->settings = array(
    'private_group_flag' => array(
      'weight' => '2',
      'label' => 'hidden',
      'format' => 'default',
    ),
    'links' => array(
      'weight' => '4',
      'label' => 'hidden',
      'format' => 'default',
    ),
    'post_date' => array(
      'weight' => '3',
      'label' => 'inline',
      'format' => 'ds_post_date_rich_snippets_published_date',
      'formatter_settings' => array(
        'ft' => array(),
        'field_delimiter' => '',
      ),
    ),
    'node_type' => array(
      'weight' => '1',
      'label' => 'hidden',
      'format' => 'default',
      'formatter_settings' => array(
        'ft' => array(
          'func' => 'theme_field',
        ),
        'field_delimiter' => '',
      ),
    ),
  );
  $export['node|poll|compact_teaser'] = $ds_fieldsetting;

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'node|poll|default';
  $ds_fieldsetting->entity_type = 'node';
  $ds_fieldsetting->bundle = 'poll';
  $ds_fieldsetting->view_mode = 'default';
  $ds_fieldsetting->settings = array(
    'links' => array(
      'weight' => '12',
      'label' => 'hidden',
      'format' => 'default',
    ),
    'comments' => array(
      'weight' => '11',
      'label' => 'hidden',
      'format' => 'default',
    ),
    'author' => array(
      'weight' => '3',
      'label' => 'hidden',
      'format' => 'author_linked',
    ),
    'post_date' => array(
      'weight' => '2',
      'label' => 'inline',
      'format' => 'ds_post_date_rich_snippets_published_date',
      'formatter_settings' => array(
        'ft' => array(
          'lb' => 'Last updated',
        ),
      ),
    ),
    'rate_integration_1' => array(
      'weight' => '9',
      'label' => 'hidden',
      'format' => 'default',
    ),
    'og_group_ref' => array(
      'formatter_settings' => array(
        'ft' => array(
          'lb' => 'Communities of Practice',
        ),
      ),
    ),
  );
  $export['node|poll|default'] = $ds_fieldsetting;

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'node|poll|full';
  $ds_fieldsetting->entity_type = 'node';
  $ds_fieldsetting->bundle = 'poll';
  $ds_fieldsetting->view_mode = 'full';
  $ds_fieldsetting->settings = array(
    'private_group_flag' => array(
      'weight' => '1',
      'label' => 'hidden',
      'format' => 'default',
    ),
    'comments' => array(
      'weight' => '12',
      'label' => 'hidden',
      'format' => 'default',
    ),
    'author' => array(
      'weight' => '3',
      'label' => 'hidden',
      'format' => 'author_linked',
    ),
    'post_date' => array(
      'weight' => '2',
      'label' => 'hidden',
      'format' => 'ds_post_date_rich_snippets_published_date',
      'formatter_settings' => array(
        'ft' => array(),
      ),
    ),
    'flag_commons_follow_node' => array(
      'weight' => '9',
      'label' => 'hidden',
      'format' => 'default',
    ),
    'flag_breakfast_recommendation' => array(
      'weight' => '10',
      'label' => 'hidden',
      'format' => 'default',
    ),
    'node_type' => array(
      'weight' => '0',
      'label' => 'hidden',
      'format' => 'default',
      'formatter_settings' => array(
        'ft' => array(
          'classes' => 'type',
        ),
        'field_delimiter' => '',
      ),
    ),
    'rate_integration_1' => array(
      'weight' => '8',
      'label' => 'hidden',
      'format' => 'default',
    ),
    'og_group_ref' => array(
      'formatter_settings' => array(
        'ft' => array(
          'lb' => 'Communities of Practice',
        ),
      ),
    ),
  );
  $export['node|poll|full'] = $ds_fieldsetting;

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'node|poll|related_content';
  $ds_fieldsetting->entity_type = 'node';
  $ds_fieldsetting->bundle = 'poll';
  $ds_fieldsetting->view_mode = 'related_content';
  $ds_fieldsetting->settings = array(
    'author' => array(
      'weight' => '3',
      'label' => 'hidden',
      'format' => 'author_linked',
    ),
    'post_date' => array(
      'weight' => '2',
      'label' => 'hidden',
      'format' => 'ds_post_date_rich_snippets_published_date',
    ),
    'profile2' => array(
      'weight' => '0',
      'label' => 'hidden',
      'format' => 'related_content',
    ),
  );
  $export['node|poll|related_content'] = $ds_fieldsetting;

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'node|poll|search_result';
  $ds_fieldsetting->entity_type = 'node';
  $ds_fieldsetting->bundle = 'poll';
  $ds_fieldsetting->view_mode = 'search_result';
  $ds_fieldsetting->settings = array(
    'private_group_flag' => array(
      'weight' => '0',
      'label' => 'hidden',
      'format' => 'default',
    ),
    'links' => array(
      'weight' => '8',
      'label' => 'hidden',
      'format' => 'default',
    ),
    'author' => array(
      'weight' => '2',
      'label' => 'hidden',
      'format' => 'author_linked',
    ),
    'post_date' => array(
      'weight' => '1',
      'label' => 'inline',
      'format' => 'ds_post_date_rich_snippets_published_date',
      'formatter_settings' => array(
        'ft' => array(),
        'field_delimiter' => '',
      ),
    ),
    'node_type' => array(
      'weight' => '0',
      'label' => 'hidden',
      'format' => 'default',
    ),
    'rate_integration_1' => array(
      'weight' => '7',
      'label' => 'hidden',
      'format' => 'default',
    ),
    'og_group_ref' => array(
      'formatter_settings' => array(
        'ft' => array(
          'lb' => 'Communities of Practice',
        ),
      ),
    ),
  );
  $export['node|poll|search_result'] = $ds_fieldsetting;

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'node|poll|teaser';
  $ds_fieldsetting->entity_type = 'node';
  $ds_fieldsetting->bundle = 'poll';
  $ds_fieldsetting->view_mode = 'teaser';
  $ds_fieldsetting->settings = array(
    'private_group_flag' => array(
      'weight' => '3',
      'label' => 'hidden',
      'format' => 'default',
    ),
    'author' => array(
      'weight' => '5',
      'label' => 'hidden',
      'format' => 'author_linked',
    ),
    'post_date' => array(
      'weight' => '1',
      'label' => 'hidden',
      'format' => 'ds_post_date_rich_snippets_published_date',
      'formatter_settings' => array(
        'ft' => array(),
      ),
    ),
    'comment_count' => array(
      'weight' => '13',
      'label' => 'hidden',
      'format' => 'default',
    ),
    'profile2' => array(
      'weight' => '0',
      'label' => 'hidden',
      'format' => 'compact_teaser',
    ),
    'node_type' => array(
      'weight' => '2',
      'label' => 'hidden',
      'format' => 'default',
      'formatter_settings' => array(
        'ft' => array(
          'func' => 'theme_field',
          'classes' => 'type',
        ),
        'field_delimiter' => '',
      ),
    ),
    'rate_integration_1' => array(
      'weight' => '12',
      'label' => 'hidden',
      'format' => 'default',
    ),
    'og_group_ref' => array(
      'formatter_settings' => array(
        'ft' => array(
          'lb' => 'Communities of Practice',
        ),
      ),
    ),
  );
  $export['node|poll|teaser'] = $ds_fieldsetting;

  return $export;
}

/**
 * Implements hook_ds_layout_settings_info().
 */
function uwc_polls_ds_layout_settings_info() {
  $export = array();

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|poll|compact_teaser';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'poll';
  $ds_layout->view_mode = 'compact_teaser';
  $ds_layout->layout = 'ds_1col';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'title_field',
        1 => 'node_type',
        2 => 'private_group_flag',
        3 => 'post_date',
        4 => 'links',
      ),
    ),
    'fields' => array(
      'title_field' => 'ds_content',
      'node_type' => 'ds_content',
      'private_group_flag' => 'ds_content',
      'post_date' => 'ds_content',
      'links' => 'ds_content',
    ),
    'classes' => array(
      'layout_class' => array(
        '' => '',
      ),
    ),
    'wrappers' => array(
      'ds_content' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
  );
  $export['node|poll|compact_teaser'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|poll|default';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'poll';
  $ds_layout->view_mode = 'default';
  $ds_layout->layout = 'ds_1col';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'title_field',
        1 => 'post_date',
        2 => 'author',
        3 => 'og_group_ref',
        4 => 'field_topics',
        5 => 'poll_view_voting',
        6 => 'poll_view_results',
        7 => 'body',
        8 => 'rate_integration_1',
        9 => 'comments',
        10 => 'links',
      ),
    ),
    'fields' => array(
      'title_field' => 'ds_content',
      'post_date' => 'ds_content',
      'author' => 'ds_content',
      'og_group_ref' => 'ds_content',
      'field_topics' => 'ds_content',
      'poll_view_voting' => 'ds_content',
      'poll_view_results' => 'ds_content',
      'body' => 'ds_content',
      'rate_integration_1' => 'ds_content',
      'comments' => 'ds_content',
      'links' => 'ds_content',
    ),
    'classes' => array(
      'layout_class' => array(
        '' => '',
      ),
    ),
    'wrappers' => array(
      'ds_content' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
  );
  $export['node|poll|default'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|poll|full';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'poll';
  $ds_layout->view_mode = 'full';
  $ds_layout->layout = 'ds_1col';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'node_type',
        1 => 'private_group_flag',
        2 => 'post_date',
        3 => 'author',
        4 => 'title_field',
        5 => 'body',
        6 => 'poll_view_voting',
        7 => 'poll_view_results',
        8 => 'rate_integration_1',
        9 => 'flag_commons_follow_node',
        10 => 'flag_breakfast_recommendation',
        11 => 'group_node_tax',
        12 => 'comments',
        13 => 'og_group_ref',
        14 => 'field_topics',
      ),
    ),
    'fields' => array(
      'node_type' => 'ds_content',
      'private_group_flag' => 'ds_content',
      'post_date' => 'ds_content',
      'author' => 'ds_content',
      'title_field' => 'ds_content',
      'body' => 'ds_content',
      'poll_view_voting' => 'ds_content',
      'poll_view_results' => 'ds_content',
      'rate_integration_1' => 'ds_content',
      'flag_commons_follow_node' => 'ds_content',
      'flag_breakfast_recommendation' => 'ds_content',
      'group_node_tax' => 'ds_content',
      'comments' => 'ds_content',
      'og_group_ref' => 'ds_content',
      'field_topics' => 'ds_content',
    ),
    'classes' => array(
      'layout_class' => array(
        '' => '',
      ),
    ),
    'wrappers' => array(
      'ds_content' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
  );
  $export['node|poll|full'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|poll|related_content';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'poll';
  $ds_layout->view_mode = 'related_content';
  $ds_layout->layout = 'ds_3col';
  $ds_layout->settings = array(
    'regions' => array(
      'left' => array(
        0 => 'profile2',
      ),
      'middle' => array(
        1 => 'title_field',
        2 => 'post_date',
        3 => 'author',
      ),
    ),
    'fields' => array(
      'profile2' => 'left',
      'title_field' => 'middle',
      'post_date' => 'middle',
      'author' => 'middle',
    ),
    'classes' => array(
      'layout_class' => array(
        '' => '',
      ),
    ),
    'wrappers' => array(
      'left' => 'div',
      'middle' => 'div',
      'right' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
  );
  $export['node|poll|related_content'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|poll|search_result';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'poll';
  $ds_layout->view_mode = 'search_result';
  $ds_layout->layout = 'ds_1col';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'private_group_flag',
        1 => 'node_type',
        2 => 'post_date',
        3 => 'author',
        4 => 'title_field',
        5 => 'body',
        6 => 'poll_view_voting',
        7 => 'poll_view_results',
        8 => 'og_group_ref',
        9 => 'rate_integration_1',
        10 => 'links',
        11 => 'field_topics',
      ),
    ),
    'fields' => array(
      'private_group_flag' => 'ds_content',
      'node_type' => 'ds_content',
      'post_date' => 'ds_content',
      'author' => 'ds_content',
      'title_field' => 'ds_content',
      'body' => 'ds_content',
      'poll_view_voting' => 'ds_content',
      'poll_view_results' => 'ds_content',
      'og_group_ref' => 'ds_content',
      'rate_integration_1' => 'ds_content',
      'links' => 'ds_content',
      'field_topics' => 'ds_content',
    ),
    'classes' => array(
      'layout_class' => array(
        '' => '',
      ),
    ),
    'wrappers' => array(
      'ds_content' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
  );
  $export['node|poll|search_result'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|poll|teaser';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'poll';
  $ds_layout->view_mode = 'teaser';
  $ds_layout->layout = 'ds_2col';
  $ds_layout->settings = array(
    'regions' => array(
      'left' => array(
        0 => 'profile2',
      ),
      'right' => array(
        1 => 'post_date',
        2 => 'node_type',
        3 => 'private_group_flag',
        4 => 'title_field',
        5 => 'author',
        6 => 'body',
        7 => 'poll_view_voting',
        8 => 'poll_view_results',
        9 => 'og_group_ref',
        10 => 'field_topics',
        11 => 'group_node_actions',
        12 => 'rate_integration_1',
        13 => 'comment_count',
      ),
    ),
    'fields' => array(
      'profile2' => 'left',
      'post_date' => 'right',
      'node_type' => 'right',
      'private_group_flag' => 'right',
      'title_field' => 'right',
      'author' => 'right',
      'body' => 'right',
      'poll_view_voting' => 'right',
      'poll_view_results' => 'right',
      'og_group_ref' => 'right',
      'field_topics' => 'right',
      'group_node_actions' => 'right',
      'rate_integration_1' => 'right',
      'comment_count' => 'right',
    ),
    'classes' => array(
      'layout_class' => array(
        '' => '',
      ),
      'left' => array(
        'teaser-left' => 'teaser-left',
      ),
      'right' => array(
        'teaser-right' => 'teaser-right',
      ),
    ),
    'wrappers' => array(
      'left' => 'div',
      'right' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
  );
  $export['node|poll|teaser'] = $ds_layout;

  return $export;
}
