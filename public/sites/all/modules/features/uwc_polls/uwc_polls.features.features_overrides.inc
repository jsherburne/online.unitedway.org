<?php
/**
 * @file
 * uwc_polls.features.features_overrides.inc
 */

/**
 * Implements hook_features_override_default_overrides().
 */
function uwc_polls_features_override_default_overrides() {
  // This code is only used for UI in features. Exported alters hooks do the magic.
  $overrides = array();

  // Exported overrides for: field_instance

  // Exported overrides for: node
  $overrides["node.poll.has_body"]["DELETED"] = TRUE;
  $overrides["node.poll.has_title"] = 1;
  $overrides["node.poll.help"] = '';

  // Exported overrides for: variable

  // Exported overrides for: views_view

 return $overrides;
}
