Note: This feature has many forcefully excluded dependencies. This is necessary in order to allow modules to be turned
off without disabling this module.