<?php
/**
 * @file
 * uwc_content_access_settings.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function uwc_content_access_settings_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'content_access_child_event';
  $strongarm->value = array(
    'view_own' => array(
      0 => 1,
      1 => 2,
      2 => 3,
      3 => 5,
    ),
    'view' => array(
      0 => 1,
      1 => 2,
      2 => 3,
      3 => 5,
    ),
    'priority' => '-1',
  );
  $export['content_access_child_event'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'content_access_document';
  $strongarm->value = array(
    'view_own' => array(
      0 => 1,
      1 => 2,
      2 => 3,
      3 => 4,
      4 => 5,
    ),
    'view' => array(
      0 => 1,
      1 => 2,
      2 => 3,
      3 => 4,
      4 => 5,
    ),
    'per_node' => 1,
    'priority' => '-1',
  );
  $export['content_access_document'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'content_access_event';
  $strongarm->value = array(
    'view_own' => array(
      0 => 1,
      1 => 2,
      2 => 3,
      3 => 5,
    ),
    'view' => array(
      0 => 1,
      1 => 2,
      2 => 3,
      3 => 5,
    ),
    'priority' => '-1',
  );
  $export['content_access_event'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'content_access_group';
  $strongarm->value = array(
    'view_own' => array(
      0 => 1,
      1 => 2,
      2 => 3,
      3 => 5,
    ),
    'view' => array(
      0 => 1,
      1 => 2,
      2 => 3,
      3 => 5,
    ),
    'priority' => '-1',
  );
  $export['content_access_group'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'content_access_guide';
  $strongarm->value = array(
    'view_own' => array(
      0 => 1,
      1 => 2,
      2 => 3,
      3 => 5,
    ),
    'view' => array(
      0 => 1,
      1 => 2,
      2 => 3,
      3 => 5,
    ),
    'priority' => '-1',
  );
  $export['content_access_guide'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'content_access_landing_page';
  $strongarm->value = array(
    'view_own' => array(
      0 => 1,
      1 => 2,
      2 => 3,
      3 => 5,
    ),
    'view' => array(
      0 => 1,
      1 => 2,
      2 => 3,
      3 => 5,
    ),
    'priority' => '-1',
  );
  $export['content_access_landing_page'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'content_access_news';
  $strongarm->value = array(
    'view_own' => array(
      0 => 1,
      1 => 2,
      2 => 3,
      3 => 5,
    ),
    'view' => array(
      0 => 1,
      1 => 2,
      2 => 3,
      3 => 5,
    ),
    'priority' => '-1',
  );
  $export['content_access_news'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'content_access_organization';
  $strongarm->value = array(
    'view_own' => array(
      0 => 1,
      1 => 2,
      2 => 3,
      3 => 5,
    ),
    'view' => array(
      0 => 1,
      1 => 2,
      2 => 3,
      3 => 5,
    ),
    'priority' => '-1',
  );
  $export['content_access_organization'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'content_access_page';
  $strongarm->value = array(
    'view_own' => array(
      0 => 1,
      1 => 2,
      2 => 3,
      3 => 5,
    ),
    'view' => array(
      0 => 1,
      1 => 2,
      2 => 3,
      3 => 5,
    ),
    'priority' => '-1',
  );
  $export['content_access_page'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'content_access_poll';
  $strongarm->value = array(
    'view_own' => array(
      0 => 1,
      1 => 2,
      2 => 3,
      3 => 5,
    ),
    'view' => array(
      0 => 1,
      1 => 2,
      2 => 3,
      3 => 5,
    ),
    'priority' => '-1',
  );
  $export['content_access_poll'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'content_access_post';
  $strongarm->value = array(
    'view_own' => array(
      0 => 1,
      1 => 2,
      2 => 3,
      3 => 5,
    ),
    'view' => array(
      0 => 1,
      1 => 2,
      2 => 3,
      3 => 5,
    ),
    'priority' => '-1',
  );
  $export['content_access_post'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'content_access_wrapped_system';
  $strongarm->value = array(
    'view_own' => array(
      0 => 1,
      1 => 2,
      2 => 3,
      3 => 4,
      4 => 5,
    ),
    'view' => array(
      0 => 1,
      1 => 2,
      2 => 3,
      3 => 4,
      4 => 5,
    ),
    'priority' => '-1',
    'per_node' => 1,
  );
  $export['content_access_wrapped_system'] = $strongarm;

  return $export;
}
