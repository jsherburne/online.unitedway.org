<?php
/**
 * @file
 * uwc_taxonomy.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function uwc_taxonomy_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "facetapi" && $api == "facetapi_defaults") {
    return array("version" => "1");
  }
  if ($module == "page_manager" && $api == "pages_default") {
    return array("version" => "1");
  }
  if ($module == "panels_mini" && $api == "panels_default") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function uwc_taxonomy_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_default_search_api_index().
 */
function uwc_taxonomy_default_search_api_index() {
  $items = array();
  $items['taxonomy_index'] = entity_import('search_api_index', '{
    "name" : "Taxonomy index",
    "machine_name" : "taxonomy_index",
    "description" : "Index of taxonomy terms and their relationships.",
    "server" : "default",
    "item_type" : "node",
    "options" : {
      "index_directly" : 1,
      "cron_limit" : "50",
      "fields" : {
        "type" : { "type" : "string" },
        "status" : { "type" : "boolean" },
        "created" : { "type" : "date" },
        "changed" : { "type" : "date" },
        "author" : { "type" : "integer", "entity_type" : "user" },
        "field_topics" : { "type" : "list\\u003Cinteger\\u003E", "entity_type" : "taxonomy_term" },
        "field_radioactivity" : { "type" : "decimal" },
        "field_group_type" : { "type" : "integer", "entity_type" : "taxonomy_term" },
        "og_group_ref" : { "type" : "list\\u003Cinteger\\u003E", "entity_type" : "node" },
        "field_resource_type" : { "type" : "integer", "entity_type" : "taxonomy_term" },
        "field_news_type" : { "type" : "integer", "entity_type" : "taxonomy_term" },
        "field_discussion_type" : { "type" : "integer", "entity_type" : "taxonomy_term" },
        "search_api_language" : { "type" : "string" },
        "search_api_access_node" : { "type" : "list\\u003Cstring\\u003E" },
        "search_api_combined_1" : { "type" : "list\\u003Cinteger\\u003E" },
        "field_resource_type:name" : { "type" : "string" },
        "field_news_type:name" : { "type" : "string" },
        "field_discussion_type:name" : { "type" : "string" }
      },
      "data_alter_callbacks" : {
        "search_api_alter_bundle_filter" : {
          "status" : 0,
          "weight" : "-10",
          "settings" : { "default" : "1", "bundles" : [] }
        },
        "search_api_alter_node_status" : { "status" : 0, "weight" : "0", "settings" : [] },
        "search_api_alter_add_combined" : {
          "status" : 1,
          "weight" : "0",
          "settings" : { "fields" : { "search_api_combined_1" : {
                "name" : "Content Subtype",
                "imitate" : "field_event_type",
                "fields" : [
                  "field_resource_type",
                  "field_event_type",
                  "field_discussion_type",
                  "field_news_type",
                  "field_page_type"
                ],
                "description" : "A @type combined of the following fields: Resource Type, Event Type, Poll Type, Discussion Type, News Type, Page Type."
              }
            }
          }
        },
        "search_api_alter_node_access" : { "status" : 1, "weight" : "0", "settings" : [] },
        "search_api_alter_add_viewed_entity" : { "status" : 0, "weight" : "0", "settings" : { "mode" : "full" } },
        "search_api_alter_add_url" : { "status" : 0, "weight" : "0", "settings" : [] },
        "search_api_alter_add_aggregation" : { "status" : 0, "weight" : "0", "settings" : [] },
        "search_api_alter_add_hierarchy" : { "status" : 0, "weight" : "0", "settings" : { "fields" : [] } }
      },
      "processors" : {
        "search_api_case_ignore" : { "status" : 0, "weight" : "0", "settings" : { "fields" : [] } },
        "search_api_html_filter" : {
          "status" : 0,
          "weight" : "10",
          "settings" : {
            "fields" : [],
            "title" : 0,
            "alt" : 1,
            "tags" : "h1 = 5\\r\\nh2 = 3\\r\\nh3 = 2\\r\\nstrong = 2\\r\\nb = 2\\r\\nem = 1.5\\r\\nu = 1.5"
          }
        },
        "search_api_tokenizer" : {
          "status" : 0,
          "weight" : "20",
          "settings" : { "fields" : [], "spaces" : "[^[:alnum:]]", "ignorable" : "[\\u0027]" }
        },
        "search_api_stopwords" : {
          "status" : 0,
          "weight" : "30",
          "settings" : {
            "fields" : [],
            "file" : "",
            "stopwords" : "but\\r\\ndid\\r\\nthe this that those\\r\\netc"
          }
        },
        "search_api_highlighting" : {
          "status" : 0,
          "weight" : "35",
          "settings" : {
            "prefix" : "\\u003Cstrong\\u003E",
            "suffix" : "\\u003C\\/strong\\u003E",
            "excerpt" : 1,
            "excerpt_length" : "256",
            "highlight" : "always"
          }
        }
      }
    },
    "enabled" : "1",
    "read_only" : "0",
    "rdf_mapping" : []
  }');
  return $items;
}

/**
 * Implements hook_default_search_api_sort().
 */
function uwc_taxonomy_default_search_api_sort() {
  $items = array();
  $items['taxonomy_index__changed'] = entity_import('search_api_sort', '{
    "index_id" : "taxonomy_index",
    "field" : "changed",
    "name" : "Last updated",
    "enabled" : "1",
    "weight" : "0",
    "identifier" : "taxonomy_index__changed",
    "default_sort" : "0",
    "default_sort_no_terms" : "0",
    "default_order" : "desc",
    "options" : { "field_name" : "Date changed" },
    "rdf_mapping" : []
  }');
  $items['taxonomy_index__created'] = entity_import('search_api_sort', '{
    "index_id" : "taxonomy_index",
    "field" : "created",
    "name" : "Date created",
    "enabled" : "1",
    "weight" : "0",
    "identifier" : "taxonomy_index__created",
    "default_sort" : "0",
    "default_sort_no_terms" : "0",
    "default_order" : "desc",
    "options" : { "field_name" : "Date created" },
    "rdf_mapping" : []
  }');
  $items['taxonomy_index__field_radioactivity'] = entity_import('search_api_sort', '{
    "index_id" : "taxonomy_index",
    "field" : "field_radioactivity",
    "name" : "Most viewed",
    "enabled" : "1",
    "weight" : "0",
    "identifier" : "taxonomy_index__field_radioactivity",
    "default_sort" : "0",
    "default_sort_no_terms" : "0",
    "default_order" : "desc",
    "options" : { "field_name" : "Radioactivity" },
    "rdf_mapping" : []
  }');
  $items['taxonomy_index__search_api_relevance'] = entity_import('search_api_sort', '{
    "index_id" : "taxonomy_index",
    "field" : "search_api_relevance",
    "name" : "Relevance",
    "enabled" : "1",
    "weight" : "0",
    "identifier" : "taxonomy_index__search_api_relevance",
    "default_sort" : "0",
    "default_sort_no_terms" : "0",
    "default_order" : "desc",
    "options" : { "field_name" : "search_api_relevance" },
    "rdf_mapping" : []
  }');
  return $items;
}
