<?php
/**
 * @file
 * uwc_taxonomy.panels_default.inc
 */

/**
 * Implements hook_default_panels_mini().
 */
function uwc_taxonomy_default_panels_mini() {
  $export = array();

  $mini = new stdClass();
  $mini->disabled = FALSE; /* Edit this to true to make a default mini disabled initially */
  $mini->api_version = 1;
  $mini->name = 'taxonomy_term_links';
  $mini->category = '';
  $mini->admin_title = 'Taxonomy term links';
  $mini->admin_description = 'Just our term links';
  $mini->requiredcontexts = array(
    0 => array(
      'identifier' => 'Taxonomy term',
      'keyword' => 'taxonomy_term',
      'name' => 'entity:taxonomy_term',
      'entity_id' => '',
      'id' => 1,
    ),
  );
  $mini->contexts = array();
  $mini->relationships = array();
  $display = new panels_display();
  $display->layout = 'twocol';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'one_main' => NULL,
      'left' => NULL,
      'right' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-1';
    $pane->panel = 'left';
    $pane->type = 'flag_widget';
    $pane->subtype = 'flag_widget';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'return' => 'Finish',
      'cancel' => 'Cancel',
      'override_title' => 0,
      'override_title_text' => '',
      'node' => '%taxonomy_term:tid',
      'flag' => 'commons_follow_term',
      'form_build_id' => 'form-X5IIDuavP-O0EKNyNv22NY0o-biMT4QqhHl56vuHlxY',
      'form_token' => 'whrV7JFVJ1AJkiEBpbUdGKJFh_GyCG3GuUvO7PwWB7I',
      'form_id' => 'panels_flags_flag_link',
      'op' => 'Finish',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array(
      'css_id' => '',
      'css_class' => 'follow-button',
    );
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $display->content['new-1'] = $pane;
    $display->panels['left'][0] = 'new-1';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = '0';
  $mini->display = $display;
  $export['taxonomy_term_links'] = $mini;

  return $export;
}
