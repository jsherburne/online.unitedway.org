<?php
/**
 * @file
 * uwc_mailcoment.mailhandler_mailbox.inc
 */

/**
 * Implements hook_default_mailhandler_mailbox().
 */
function uwc_mailcoment_default_mailhandler_mailbox() {
  $export = array();

  $mailbox = new stdClass();
  $mailbox->disabled = FALSE; /* Edit this to true to make a default mailbox disabled initially */
  $mailbox->api_version = 2;
  $mailbox->mail = 'uwc_mailcomment_mailbox';
  $mailbox->admin_title = 'Uwc mailbox';
  $mailbox->settings = array(
    'type' => 'imap',
    'folder' => 'INBOX',
    'domain' => 'localhost',
    'port' => '143',
    'insecure' => 1,
    'name' => 'uwonline',
    'pass' => '@bc4P)OJig^I',
    'extraimap' => '/notls',
    'limit' => '0',
    'encoding' => 'UTF-8',
    'flag_after_read' => 1,
    'delete_after_read' => 0,
    'fromheader' => 'From',
    'security' => '0',
    'replies' => '0',
    'retrieve' => 'MailhandlerPhpImapRetrieve',
    'readonly' => 0,
  );
  $export['uwc_mailcomment_mailbox'] = $mailbox;

  return $export;
}
