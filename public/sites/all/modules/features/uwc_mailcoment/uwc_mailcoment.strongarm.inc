<?php
/**
 * @file
 * uwc_mailcoment.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function uwc_mailcoment_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'mailcomment_message_notify_commons_notify_comment_created';
  $strongarm->value = 1;
  $export['mailcomment_message_notify_commons_notify_comment_created'] = $strongarm;

  return $export;
}
