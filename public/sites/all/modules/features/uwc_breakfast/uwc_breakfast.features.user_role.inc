<?php
/**
 * @file
 * uwc_breakfast.features.user_role.inc
 */

/**
 * Implements hook_user_default_roles().
 */
function uwc_breakfast_user_default_roles() {
  $roles = array();

  // Exported role: breakfast recommender.
  $roles['breakfast recommender'] = array(
    'name' => 'breakfast recommender',
    'weight' => 4,
  );

  return $roles;
}
