<?php
/**
 * @file
 * uwc_breakfast.features.inc
 */

/**
 * Implements hook_views_api().
 */
function uwc_breakfast_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_flag_default_flags().
 */
function uwc_breakfast_flag_default_flags() {
  $flags = array();
  // Exported flag: "Recommend for Breakfast".
  $flags['breakfast_recommendation'] = array(
    'entity_type' => 'node',
    'title' => 'Recommend for Breakfast',
    'global' => 1,
    'types' => array(
      0 => 'post',
      1 => 'event',
      2 => 'group',
      3 => 'guide',
      4 => 'landing_page',
      5 => 'news',
      6 => 'organization',
      7 => 'page',
      8 => 'poll',
      9 => 'document',
      10 => 'wrapped_system',
    ),
    'flag_short' => 'Recommend for Breakfast',
    'flag_long' => 'Recommend this content for Breakfast',
    'flag_message' => 'Breakfast recommendation submitted',
    'unflag_short' => 'Remove recommendation for Breakfast',
    'unflag_long' => 'Un-recommend this content for Beakfast',
    'unflag_message' => 'Breakfast recommendation revoked',
    'unflag_denied_text' => '',
    'link_type' => 'confirm',
    'weight' => 0,
    'show_in_links' => array(
      'full' => 0,
      'teaser' => 0,
      'rss' => 0,
      'diff_standard' => 0,
      'token' => 0,
      'search_result' => 0,
      'compact_teaser' => 0,
      'related_content' => 0,
      'revision' => 0,
    ),
    'show_as_field' => FALSE,
    'show_on_form' => 0,
    'access_author' => '',
    'show_contextual_link' => 0,
    'i18n' => 0,
    'flag_confirmation' => 'Breakfast submission',
    'unflag_confirmation' => 'Remove Breakfast submission',
    'module' => 'uwc_breakfast',
    'locked' => array(
      0 => 'name',
    ),
    'api_version' => 3,
  );
  return $flags;

}
