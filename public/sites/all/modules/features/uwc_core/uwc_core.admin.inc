<?php

/**
 * Form constructor for the UW IP address settings form.
 */
function uwc_core_settings_form($form, &$form_state) {
  global $user;

  $form['uw_ip'] = array(
    '#type' => 'textfield',
    '#title' => t('United Way IP Address (for Google Analytics filtering)'),
    '#description' => t('Example: current user would be ') . '"' . token_replace('[current-user:uw-user]', array('current-user' => $user)) . '" ' . t('for your IP: ') . ip_address(),
    '#default_value' => variable_get('uw_ip', ''),
  );

  $form['#submit'][] = 'uwc_core_settings_form_submit';
  return system_settings_form($form);
}

/**
 * Validation for uwc_core_settings_form()
 */
function uwc_core_settings_form_validate($form, &$form_state) {
  //todo: probably should check valid IP address
}

/**
 * Form submission handler for uwc_core_settings_form().
 */
function uwc_core_settings_form_submit($form, $form_state) {
  variable_set('uw_ip', $form_state['values']['uw_ip']);
}