<?php
/**
 * @file
 * uwc_core.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function uwc_core_field_group_info() {
  $export = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_slideshow_configuration|node|news|form';
  $field_group->group_name = 'group_slideshow_configuration';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'news';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Slideshow Configuration',
    'weight' => '45',
    'children' => array(
      0 => 'field_slide_title',
      1 => 'field_slide_image',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Slideshow Configuration',
      'instance_settings' => array(
        'required_fields' => 0,
        'classes' => 'group-slideshow-configuration field-group-fieldset',
        'description' => '',
      ),
      'formatter' => 'collapsible',
    ),
  );
  $export['group_slideshow_configuration|node|news|form'] = $field_group;

  return $export;
}
