<?php
/**
 * @file
 * uwc_core.features.menu_custom.inc
 */

/**
 * Implements hook_menu_default_menu_custom().
 */
function uwc_core_menu_default_menu_custom() {
  $menus = array();

  // Exported menu: menu-quick-links.
  $menus['menu-quick-links'] = array(
    'menu_name' => 'menu-quick-links',
    'title' => 'Quick Links',
    'description' => 'Fast links for the home page.',
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Fast links for the home page.');
  t('Quick Links');


  return $menus;
}
