<?php
/**
 * @file
 * uwc_core.ds.inc
 */

/**
 * Implements hook_ds_view_modes_info().
 */
function uwc_core_ds_view_modes_info() {
  $export = array();

  $ds_view_mode = new stdClass();
  $ds_view_mode->api_version = 1;
  $ds_view_mode->view_mode = 'compact_teaser';
  $ds_view_mode->label = 'Compact Teaser';
  $ds_view_mode->entities = array(
    'node' => 'node',
    'profile2' => 'profile2',
  );
  $export['compact_teaser'] = $ds_view_mode;

  $ds_view_mode = new stdClass();
  $ds_view_mode->api_version = 1;
  $ds_view_mode->view_mode = 'related_content';
  $ds_view_mode->label = 'Related content';
  $ds_view_mode->entities = array(
    'node' => 'node',
    'profile2' => 'profile2',
  );
  $export['related_content'] = $ds_view_mode;

  $ds_view_mode = new stdClass();
  $ds_view_mode->api_version = 1;
  $ds_view_mode->view_mode = 'search_result';
  $ds_view_mode->label = 'Search result';
  $ds_view_mode->entities = array(
    'node' => 'node',
    'profile2' => 'profile2',
  );
  $export['search_result'] = $ds_view_mode;

  return $export;
}
