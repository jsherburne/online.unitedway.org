<?php
/**
 * @file
 * uwc_core.quicktabs.inc
 */

/**
 * Implements hook_quicktabs_default_quicktabs().
 */
function uwc_core_quicktabs_default_quicktabs() {
  $export = array();

  $quicktabs = new stdClass();
  $quicktabs->disabled = FALSE; /* Edit this to true to make a default quicktabs disabled initially */
  $quicktabs->api_version = 1;
  $quicktabs->machine_name = 'uwc_get_connect';
  $quicktabs->ajax = 0;
  $quicktabs->hide_empty_tabs = FALSE;
  $quicktabs->default_tab = 0;
  $quicktabs->title = 'Get Connected';
  $quicktabs->tabs = array(
    0 => array(
      'vid' => 'uwc_group_content',
      'display' => 'uwc_recent_group_content',
      'args' => '',
      'title' => 'Recent',
      'weight' => '-100',
      'type' => 'view',
    ),
    1 => array(
      'vid' => 'uwc_group_content',
      'display' => 'uwc_popular_group_content',
      'args' => '',
      'title' => 'Popular',
      'weight' => '-99',
      'type' => 'view',
    ),
    2 => array(
      'vid' => 'uwc_recommended_content',
      'display' => 'uwc_recomended_content_pane',
      'args' => '',
      'title' => 'Recommended',
      'weight' => '-98',
      'type' => 'view',
    ),
    3 => array(
      'vid' => 'uwc_personal_group_content',
      'display' => 'uwc_personal_group_content',
      'args' => '',
      'title' => 'My Groups',
      'weight' => '-97',
      'type' => 'view',
    ),
  );
  $quicktabs->renderer = 'quicktabs';
  $quicktabs->style = 'nostyle';
  $quicktabs->options = array();

  // Translatables
  // Included for use with string extractors like potx.
  t('Get Connected');
  t('My Groups');
  t('Popular');
  t('Recent');
  t('Recommended');

  $export['uwc_get_connect'] = $quicktabs;

  $quicktabs = new stdClass();
  $quicktabs->disabled = FALSE; /* Edit this to true to make a default quicktabs disabled initially */
  $quicktabs->api_version = 1;
  $quicktabs->machine_name = 'uwc_homepage_picks';
  $quicktabs->ajax = 0;
  $quicktabs->hide_empty_tabs = TRUE;
  $quicktabs->default_tab = 0;
  $quicktabs->title = 'Find Ideas & Resources';
  $quicktabs->tabs = array(
    0 => array(
      'vid' => 'uwc_recommended_content',
      'display' => 'uww_picks',
      'args' => '',
      'title' => 'Recommended',
      'weight' => '-100',
      'type' => 'view',
    ),
    1 => array(
      'vid' => 'uwc_radioactivity',
      'display' => 'site_wide',
      'args' => '',
      'title' => 'Community Favorites',
      'weight' => '-99',
      'type' => 'view',
    ),
  );
  $quicktabs->renderer = 'quicktabs';
  $quicktabs->style = 'nostyle';
  $quicktabs->options = array();

  // Translatables
  // Included for use with string extractors like potx.
  t('Community Favorites');
  t('Find Ideas & Resources');
  t('Recommended');

  $export['uwc_homepage_picks'] = $quicktabs;

  return $export;
}
