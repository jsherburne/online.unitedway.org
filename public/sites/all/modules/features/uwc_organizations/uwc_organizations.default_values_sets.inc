<?php
/**
 * @file
 * uwc_organizations.default_values_sets.inc
 */

/**
 * Implements hook_default_values_values().
 */
function uwc_organizations_default_values_values() {
  $export = array();

  $values = new stdClass();
  $values->disabled = FALSE; /* Edit this to true to make a default values disabled initially */
  $values->api_version = 1;
  $values->name = 'organization_types';
  $values->title = 'Organization Types';
  $values->description = 'Categories of United Way organizations.';
  $values->data = array(
    0 => array(
      'key' => 'local_united_way',
      'value' => 'Local United Way',
      'weight' => '0',
    ),
    1 => array(
      'key' => 'global_corporate_leader',
      'value' => 'Global Corporate Leader',
      'weight' => '1',
    ),
  );
  $export['organization_types'] = $values;

  return $export;
}
