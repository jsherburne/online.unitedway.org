<?php
/**
 * @file
 * uwc_organizations.features.features_overrides.inc
 */

/**
 * Implements hook_features_override_default_overrides().
 */
function uwc_organizations_features_override_default_overrides() {
  // This code is only used for UI in features. Exported alters hooks do the magic.
  $overrides = array();

  // Exported overrides for: field_instance

 return $overrides;
}
