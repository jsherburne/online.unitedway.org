<?php
/**
 * @file
 * uwc_organizations.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function uwc_organizations_field_group_info() {
  $export = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_general_contact|node|organization|default';
  $field_group->group_name = 'group_general_contact';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'organization';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'General contact',
    'weight' => '5',
    'children' => array(
      0 => 'field_org_fax',
      1 => 'field_org_gen_email',
      2 => 'field_org_phone',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-general-contact field-group-fieldset',
      ),
    ),
  );
  $export['group_general_contact|node|organization|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_general_contact|node|organization|full';
  $field_group->group_name = 'group_general_contact';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'organization';
  $field_group->mode = 'full';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'General contact',
    'weight' => '2',
    'children' => array(
      0 => 'field_org_fax',
      1 => 'field_org_gen_email',
      2 => 'field_org_phone',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'General contact',
      'instance_settings' => array(
        'classes' => 'group-general-contact field-group-fieldset field-group-div',
        'description' => '',
        'show_label' => 1,
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'fast',
      ),
      'formatter' => 'open',
    ),
  );
  $export['group_general_contact|node|organization|full'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_node_actions|node|organization|teaser';
  $field_group->group_name = 'group_node_actions';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'organization';
  $field_group->mode = 'teaser';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Node Actions',
    'weight' => '23',
    'children' => array(
      0 => 'comment_count',
      1 => 'rate_integration_1',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'Node Actions',
      'instance_settings' => array(
        'classes' => 'group-node-actions field-group-div',
        'description' => '',
        'show_label' => '0',
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'fast',
      ),
      'formatter' => 'open',
    ),
  );
  $export['group_node_actions|node|organization|teaser'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_org_contact|node|organization|form';
  $field_group->group_name = 'group_org_contact';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'organization';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Contact information',
    'weight' => '10',
    'children' => array(
      0 => 'field_org_contacts',
      1 => 'field_org_fax',
      2 => 'field_org_gen_email',
      3 => 'field_org_info_ref',
      4 => 'field_org_phone',
      5 => 'field_org_pledge_link',
      6 => 'field_org_website',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Contact information',
      'instance_settings' => array(
        'required_fields' => 1,
        'classes' => 'group-org-contact field-group-fieldset',
        'description' => '',
      ),
      'formatter' => 'collapsible',
    ),
  );
  $export['group_org_contact|node|organization|form'] = $field_group;

  return $export;
}
