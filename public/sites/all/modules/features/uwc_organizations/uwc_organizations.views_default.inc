<?php
/**
 * @file
 * uwc_organizations.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function uwc_organizations_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'uwc_ach_codes';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'ACH Codes';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'ACH Codes';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'none';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['style_plugin'] = 'table';
  $handler->display->display_options['style_options']['columns'] = array(
    'field_uw_id' => 'field_uw_id',
    'field_org_name' => 'field_org_name',
    'field_address' => 'field_address',
    'field_address_1' => 'field_address_1',
    'field_address_2' => 'field_address_2',
    'field_ach_code' => 'field_ach_code',
  );
  $handler->display->display_options['style_options']['default'] = 'field_org_name';
  $handler->display->display_options['style_options']['info'] = array(
    'field_uw_id' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_org_name' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_address' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_address_1' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_address_2' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_ach_code' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
  );
  $handler->display->display_options['style_options']['caption'] = 'ACH Codes';
  /* Field: Content: Nid */
  $handler->display->display_options['fields']['nid']['id'] = 'nid';
  $handler->display->display_options['fields']['nid']['table'] = 'node';
  $handler->display->display_options['fields']['nid']['field'] = 'nid';
  $handler->display->display_options['fields']['nid']['label'] = '';
  $handler->display->display_options['fields']['nid']['exclude'] = TRUE;
  $handler->display->display_options['fields']['nid']['element_label_colon'] = FALSE;
  /* Field: Content: Organization Number */
  $handler->display->display_options['fields']['field_uw_id']['id'] = 'field_uw_id';
  $handler->display->display_options['fields']['field_uw_id']['table'] = 'field_data_field_uw_id';
  $handler->display->display_options['fields']['field_uw_id']['field'] = 'field_uw_id';
  $handler->display->display_options['fields']['field_uw_id']['label'] = 'ID';
  $handler->display->display_options['fields']['field_uw_id']['element_label_colon'] = FALSE;
  /* Field: Content: Organization Name */
  $handler->display->display_options['fields']['field_org_name']['id'] = 'field_org_name';
  $handler->display->display_options['fields']['field_org_name']['table'] = 'field_data_field_org_name';
  $handler->display->display_options['fields']['field_org_name']['field'] = 'field_org_name';
  $handler->display->display_options['fields']['field_org_name']['label'] = 'Company';
  $handler->display->display_options['fields']['field_org_name']['alter']['make_link'] = TRUE;
  $handler->display->display_options['fields']['field_org_name']['alter']['path'] = 'node/[nid]';
  $handler->display->display_options['fields']['field_org_name']['alter']['alt'] = '[field_org_name]';
  $handler->display->display_options['fields']['field_org_name']['element_label_colon'] = FALSE;
  /* Field: Content: Business Address */
  $handler->display->display_options['fields']['field_address']['id'] = 'field_address';
  $handler->display->display_options['fields']['field_address']['table'] = 'field_data_field_address';
  $handler->display->display_options['fields']['field_address']['field'] = 'field_address';
  $handler->display->display_options['fields']['field_address']['label'] = 'City';
  $handler->display->display_options['fields']['field_address']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_address']['click_sort_column'] = 'locality';
  $handler->display->display_options['fields']['field_address']['type'] = 'addressfield_components';
  $handler->display->display_options['fields']['field_address']['settings'] = array(
    'components' => array(
      'locality' => 'locality',
    ),
    'separator' => ', ',
  );
  /* Field: Content: Business Address */
  $handler->display->display_options['fields']['field_address_1']['id'] = 'field_address_1';
  $handler->display->display_options['fields']['field_address_1']['table'] = 'field_data_field_address';
  $handler->display->display_options['fields']['field_address_1']['field'] = 'field_address';
  $handler->display->display_options['fields']['field_address_1']['label'] = 'State/Provice';
  $handler->display->display_options['fields']['field_address_1']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_address_1']['click_sort_column'] = 'administrative_area';
  $handler->display->display_options['fields']['field_address_1']['type'] = 'addressfield_components';
  $handler->display->display_options['fields']['field_address_1']['settings'] = array(
    'components' => array(
      'administrative_area' => 'administrative_area',
    ),
    'separator' => ', ',
  );
  /* Field: Content: Business Address */
  $handler->display->display_options['fields']['field_address_2']['id'] = 'field_address_2';
  $handler->display->display_options['fields']['field_address_2']['table'] = 'field_data_field_address';
  $handler->display->display_options['fields']['field_address_2']['field'] = 'field_address';
  $handler->display->display_options['fields']['field_address_2']['label'] = 'Country';
  $handler->display->display_options['fields']['field_address_2']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_address_2']['click_sort_column'] = 'country';
  $handler->display->display_options['fields']['field_address_2']['type'] = 'addressfield_country';
  $handler->display->display_options['fields']['field_address_2']['settings'] = array(
    'use_widget_handlers' => 1,
    'format_handlers' => array(
      'address' => 'address',
    ),
  );
  /* Field: Content: ACH Code */
  $handler->display->display_options['fields']['field_ach_code']['id'] = 'field_ach_code';
  $handler->display->display_options['fields']['field_ach_code']['table'] = 'field_data_field_ach_code';
  $handler->display->display_options['fields']['field_ach_code']['field'] = 'field_ach_code';
  $handler->display->display_options['fields']['field_ach_code']['element_label_colon'] = FALSE;
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'organization' => 'organization',
  );

  /* Display: ACH Codes page */
  $handler = $view->new_display('page', 'ACH Codes page', 'uwc_ach_code_page');
  $handler->display->display_options['path'] = 'ach-codes';
  $export['uwc_ach_codes'] = $view;

  return $export;
}
