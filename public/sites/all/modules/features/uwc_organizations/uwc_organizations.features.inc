<?php
/**
 * @file
 * uwc_organizations.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function uwc_organizations_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "ds" && $api == "ds") {
    return array("version" => "1");
  }
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
  if ($module == "values" && $api == "default_values_sets") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function uwc_organizations_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function uwc_organizations_node_info() {
  $items = array(
    'organization' => array(
      'name' => t('Organization'),
      'base' => 'node_content',
      'description' => t('An Organization captures the profile and related content of a United Way institution.'),
      'has_title' => '1',
      'title_label' => t('Name'),
      'help' => '',
    ),
  );
  return $items;
}
