<?php
/**
 * @file
 * uwc_devel.features.user_role.inc
 */

/**
 * Implements hook_user_default_roles().
 */
function uwc_devel_user_default_roles() {
  $roles = array();

  // Exported role: developer.
  $roles['developer'] = array(
    'name' => 'developer',
    'weight' => 3,
  );

  return $roles;
}
