<?php
/**
 * @file
 * uwc_posts.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function uwc_posts_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "ds" && $api == "ds") {
    return array("version" => "1");
  }
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function uwc_posts_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_default_message_type().
 */
function uwc_posts_default_message_type() {
  $items = array();
  $items['commons_posts_post_created'] = entity_import('message_type', '{
    "name" : "commons_posts_post_created",
    "description" : "Commons Posts - Post Created",
    "argument_keys" : [],
    "argument" : [],
    "category" : "message_type",
    "data" : {
      "token options" : { "clear" : 0 },
      "purge" : { "override" : 0, "enabled" : 0, "quota" : "", "days" : "" }
    },
    "language" : "",
    "arguments" : null,
    "message_text" : { "und" : [
        {
          "value" : "[message:user:picture:35x35]",
          "format" : "filtered_html",
          "safe_value" : "[message:user:picture:35x35]"
        },
        {
          "value" : "\\u003Ca href=[message:user:url:absolute]\\u003E[message:user:name]\\u003C\\/a\\u003E posted \\u003Ca href=[message:field-target-nodes:0:url]\\u003E[message:field-target-nodes:0:title]\\u003C\\/a\\u003E",
          "format" : "full_html",
          "safe_value" : "\\u003Ca href=[message:user:url:absolute]\\u003E[message:user:name]\\u003C\\/a\\u003E posted \\u003Ca href=[message:field-target-nodes:0:url]\\u003E[message:field-target-nodes:0:title]\\u003C\\/a\\u003E"
        },
        {
          "value" : "[commons-groups:in-groups-text]",
          "format" : "full_html",
          "safe_value" : "[commons-groups:in-groups-text]"
        }
      ]
    },
    "rdf_mapping" : []
  }');
  return $items;
}

/**
 * Implements hook_node_info().
 */
function uwc_posts_node_info() {
  $items = array(
    'post' => array(
      'name' => t('Discussion'),
      'base' => 'node_content',
      'description' => t('Start a conversation or share some information.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
