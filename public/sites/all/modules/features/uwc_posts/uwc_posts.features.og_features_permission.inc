<?php
/**
 * @file
 * uwc_posts.features.og_features_permission.inc
 */

/**
 * Implements hook_og_features_default_permissions().
 */
function uwc_posts_og_features_default_permissions() {
  $permissions = array();

  // Exported og permission: 'node:group:create post content'
  $permissions['node:group:create post content'] = array(
    'roles' => array(
      'administrator member' => 'administrator member',
      'group leader' => 'group leader',
      'member' => 'member',
      'non-member' => 'non-member',
      'topic leader' => 'topic leader',
    ),
  );

  // Exported og permission: 'node:group:delete any post content'
  $permissions['node:group:delete any post content'] = array(
    'roles' => array(
      'administrator member' => 'administrator member',
      'group leader' => 'group leader',
    ),
  );

  // Exported og permission: 'node:group:delete own post content'
  $permissions['node:group:delete own post content'] = array(
    'roles' => array(
      'administrator member' => 'administrator member',
      'group leader' => 'group leader',
      'member' => 'member',
      'topic leader' => 'topic leader',
    ),
  );

  // Exported og permission: 'node:group:update any post content'
  $permissions['node:group:update any post content'] = array(
    'roles' => array(
      'administrator member' => 'administrator member',
      'group leader' => 'group leader',
    ),
  );

  // Exported og permission: 'node:group:update own post content'
  $permissions['node:group:update own post content'] = array(
    'roles' => array(
      'administrator member' => 'administrator member',
      'group leader' => 'group leader',
      'member' => 'member',
      'non-member' => 'non-member',
      'topic leader' => 'topic leader',
    ),
  );

  return $permissions;
}
