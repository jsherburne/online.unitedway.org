<?php
/**
 * @file
 * uwc_posts.ds.inc
 */

/**
 * Implements hook_ds_field_settings_info().
 */
function uwc_posts_ds_field_settings_info() {
  $export = array();

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'comment|comment_node_post|default';
  $ds_fieldsetting->entity_type = 'comment';
  $ds_fieldsetting->bundle = 'comment_node_post';
  $ds_fieldsetting->view_mode = 'default';
  $ds_fieldsetting->settings = array(
    'links' => array(
      'weight' => '1',
      'label' => 'hidden',
      'format' => 'default',
    ),
    'post_date' => array(
      'weight' => '2',
      'label' => 'hidden',
      'format' => 'ds_post_date_time_ago_dynamic',
      'formatter_settings' => array(
        'ft' => array(),
      ),
    ),
    'flag_inappropriate_comment' => array(
      'weight' => '5',
      'label' => 'hidden',
      'format' => 'default',
    ),
    'profile2' => array(
      'weight' => '0',
      'label' => 'hidden',
      'format' => 'compact_teaser',
    ),
    'rate_integration_1' => array(
      'weight' => '6',
      'label' => 'hidden',
      'format' => 'default',
    ),
  );
  $export['comment|comment_node_post|default'] = $ds_fieldsetting;

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'node|post|compact_teaser';
  $ds_fieldsetting->entity_type = 'node';
  $ds_fieldsetting->bundle = 'post';
  $ds_fieldsetting->view_mode = 'compact_teaser';
  $ds_fieldsetting->settings = array(
    'private_group_flag' => array(
      'weight' => '4',
      'label' => 'hidden',
      'format' => 'default',
    ),
    'author' => array(
      'weight' => '1',
      'label' => 'hidden',
      'format' => 'author_linked',
    ),
    'post_date' => array(
      'weight' => '0',
      'label' => 'hidden',
      'format' => 'ds_post_date_rich_snippets_published_date',
    ),
    'field_discussion_type' => array(
      'formatter_settings' => array(
        'ft' => array(
          'func' => 'theme_field',
        ),
      ),
    ),
  );
  $export['node|post|compact_teaser'] = $ds_fieldsetting;

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'node|post|default';
  $ds_fieldsetting->entity_type = 'node';
  $ds_fieldsetting->bundle = 'post';
  $ds_fieldsetting->view_mode = 'default';
  $ds_fieldsetting->settings = array(
    'links' => array(
      'weight' => '9',
      'label' => 'hidden',
      'format' => 'default',
    ),
    'comments' => array(
      'weight' => '10',
      'label' => 'hidden',
      'format' => 'default',
    ),
    'author' => array(
      'weight' => '2',
      'label' => 'hidden',
      'format' => 'author_linked',
    ),
    'post_date' => array(
      'weight' => '1',
      'label' => 'hidden',
      'format' => 'ds_post_date_rich_snippets_published_date',
    ),
    'rate_integration_1' => array(
      'weight' => '8',
      'label' => 'hidden',
      'format' => 'default',
    ),
    'field_discussion_type' => array(
      'formatter_settings' => array(
        'ft' => array(
          'classes' => 'type',
        ),
      ),
    ),
    'og_group_ref' => array(
      'formatter_settings' => array(
        'ft' => array(
          'lb' => 'Communities of Practice',
        ),
      ),
    ),
  );
  $export['node|post|default'] = $ds_fieldsetting;

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'node|post|full';
  $ds_fieldsetting->entity_type = 'node';
  $ds_fieldsetting->bundle = 'post';
  $ds_fieldsetting->view_mode = 'full';
  $ds_fieldsetting->settings = array(
    'private_group_flag' => array(
      'weight' => '1',
      'label' => 'hidden',
      'format' => 'default',
    ),
    'comments' => array(
      'weight' => '11',
      'label' => 'hidden',
      'format' => 'default',
    ),
    'author' => array(
      'weight' => '3',
      'label' => 'hidden',
      'format' => 'author_linked',
    ),
    'post_date' => array(
      'weight' => '2',
      'label' => 'hidden',
      'format' => 'ds_post_date_rich_snippets_published_date',
    ),
    'flag_commons_follow_node' => array(
      'weight' => '6',
      'label' => 'hidden',
      'format' => 'default',
    ),
    'flag_breakfast_recommendation' => array(
      'weight' => '7',
      'label' => 'hidden',
      'format' => 'default',
    ),
    'rate_integration_1' => array(
      'weight' => '5',
      'label' => 'hidden',
      'format' => 'default',
    ),
    'field_discussion_type' => array(
      'formatter_settings' => array(
        'ft' => array(
          'func' => 'theme_field',
          'classes' => 'type',
        ),
      ),
    ),
    'og_group_ref' => array(
      'formatter_settings' => array(
        'ft' => array(
          'lb' => 'Communities of Practice',
        ),
      ),
    ),
  );
  $export['node|post|full'] = $ds_fieldsetting;

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'node|post|search_result';
  $ds_fieldsetting->entity_type = 'node';
  $ds_fieldsetting->bundle = 'post';
  $ds_fieldsetting->view_mode = 'search_result';
  $ds_fieldsetting->settings = array(
    'private_group_flag' => array(
      'weight' => '2',
      'label' => 'hidden',
      'format' => 'default',
    ),
    'links' => array(
      'weight' => '9',
      'label' => 'hidden',
      'format' => 'default',
    ),
    'author' => array(
      'weight' => '4',
      'label' => 'hidden',
      'format' => 'author_linked',
    ),
    'post_date' => array(
      'weight' => '3',
      'label' => 'hidden',
      'format' => 'ds_post_date_rich_snippets_published_date',
    ),
    'rate_integration_1' => array(
      'weight' => '8',
      'label' => 'hidden',
      'format' => 'default',
    ),
    'field_discussion_type' => array(
      'formatter_settings' => array(
        'ft' => array(
          'func' => 'theme_field',
        ),
      ),
    ),
    'og_group_ref' => array(
      'formatter_settings' => array(
        'ft' => array(
          'lb' => 'Communities of Practice',
        ),
      ),
    ),
  );
  $export['node|post|search_result'] = $ds_fieldsetting;

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'node|post|teaser';
  $ds_fieldsetting->entity_type = 'node';
  $ds_fieldsetting->bundle = 'post';
  $ds_fieldsetting->view_mode = 'teaser';
  $ds_fieldsetting->settings = array(
    'private_group_flag' => array(
      'weight' => '3',
      'label' => 'hidden',
      'format' => 'default',
    ),
    'author' => array(
      'weight' => '5',
      'label' => 'hidden',
      'format' => 'author_linked',
      'formatter_settings' => array(
        'ft' => array(),
      ),
    ),
    'post_date' => array(
      'weight' => '1',
      'label' => 'hidden',
      'format' => 'ds_post_date_rich_snippets_published_date',
      'formatter_settings' => array(
        'ft' => array(),
      ),
    ),
    'comment_count' => array(
      'weight' => '10',
      'label' => 'hidden',
      'format' => 'default',
    ),
    'profile2' => array(
      'weight' => '0',
      'label' => 'hidden',
      'format' => 'compact_teaser',
    ),
    'rate_integration_1' => array(
      'weight' => '9',
      'label' => 'hidden',
      'format' => 'default',
    ),
    'field_discussion_type' => array(
      'formatter_settings' => array(
        'ft' => array(
          'func' => 'theme_field',
          'classes' => 'type',
        ),
      ),
    ),
    'og_group_ref' => array(
      'formatter_settings' => array(
        'ft' => array(
          'lb' => 'Communities of practice',
        ),
      ),
    ),
  );
  $export['node|post|teaser'] = $ds_fieldsetting;

  return $export;
}

/**
 * Implements hook_ds_layout_settings_info().
 */
function uwc_posts_ds_layout_settings_info() {
  $export = array();

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'comment|comment_node_post|default';
  $ds_layout->entity_type = 'comment';
  $ds_layout->bundle = 'comment_node_post';
  $ds_layout->view_mode = 'default';
  $ds_layout->layout = 'ds_2col';
  $ds_layout->settings = array(
    'regions' => array(
      'left' => array(
        0 => 'profile2',
      ),
      'right' => array(
        1 => 'links',
        2 => 'post_date',
        3 => 'comment_body',
        4 => 'field_file_attachment',
        5 => 'flag_inappropriate_comment',
        6 => 'rate_integration_1',
      ),
    ),
    'fields' => array(
      'profile2' => 'left',
      'links' => 'right',
      'post_date' => 'right',
      'comment_body' => 'right',
      'field_file_attachment' => 'right',
      'flag_inappropriate_comment' => 'right',
      'rate_integration_1' => 'right',
    ),
    'classes' => array(
      'layout_class' => array(
        '' => '',
      ),
      'left' => array(
        'teaser-left' => 'teaser-left',
      ),
      'right' => array(
        'teaser-right' => 'teaser-right',
      ),
    ),
    'wrappers' => array(
      'left' => 'div',
      'right' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
  );
  $export['comment|comment_node_post|default'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|post|compact_teaser';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'post';
  $ds_layout->view_mode = 'compact_teaser';
  $ds_layout->layout = 'ds_1col';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'post_date',
        1 => 'author',
        2 => 'title_field',
        3 => 'field_discussion_type',
        4 => 'private_group_flag',
      ),
    ),
    'fields' => array(
      'post_date' => 'ds_content',
      'author' => 'ds_content',
      'title_field' => 'ds_content',
      'field_discussion_type' => 'ds_content',
      'private_group_flag' => 'ds_content',
    ),
    'classes' => array(
      'layout_class' => array(
        '' => '',
      ),
    ),
    'wrappers' => array(
      'ds_content' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
  );
  $export['node|post|compact_teaser'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|post|default';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'post';
  $ds_layout->view_mode = 'default';
  $ds_layout->layout = 'ds_1col';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'field_discussion_type',
        1 => 'post_date',
        2 => 'author',
        3 => 'title_field',
        4 => 'og_group_ref',
        5 => 'field_topics',
        6 => 'body',
        7 => 'field_file_attachment',
        8 => 'rate_integration_1',
        9 => 'links',
        10 => 'comments',
      ),
    ),
    'fields' => array(
      'field_discussion_type' => 'ds_content',
      'post_date' => 'ds_content',
      'author' => 'ds_content',
      'title_field' => 'ds_content',
      'og_group_ref' => 'ds_content',
      'field_topics' => 'ds_content',
      'body' => 'ds_content',
      'field_file_attachment' => 'ds_content',
      'rate_integration_1' => 'ds_content',
      'links' => 'ds_content',
      'comments' => 'ds_content',
    ),
    'classes' => array(
      'layout_class' => array(
        '' => '',
      ),
    ),
    'wrappers' => array(
      'ds_content' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
  );
  $export['node|post|default'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|post|full';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'post';
  $ds_layout->view_mode = 'full';
  $ds_layout->layout = 'ds_1col';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'field_discussion_type',
        1 => 'private_group_flag',
        2 => 'post_date',
        3 => 'author',
        4 => 'title_field',
        5 => 'og_group_ref',
        6 => 'field_topics',
        7 => 'rate_integration_1',
        8 => 'flag_commons_follow_node',
        9 => 'flag_breakfast_recommendation',
        10 => 'group_node_tax',
        11 => 'body',
        12 => 'field_file_attachment',
        13 => 'comments',
      ),
    ),
    'fields' => array(
      'field_discussion_type' => 'ds_content',
      'private_group_flag' => 'ds_content',
      'post_date' => 'ds_content',
      'author' => 'ds_content',
      'title_field' => 'ds_content',
      'og_group_ref' => 'ds_content',
      'field_topics' => 'ds_content',
      'rate_integration_1' => 'ds_content',
      'flag_commons_follow_node' => 'ds_content',
      'flag_breakfast_recommendation' => 'ds_content',
      'group_node_tax' => 'ds_content',
      'body' => 'ds_content',
      'field_file_attachment' => 'ds_content',
      'comments' => 'ds_content',
    ),
    'classes' => array(
      'layout_class' => array(
        '' => '',
      ),
    ),
    'wrappers' => array(
      'ds_content' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
  );
  $export['node|post|full'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|post|search_result';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'post';
  $ds_layout->view_mode = 'search_result';
  $ds_layout->layout = 'ds_1col';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'title_field',
        1 => 'field_discussion_type',
        2 => 'private_group_flag',
        3 => 'post_date',
        4 => 'author',
        5 => 'og_group_ref',
        6 => 'body',
        7 => 'field_topics',
        8 => 'rate_integration_1',
        9 => 'links',
      ),
    ),
    'fields' => array(
      'title_field' => 'ds_content',
      'field_discussion_type' => 'ds_content',
      'private_group_flag' => 'ds_content',
      'post_date' => 'ds_content',
      'author' => 'ds_content',
      'og_group_ref' => 'ds_content',
      'body' => 'ds_content',
      'field_topics' => 'ds_content',
      'rate_integration_1' => 'ds_content',
      'links' => 'ds_content',
    ),
    'classes' => array(
      'layout_class' => array(
        '' => '',
      ),
    ),
    'wrappers' => array(
      'ds_content' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
  );
  $export['node|post|search_result'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|post|teaser';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'post';
  $ds_layout->view_mode = 'teaser';
  $ds_layout->layout = 'ds_2col';
  $ds_layout->settings = array(
    'regions' => array(
      'left' => array(
        0 => 'profile2',
      ),
      'right' => array(
        1 => 'post_date',
        2 => 'field_discussion_type',
        3 => 'private_group_flag',
        4 => 'title_field',
        5 => 'author',
        6 => 'body',
        7 => 'og_group_ref',
        8 => 'field_topics',
        9 => 'rate_integration_1',
        10 => 'group_node_actions',
        11 => 'comment_count',
      ),
    ),
    'fields' => array(
      'profile2' => 'left',
      'post_date' => 'right',
      'field_discussion_type' => 'right',
      'private_group_flag' => 'right',
      'title_field' => 'right',
      'author' => 'right',
      'body' => 'right',
      'og_group_ref' => 'right',
      'field_topics' => 'right',
      'rate_integration_1' => 'right',
      'group_node_actions' => 'right',
      'comment_count' => 'right',
    ),
    'classes' => array(
      'layout_class' => array(
        '' => '',
      ),
      'left' => array(
        'teaser-left' => 'teaser-left',
      ),
      'right' => array(
        'teaser-right' => 'teaser-right',
      ),
    ),
    'wrappers' => array(
      'left' => 'div',
      'right' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
  );
  $export['node|post|teaser'] = $ds_layout;

  return $export;
}
