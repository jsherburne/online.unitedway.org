<?php
/**
 * @file
 * Code for the UW Connect: Posts feature.
 */

include_once 'uwc_posts.features.inc';

/**
 * Implements hook_commons_bw_group_widget().
 */
function uwc_posts_commons_bw_group_widget() {
  return array(
      'uwc_posts' => array(
          'title' => t('Discussions'),
          'type' => 'view',
          'vid' => 'commons_bw_posts',
          'display' => 'default',
          'weight' => -1,
          'bundle' => 'post',
      ),
  );
}

/**
 * Implements hook_commons_bw_create_all_widget().
 */
function uwc_posts_commons_bw_create_all_widget($group) {
  if (og_user_access('node', $group->nid, 'create post content')) {
    $link = l(t('Create a post'),'node/add/post',
        array('attributes' => array('class' => 'commons-posts-create'), 'query' => array('og_group_ref' => $group->nid))
    );
    return array(
        'uwc_posts' => array(
            'default' => TRUE,
            'link' => $link,
            'text' =>  t('Foster a topic through commenting'),
            '#weight' => -1,
        ),
    );
  }
}

/**
 * Implements hook_form_FROM_ID_alter().
 */
function uwc_posts_form_commons_bw_partial_node_form_alter(&$form, &$form_state) {
  if (empty($form['#entity']) || $form['#entity']->type != 'post') {
    return;
  }

  $language = $form['body']['#language'];
  $form['body'][$language][0]['#title_display'] = 'invisible';
  $form['body'][$language][0]['#required'] = TRUE;
  $form['body'][$language][0]['#placeholder'] = t("What's on your mind?");
  $form['body'][$language][0]['#resizable'] = FALSE;

  // Set fields as hideable so the forms can be compacted.
  $form['body']['#attributes']['class'][] = 'trigger-field';
  foreach (array('field_image', 'og_group_ref', 'choice_wrapper', 'actions') as $field) {
    if (isset($form[$field])) {
      $form[$field]['#attributes']['class'][] = 'hideable-field';
    }
  }

  $form['actions']['submit']['#value'] = t('Post');
  $form['#pre_render'][] = 'uwc_posts_form_commons_bw_partial_node_form_after_build';
}

/**
 * After-build call-back.
 * See uwc_posts_form_commons_bw_partial_node_form_alter().
 */
function uwc_posts_form_commons_bw_partial_node_form_after_build($form) {
  $language = $form['body']['#language'];
  $form['body'][$language][0]['#pre_render'] = array();
  $form['body'][$language][0]['format']['#access'] = FALSE;
  $form['body'][$language][0]['value']['#rows'] = 3;

  return $form;
}


/**
 * Implements commons_activity_streams_message_selection_alter().
 */
function uwc_posts_commons_activity_streams_message_selection_alter(&$message_type, $hook, $node) {
  // Use a "User posted [title]" format activity stream message.
  if ($hook == 'node_insert' && $node->type == 'post') {
    $message_type = 'commons_posts_post_created';
  }
}

/**
 * Implements hook_commons_entity_integration.
 */
function uwc_posts_commons_entity_integration() {
  return array(
      'node' => array(
          'post' => array(
          ),
      ),
  );
}

/**
 * Implements hook_strongarm_alter().
 */
function uwc_posts_strongarm_alter(&$items) {
  // Expose the Post content type for 'liking' via the Commons_like module
  // by altering the configuration for the Rate.module widget that it provides.
  if (!empty($items['rate_widgets']->value)) {
    foreach($items['rate_widgets']->value as $key => $widget) {
      if ($widget->name == 'commons_like') {
        if (!in_array('post', $items['rate_widgets']->value[$key]->node_types)) {
          $items['rate_widgets']->value[$key]->node_types[] = 'post';
        }
        if (!in_array('post', $items['rate_widgets']->value[$key]->comment_types)) {
          $items['rate_widgets']->value[$key]->comment_types[] = 'post';
        }
      }
    }
  }
  // Expose the post content type for integration with Commons Radioactivity
  // and Commons Groups.
  foreach (array('uwc_radioactivity_entity_types', 'uwc_groups_entity_types') as $key) {
    if (isset($items[$key])) {
      $items[$key]->value['node']['post'] = 1;
    }
  }
}

/**
 * Implements hook_views_default_views_alter().
 *
 * Display posts on the browsing widget main view.
 */
function uwc_posts_views_default_views_alter(&$views) {
  if (!empty($views['commons_bw_all'])) {
    $views['commons_bw_all']->display['default']->display_options['filters']['type']['value']['post'] = 'post';
  }
}


/**
 * Implements hook_entity_info_alter().
 */
function uwc_posts_entity_info_alter(&$entity_info) {
  $entity_info['node']['bundles']['post']['label'] = t('Discussion');
}
