<?php
/**
 * @file
 * uwc_pages.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function uwc_pages_taxonomy_default_vocabularies() {
  return array();
}
