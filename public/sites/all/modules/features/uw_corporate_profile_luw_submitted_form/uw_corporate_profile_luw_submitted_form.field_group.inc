<?php
/**
 * @file
 * uw_corporate_profile_luw_submitted_form.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function uw_corporate_profile_luw_submitted_form_field_group_info() {
  $export = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_campaign_details|entityform|company_profile_luw_provided|form';
  $field_group->group_name = 'group_campaign_details';
  $field_group->entity_type = 'entityform';
  $field_group->bundle = 'company_profile_luw_provided';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_profile';
  $field_group->data = array(
    'label' => 'Campaign Details',
    'weight' => '5',
    'children' => array(
      0 => 'field_campaign_theme',
      1 => 'field_campaign_materials_provide',
      2 => 'field_provide_local_materials_',
      3 => 'field_use_local_pledge_card_',
      4 => 'field_campaign_structure_luw',
    ),
    'format_type' => 'multipage',
    'format_settings' => array(
      'formatter' => 'no-start',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_campaign_details|entityform|company_profile_luw_provided|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_campaign_results|entityform|company_profile_luw_provided|form';
  $field_group->group_name = 'group_campaign_results';
  $field_group->entity_type = 'entityform';
  $field_group->bundle = 'company_profile_luw_provided';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_profile';
  $field_group->data = array(
    'label' => 'Campaign Results',
    'weight' => '4',
    'children' => array(
      0 => 'field_corporate_results_availabl',
      1 => 'field_employee_results_available',
      2 => 'field_results_provided_by',
      3 => 'field_how_and_from_where_will_yo',
      4 => 'field_reports_available_luw',
    ),
    'format_type' => 'multipage',
    'format_settings' => array(
      'formatter' => 'no-start',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_campaign_results|entityform|company_profile_luw_provided|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_company_information|entityform|company_profile_luw_provided|form';
  $field_group->group_name = 'group_company_information';
  $field_group->entity_type = 'entityform';
  $field_group->bundle = 'company_profile_luw_provided';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_profile';
  $field_group->data = array(
    'label' => 'Company Information',
    'weight' => '2',
    'children' => array(
      0 => 'title_field',
      1 => 'field_contact_name',
      2 => 'field_contact_email',
      3 => 'field_phone',
      4 => 'field_campaign_start_date',
      5 => 'field_campaign_end_date',
    ),
    'format_type' => 'multipage',
    'format_settings' => array(
      'formatter' => 'no-start',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_company_information|entityform|company_profile_luw_provided|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_corporate_information|entityform|company_profile_luw_provided|form';
  $field_group->group_name = 'group_corporate_information';
  $field_group->entity_type = 'entityform';
  $field_group->bundle = 'company_profile_luw_provided';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_profile';
  $field_group->data = array(
    'label' => 'Corporate Information',
    'weight' => '8',
    'children' => array(
      0 => 'field_company_overview',
      1 => 'field_employee_giving',
      2 => 'field_company_giving',
      3 => 'field_leadership_giving',
      4 => 'field_retiree_program',
      5 => 'field_additional_information',
    ),
    'format_type' => 'multipage',
    'format_settings' => array(
      'formatter' => 'no-start',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_corporate_information|entityform|company_profile_luw_provided|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_finance_information|entityform|company_profile_luw_provided|form';
  $field_group->group_name = 'group_finance_information';
  $field_group->entity_type = 'entityform';
  $field_group->bundle = 'company_profile_luw_provided';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_profile';
  $field_group->data = array(
    'label' => 'Finance Information',
    'weight' => '7',
    'children' => array(
      0 => 'field_corporate_gift_reduced_for',
      1 => 'field_corp_gift_determined_luw',
      2 => 'field_does_match_gift_follow_luw',
      3 => 'field_corporate_gift_match_rate_',
      4 => 'field_corporate_gift_match_cap_p',
      5 => 'field_corporate_gift_comments',
      6 => 'field_when_to_expect_first_emplo',
      7 => 'field_payment_processing_details',
      8 => 'field_employee_payment_paid_luw',
      9 => 'field_corporate_payment_paid_luw',
      10 => 'field_united_way_responsible_luw',
      11 => 'field_payment_frequency_luw',
      12 => 'field_corporate_gift_',
    ),
    'format_type' => 'multipage',
    'format_settings' => array(
      'formatter' => 'no-start',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_finance_information|entityform|company_profile_luw_provided|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_pledges|entityform|company_profile_luw_provided|form';
  $field_group->group_name = 'group_pledges';
  $field_group->entity_type = 'entityform';
  $field_group->bundle = 'company_profile_luw_provided';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_profile';
  $field_group->data = array(
    'label' => 'Pledges',
    'weight' => '6',
    'children' => array(
      0 => 'field_employee_gift_reduced_for_',
      1 => 'field_pledge_method_luw',
      2 => 'field_pledge_capture_by_luw',
      3 => 'field_united_way_impact_areasluw',
      4 => 'field_giving_methods_luw',
      5 => 'field_employee_pledges_tied_luw',
    ),
    'format_type' => 'multipage',
    'format_settings' => array(
      'formatter' => 'no-start',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_pledges|entityform|company_profile_luw_provided|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_profile|entityform|company_profile_luw_provided|form';
  $field_group->group_name = 'group_profile';
  $field_group->entity_type = 'entityform';
  $field_group->bundle = 'company_profile_luw_provided';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Profile',
    'weight' => '1',
    'children' => array(
      0 => 'group_company_information',
      1 => 'group_relationship_building',
      2 => 'group_campaign_results',
      3 => 'group_campaign_details',
      4 => 'group_pledges',
      5 => 'group_finance_information',
      6 => 'group_corporate_information',
    ),
    'format_type' => 'multipage-group',
    'format_settings' => array(
      'label' => 'Profile',
      'instance_settings' => array(
        'classes' => 'group-profile field-group-multipage-group',
        'page_header' => '0',
        'page_counter' => '1',
        'move_button' => '0',
        'move_additional' => '1',
      ),
    ),
  );
  $export['group_profile|entityform|company_profile_luw_provided|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_relationship_building|entityform|company_profile_luw_provided|form';
  $field_group->group_name = 'group_relationship_building';
  $field_group->entity_type = 'entityform';
  $field_group->bundle = 'company_profile_luw_provided';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_profile';
  $field_group->data = array(
    'label' => 'Relationship Building',
    'weight' => '3',
    'children' => array(
      0 => 'field_provide_list_of_local_comp',
      1 => 'field_provide_donor_contact_luw',
      2 => 'field_loyal_contributors_tracked',
      3 => 'field_donor_e_mail_opt_in_luw',
    ),
    'format_type' => 'multipage',
    'format_settings' => array(
      'formatter' => 'no-start',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_relationship_building|entityform|company_profile_luw_provided|form'] = $field_group;

  return $export;
}
