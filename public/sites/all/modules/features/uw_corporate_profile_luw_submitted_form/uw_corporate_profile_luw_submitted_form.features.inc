<?php
/**
 * @file
 * uw_corporate_profile_luw_submitted_form.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function uw_corporate_profile_luw_submitted_form_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_default_entityform_type().
 */
function uw_corporate_profile_luw_submitted_form_default_entityform_type() {
  $items = array();
  $items['company_profile_luw_provided'] = entity_import('entityform_type', '{
    "type" : "company_profile_luw_provided",
    "label" : "Company Profile -LUW Provided",
    "data" : {
      "draftable" : 0,
      "draft_redirect_path" : "",
      "draft_button_text" : "",
      "draft_save_text" : { "value" : "", "format" : "filtered_html" },
      "submit_button_text" : "",
      "submit_confirm_msg" : "",
      "your_submissions" : "",
      "disallow_resubmit_msg" : "",
      "delete_confirm_msg" : "",
      "page_title_view" : "",
      "preview_page" : 1,
      "submission_page_title" : "Thank you",
      "submission_text" : {
        "value" : "Thank you for submitting your company information. A memeber of the corporate relations team will review your submission and contact you.\\u003Cbr \\/\\u003E\\r\\nIf you would like review and edit your information you may do so here.\\u0026nbsp;[entityform:url]\\u003Cbr \\/\\u003E\\r\\nIf you would like to add a new company you may do so here. \\u003Ca href=\\u0022http:\\/\\/ojjohnson.dev.online.unitedway.org\\/eform\\/submit\\/company-profile-luw-provided\\u0022\\u003Ehttps:\\/\\/ojjohnson.dev.online.unitedway.org\\/eform\\/submit\\/company-profile-luw-provided\\u003C\\/a\\u003E\\u003Cbr \\/\\u003E\\r\\nIf you have any questions, feel free to e-mail\\u003Ca href=\\u0022mailto:gclinfo@uww.unitedway.org?subject=Question%20about%20Corporate%20Profile%20Form\\u0022\\u003E gclinfo@uww.unitedway.org\\u003C\\/a\\u003E.",
        "format" : "filtered_html"
      },
      "submission_show_submitted" : 1,
      "submissions_view" : "default",
      "user_submissions_view" : "default",
      "form_status" : "ENTITYFORM_OPEN",
      "roles" : {
        "2" : "2",
        "1" : 0,
        "3" : 0,
        "4" : 0,
        "6" : 0,
        "5" : 0,
        "7" : 0,
        "10" : 0,
        "8" : 0,
        "9" : 0,
        "11" : 0
      },
      "resubmit_action" : "new",
      "redirect_path" : "",
      "instruction_pre" : {
        "value" : "\\u003Ch1\\u003ECompany Profile Form\\u003C\\/h1\\u003E\\r\\n",
        "format" : "filtered_html"
      }
    },
    "weight" : "0",
    "rdf_mapping" : [],
    "paths" : []
  }');
  return $items;
}
