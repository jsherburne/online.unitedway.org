<?php
/**
 * @file
 * uw_corporate_profile_luw_submitted_form.features.field_base.inc
 */

/**
 * Implements hook_field_default_field_bases().
 */
function uw_corporate_profile_luw_submitted_form_field_default_field_bases() {
  $field_bases = array();

  // Exported field_base: 'field_additional_information'
  $field_bases['field_additional_information'] = array(
    'active' => 1,
    'cardinality' => -1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_additional_information',
    'field_permissions' => array(
      'type' => 0,
    ),
    'foreign keys' => array(
      'fid' => array(
        'columns' => array(
          'fid' => 'fid',
        ),
        'table' => 'file_managed',
      ),
    ),
    'indexes' => array(
      'fid' => array(
        0 => 'fid',
      ),
    ),
    'locked' => 0,
    'module' => 'file',
    'settings' => array(
      'display_default' => 0,
      'display_field' => 0,
      'profile2_private' => FALSE,
      'uri_scheme' => 'public',
    ),
    'translatable' => 0,
    'type' => 'file',
  );

  // Exported field_base: 'field_campaign_structure_luw'
  $field_bases['field_campaign_structure_luw'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_campaign_structure_luw',
    'field_permissions' => array(
      'type' => 0,
    ),
    'foreign keys' => array(),
    'indexes' => array(
      'value' => array(
        0 => 'value',
      ),
    ),
    'locked' => 0,
    'module' => 'list',
    'settings' => array(
      'allowed_values' => array(
        'UW w/ No More than 3 Agencies' => 'UW w/ No More than 3 Agencies',
        'United Way Branded -- UW Only' => 'United Way Branded -- UW Only',
        'United Way Branded -- UW & Member Agencies' => 'United Way Branded -- UW & Member Agencies',
        'United Way Branded -- UW Health & Human Services' => 'United Way Branded -- UW Health & Human Services',
        'United Way Branded -- UW & any 501-c-3' => 'United Way Branded -- UW & any 501-c-3',
        'Company Branded with a UW as partner' => 'Company Branded with a UW as partner',
        'Company Branded with Sided by Side Campaign' => 'Company Branded with Sided by Side Campaign',
        'Company Branded with other Federations' => 'Company Branded with other Federations',
        'Company Branded with Open Choice -- No UW Influence' => 'Company Branded with Open Choice -- No UW Influence',
        'Company branded with open choice' => 'Company branded with open choice',
      ),
      'allowed_values_function' => '',
      'profile2_private' => FALSE,
      'values_field' => '',
    ),
    'translatable' => 0,
    'type' => 'list_text',
  );

  // Exported field_base: 'field_close_date'
  $field_bases['field_close_date'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_close_date',
    'field_permissions' => array(
      'type' => 2,
    ),
    'foreign keys' => array(),
    'indexes' => array(),
    'locked' => 0,
    'module' => 'date',
    'settings' => array(
      'cache_count' => 4,
      'cache_enabled' => 0,
      'granularity' => array(
        'day' => 'day',
        'hour' => 0,
        'minute' => 0,
        'month' => 'month',
        'second' => 0,
        'year' => 'year',
      ),
      'profile2_private' => FALSE,
      'timezone_db' => '',
      'todate' => '',
      'tz_handling' => 'none',
    ),
    'translatable' => 0,
    'type' => 'date',
  );

  // Exported field_base: 'field_contact_email'
  $field_bases['field_contact_email'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_contact_email',
    'field_permissions' => array(
      'type' => 0,
    ),
    'foreign keys' => array(
      'format' => array(
        'columns' => array(
          'format' => 'format',
        ),
        'table' => 'filter_format',
      ),
    ),
    'indexes' => array(
      'format' => array(
        0 => 'format',
      ),
    ),
    'locked' => 0,
    'module' => 'text',
    'settings' => array(
      'max_length' => 255,
      'profile2_private' => FALSE,
    ),
    'translatable' => 0,
    'type' => 'text',
  );

  // Exported field_base: 'field_contact_name'
  $field_bases['field_contact_name'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_contact_name',
    'field_permissions' => array(
      'type' => 0,
    ),
    'foreign keys' => array(
      'format' => array(
        'columns' => array(
          'format' => 'format',
        ),
        'table' => 'filter_format',
      ),
    ),
    'indexes' => array(
      'format' => array(
        0 => 'format',
      ),
    ),
    'locked' => 0,
    'module' => 'text',
    'settings' => array(
      'max_length' => 255,
      'profile2_private' => FALSE,
    ),
    'translatable' => 0,
    'type' => 'text',
  );

  // Exported field_base: 'field_corp_gift_determined_luw'
  $field_bases['field_corp_gift_determined_luw'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_corp_gift_determined_luw',
    'field_permissions' => array(
      'type' => 0,
    ),
    'foreign keys' => array(),
    'indexes' => array(
      'value' => array(
        0 => 'value',
      ),
    ),
    'locked' => 0,
    'module' => 'list',
    'settings' => array(
      'allowed_values' => array(
        'Company Requests Funding' => 'Company Requests Funding',
        'LUW Request Funding' => 'LUW Request Funding',
        'Match' => 'Match',
        'Per Capita' => 'Per Capita',
        'Predetermined Set Amount' => 'Predetermined Set Amount',
        'Varies By Location' => 'Varies By Location',
        'Other' => 'Other',
      ),
      'allowed_values_function' => '',
      'profile2_private' => FALSE,
      'values_field' => '',
    ),
    'translatable' => 0,
    'type' => 'list_text',
  );

  // Exported field_base: 'field_corporate_payment_paid_luw'
  $field_bases['field_corporate_payment_paid_luw'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_corporate_payment_paid_luw',
    'field_permissions' => array(
      'type' => 0,
    ),
    'foreign keys' => array(),
    'indexes' => array(
      'value' => array(
        0 => 'value',
      ),
    ),
    'locked' => 0,
    'module' => 'list',
    'settings' => array(
      'allowed_values' => array(
        'America\'s Charities' => 'America\'s Charities',
        'Benevity' => 'Benevity',
        'Company' => 'Company',
        'Cybergrants' => 'Cybergrants',
        'DonationsXChange' => 'DonationsXChange',
        'DoTopia' => 'DoTopia',
        'Frontstream/Truist' => 'Frontstream/Truist',
        'Good Done Great' => 'Good Done Great',
        'JK Group' => 'JK Group',
        'Kitera' => 'Kitera',
        'Local United Way' => 'Local United Way',
        'Network for Good' => 'Network for Good',
        'Other' => 'Other',
        'Rapid Pledge' => 'Rapid Pledge',
        'UPIC' => 'UPIC',
        'UWONE' => 'UWONE',
        'UW Tristate' => 'UW Tristate',
        'Your Cause' => 'Your Cause',
      ),
      'allowed_values_function' => '',
      'profile2_private' => FALSE,
      'values_field' => '',
    ),
    'translatable' => 0,
    'type' => 'list_text',
  );

  // Exported field_base: 'field_does_match_gift_follow_luw'
  $field_bases['field_does_match_gift_follow_luw'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_does_match_gift_follow_luw',
    'field_permissions' => array(
      'type' => 0,
    ),
    'foreign keys' => array(),
    'indexes' => array(
      'value' => array(
        0 => 'value',
      ),
    ),
    'locked' => 0,
    'module' => 'list',
    'settings' => array(
      'allowed_values' => array(
        'Yes' => 'Yes',
        'No' => 'No',
        'Other' => 'Other',
      ),
      'allowed_values_function' => '',
      'profile2_private' => FALSE,
      'values_field' => '',
    ),
    'translatable' => 0,
    'type' => 'list_text',
  );

  // Exported field_base: 'field_donor_e_mail_opt_in_luw'
  $field_bases['field_donor_e_mail_opt_in_luw'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_donor_e_mail_opt_in_luw',
    'field_permissions' => array(
      'type' => 0,
    ),
    'foreign keys' => array(),
    'indexes' => array(
      'value' => array(
        0 => 'value',
      ),
    ),
    'locked' => 0,
    'module' => 'list',
    'settings' => array(
      'allowed_values' => array(
        'All' => 'All',
        'Most' => 'Most',
        'Some' => 'Some',
        'HQ only' => 'HQ only',
        'None' => 'None',
        'Home email only' => 'Home email only',
        'Work email only' => 'Work email only',
        'Work and home' => 'Work and home',
        'Not provided' => 'Not provided',
      ),
      'allowed_values_function' => '',
      'profile2_private' => FALSE,
      'values_field' => '',
    ),
    'translatable' => 0,
    'type' => 'list_text',
  );

  // Exported field_base: 'field_employee_payment_paid_luw'
  $field_bases['field_employee_payment_paid_luw'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_employee_payment_paid_luw',
    'field_permissions' => array(
      'type' => 0,
    ),
    'foreign keys' => array(),
    'indexes' => array(
      'value' => array(
        0 => 'value',
      ),
    ),
    'locked' => 0,
    'module' => 'list',
    'settings' => array(
      'allowed_values' => array(
        'America\'s Charities' => 'America\'s Charities',
        'Benevity' => 'Benevity',
        'Company' => 'Company',
        'Cybergrants' => 'Cybergrants',
        'DonationsXChange' => 'DonationsXChange',
        'DoTopia' => 'DoTopia',
        'Frontstream/Truist' => 'Frontstream/Truist',
        'Good Done Great' => 'Good Done Great',
        'JK Group' => 'JK Group',
        'Kitera' => 'Kitera',
        'Local United Way' => 'Local United Way',
        'Network for Good' => 'Network for Good',
        'Other' => 'Other',
        'Rapid Pledge' => 'Rapid Pledge',
        'UPIC' => 'UPIC',
        'UWONE' => 'UWONE',
        'UW Tristate' => 'UW Tristate',
        'Your Cause' => 'Your Cause',
      ),
      'allowed_values_function' => '',
      'profile2_private' => FALSE,
      'values_field' => '',
    ),
    'translatable' => 0,
    'type' => 'list_text',
  );

  // Exported field_base: 'field_employee_pledges_tied_luw'
  $field_bases['field_employee_pledges_tied_luw'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_employee_pledges_tied_luw',
    'field_permissions' => array(
      'type' => 0,
    ),
    'foreign keys' => array(),
    'indexes' => array(
      'value' => array(
        0 => 'value',
      ),
    ),
    'locked' => 0,
    'module' => 'list',
    'settings' => array(
      'allowed_values' => array(
        'Unknown' => 'Unknown',
        'Employee Home ZIP' => 'Employee Home ZIP',
        'Employee Work ZIP' => 'Employee Work ZIP',
        'None' => 'None',
        'Employee Choice' => 'Employee Choice',
      ),
      'allowed_values_function' => '',
      'profile2_private' => FALSE,
      'values_field' => '',
    ),
    'translatable' => 0,
    'type' => 'list_text',
  );

  // Exported field_base: 'field_giving_methods_luw'
  $field_bases['field_giving_methods_luw'] = array(
    'active' => 1,
    'cardinality' => -1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_giving_methods_luw',
    'field_permissions' => array(
      'type' => 0,
    ),
    'foreign keys' => array(),
    'indexes' => array(
      'value' => array(
        0 => 'value',
      ),
    ),
    'locked' => 0,
    'module' => 'list',
    'settings' => array(
      'allowed_values' => array(
        'Cash' => 'Cash',
        'Check' => 'Check',
        'Continuous Giving' => 'Continuous Giving',
        'Credit Card' => 'Credit Card',
        'Debit Card' => 'Debit Card',
        'Direct Bill' => 'Direct Bill',
        'Payroll Deduction' => 'Payroll Deduction',
        'Stock' => 'Stock',
      ),
      'allowed_values_function' => '',
      'profile2_private' => FALSE,
      'values_field' => '',
    ),
    'translatable' => 0,
    'type' => 'list_text',
  );

  // Exported field_base: 'field_payment_frequency_luw'
  $field_bases['field_payment_frequency_luw'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_payment_frequency_luw',
    'field_permissions' => array(
      'type' => 0,
    ),
    'foreign keys' => array(),
    'indexes' => array(
      'value' => array(
        0 => 'value',
      ),
    ),
    'locked' => 0,
    'module' => 'list',
    'settings' => array(
      'allowed_values' => array(
        'Yes' => 'Yes',
        'No' => 'No',
        'See Giving Section for Details' => 'See Giving Section for Details',
      ),
      'allowed_values_function' => '',
      'profile2_private' => FALSE,
      'values_field' => '',
    ),
    'translatable' => 0,
    'type' => 'list_text',
  );

  // Exported field_base: 'field_pledge_capture_by_luw'
  $field_bases['field_pledge_capture_by_luw'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_pledge_capture_by_luw',
    'field_permissions' => array(
      'type' => 0,
    ),
    'foreign keys' => array(),
    'indexes' => array(
      'value' => array(
        0 => 'value',
      ),
    ),
    'locked' => 0,
    'module' => 'list',
    'settings' => array(
      'allowed_values' => array(
        'America\'s Charities' => 'America\'s Charities',
        'Andar' => 'Andar',
        'Benevity' => 'Benevity',
        'Company' => 'Company',
        'Create Hope' => 'Create Hope',
        'Cybergrants' => 'Cybergrants',
        'DonationXChange' => 'DonationXChange',
        'DoTopia' => 'DoTopia',
        'Frontstream/Truist' => 'Frontstream/Truist',
        'Frontstream/Truist via Local United Way' => 'Frontstream/Truist via Local United Way',
        'Good Done Great' => 'Good Done Great',
        'JK Group' => 'JK Group',
        'KInd Mark' => 'KInd Mark',
        'Kintera' => 'Kintera',
        'Other' => 'Other',
        'Profit for Purpose' => 'Profit for Purpose',
        'Rapid Pledge' => 'Rapid Pledge',
        'Your Cause' => 'Your Cause',
      ),
      'allowed_values_function' => '',
      'profile2_private' => FALSE,
      'values_field' => '',
    ),
    'translatable' => 0,
    'type' => 'list_text',
  );

  // Exported field_base: 'field_pledge_method_luw'
  $field_bases['field_pledge_method_luw'] = array(
    'active' => 1,
    'cardinality' => -1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_pledge_method_luw',
    'field_permissions' => array(
      'type' => 0,
    ),
    'foreign keys' => array(),
    'indexes' => array(
      'value' => array(
        0 => 'value',
      ),
    ),
    'locked' => 0,
    'module' => 'list',
    'settings' => array(
      'allowed_values' => array(
        'ePledging' => 'ePledging',
        'Other' => 'Other',
        'Paper' => 'Paper',
        'Scan Pledging' => 'Scan Pledging',
        'Telephone' => 'Telephone',
      ),
      'allowed_values_function' => '',
      'profile2_private' => FALSE,
      'values_field' => '',
    ),
    'translatable' => 0,
    'type' => 'list_text',
  );

  // Exported field_base: 'field_provide_donor_contact_luw'
  $field_bases['field_provide_donor_contact_luw'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_provide_donor_contact_luw',
    'field_permissions' => array(
      'type' => 0,
    ),
    'foreign keys' => array(),
    'indexes' => array(
      'value' => array(
        0 => 'value',
      ),
    ),
    'locked' => 0,
    'module' => 'list',
    'settings' => array(
      'allowed_values' => array(
        'Email' => 'Email',
        'Mailing Address' => 'Mailing Address',
        'Name' => 'Name',
      ),
      'allowed_values_function' => '',
      'profile2_private' => FALSE,
      'values_field' => '',
    ),
    'translatable' => 0,
    'type' => 'list_text',
  );

  // Exported field_base: 'field_reports_available_luw'
  $field_bases['field_reports_available_luw'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_reports_available_luw',
    'field_permissions' => array(
      'type' => 0,
    ),
    'foreign keys' => array(),
    'indexes' => array(
      'value' => array(
        0 => 'value',
      ),
    ),
    'locked' => 0,
    'module' => 'list',
    'settings' => array(
      'allowed_values' => array(
        'Amount you will receive' => 'Amount you will receive',
        'Limited' => 'Limited',
        'Other' => 'Other',
        'Summary' => 'Summary',
        'TBD' => 'TBD',
        'Where raised' => 'Where raised',
      ),
      'allowed_values_function' => '',
      'profile2_private' => FALSE,
      'values_field' => '',
    ),
    'translatable' => 0,
    'type' => 'list_text',
  );

  // Exported field_base: 'field_united_way_impact_areasluw'
  $field_bases['field_united_way_impact_areasluw'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_united_way_impact_areasluw',
    'field_permissions' => array(
      'type' => 0,
    ),
    'foreign keys' => array(),
    'indexes' => array(
      'value' => array(
        0 => 'value',
      ),
    ),
    'locked' => 0,
    'module' => 'list',
    'settings' => array(
      'allowed_values' => array(
        'No' => 'No',
        'Yes, National Impact Areas' => 'Yes, National Impact Areas',
        'Yes, Local Impact Areas' => 'Yes, Local Impact Areas',
        'Yes, National and Local Impact Areas' => 'Yes, National and Local Impact Areas',
        'Unknown' => 'Unknown',
      ),
      'allowed_values_function' => '',
      'profile2_private' => FALSE,
      'values_field' => '',
    ),
    'translatable' => 0,
    'type' => 'list_text',
  );

  // Exported field_base: 'field_united_way_responsible_luw'
  $field_bases['field_united_way_responsible_luw'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_united_way_responsible_luw',
    'field_permissions' => array(
      'type' => 0,
    ),
    'foreign keys' => array(),
    'indexes' => array(
      'value' => array(
        0 => 'value',
      ),
    ),
    'locked' => 0,
    'module' => 'list',
    'settings' => array(
      'allowed_values' => array(
        'Yes' => 'Yes',
        'No' => 'No',
        'See Giving Section for Details' => 'See Giving Section for Details',
      ),
      'allowed_values_function' => '',
      'profile2_private' => FALSE,
      'values_field' => '',
    ),
    'translatable' => 0,
    'type' => 'list_text',
  );

  return $field_bases;
}
