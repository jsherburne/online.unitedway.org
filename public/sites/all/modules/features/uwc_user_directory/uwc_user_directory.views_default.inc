<?php
/**
 * @file
 * uwc_user_directory.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function uwc_user_directory_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'uwc_user_profile_directory';
  $view->description = 'A directory listing of all user profiles in the system';
  $view->tag = 'uwc, directory, users';
  $view->base_table = 'search_api_index_user_profile_index';
  $view->human_name = 'UWC: User Profile Directory';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'User Directory';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'none';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['row_plugin'] = 'entity';
  $handler->display->display_options['row_options']['view_mode'] = 'search_result';
  /* No results behavior: Global: Text area */
  $handler->display->display_options['empty']['area']['id'] = 'area';
  $handler->display->display_options['empty']['area']['table'] = 'views';
  $handler->display->display_options['empty']['area']['field'] = 'area';
  $handler->display->display_options['empty']['area']['label'] = 'No Results';
  $handler->display->display_options['empty']['area']['empty'] = TRUE;
  $handler->display->display_options['empty']['area']['content'] = 'No users were found for these search criteria.';
  $handler->display->display_options['empty']['area']['format'] = 'filtered_html';
  /* Field: Indexed Profile: Profile ID */
  $handler->display->display_options['fields']['pid']['id'] = 'pid';
  $handler->display->display_options['fields']['pid']['table'] = 'search_api_index_user_profile_index';
  $handler->display->display_options['fields']['pid']['field'] = 'pid';
  /* Contextual filter: User » Group membership: Node ID (indexed) */
  $handler->display->display_options['arguments']['user_og_user_node_nid']['id'] = 'user_og_user_node_nid';
  $handler->display->display_options['arguments']['user_og_user_node_nid']['table'] = 'search_api_index_user_profile_index';
  $handler->display->display_options['arguments']['user_og_user_node_nid']['field'] = 'user_og_user_node_nid';
  $handler->display->display_options['arguments']['user_og_user_node_nid']['default_argument_type'] = 'fixed';
  $handler->display->display_options['arguments']['user_og_user_node_nid']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['user_og_user_node_nid']['break_phrase'] = 0;
  $handler->display->display_options['arguments']['user_og_user_node_nid']['not'] = 0;
  /* Filter criterion: Search: Fulltext search */
  $handler->display->display_options['filters']['search_api_views_fulltext']['id'] = 'search_api_views_fulltext';
  $handler->display->display_options['filters']['search_api_views_fulltext']['table'] = 'search_api_index_user_profile_index';
  $handler->display->display_options['filters']['search_api_views_fulltext']['field'] = 'search_api_views_fulltext';
  $handler->display->display_options['filters']['search_api_views_fulltext']['exposed'] = TRUE;
  $handler->display->display_options['filters']['search_api_views_fulltext']['expose']['operator_id'] = 'search_api_views_fulltext_op';
  $handler->display->display_options['filters']['search_api_views_fulltext']['expose']['label'] = 'Keyword search';
  $handler->display->display_options['filters']['search_api_views_fulltext']['expose']['operator'] = 'search_api_views_fulltext_op';
  $handler->display->display_options['filters']['search_api_views_fulltext']['expose']['identifier'] = 'user_search';
  $handler->display->display_options['filters']['search_api_views_fulltext']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
    4 => 0,
    6 => 0,
    5 => 0,
    7 => 0,
    10 => 0,
    8 => 0,
    9 => 0,
    11 => 0,
  );
  $handler->display->display_options['filters']['search_api_views_fulltext']['fields'] = array(
    'field_current_position' => 'field_current_position',
    'field_personal_interests' => 'field_personal_interests',
    'search_api_aggregation_1' => 'search_api_aggregation_1',
    'user:mail' => 'user:mail',
    'field_bio:value' => 'field_bio:value',
  );

  /* Display: User Directory */
  $handler = $view->new_display('panel_pane', 'User Directory', 'user_directory_panel');
  $handler->display->display_options['exposed_block'] = TRUE;
  $handler->display->display_options['argument_input'] = array(
    'user_og_user_node_nid' => array(
      'type' => 'context',
      'context' => 'entity:node.nid',
      'context_optional' => 1,
      'panel' => '0',
      'fixed' => '',
      'label' => 'User » Group membership: Node ID (indexed)',
    ),
  );
  $handler->display->display_options['inherit_panels_path'] = '1';
  $export['uwc_user_profile_directory'] = $view;

  return $export;
}
