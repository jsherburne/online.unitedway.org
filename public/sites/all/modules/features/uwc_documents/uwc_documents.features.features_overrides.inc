<?php
/**
 * @file
 * uwc_documents.features.features_overrides.inc
 */

/**
 * Implements hook_features_override_default_overrides().
 */
function uwc_documents_features_override_default_overrides() {
  // This code is only used for UI in features. Exported alters hooks do the magic.
  $overrides = array();

  // Exported overrides for: field_base

  // Exported overrides for: field_instance

  // Exported overrides for: variable

  // Exported overrides for: views_view

 return $overrides;
}
