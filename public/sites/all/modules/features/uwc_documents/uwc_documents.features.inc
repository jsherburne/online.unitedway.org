<?php
/**
 * @file
 * uwc_documents.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function uwc_documents_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "ds" && $api == "ds") {
    return array("version" => "1");
  }
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function uwc_documents_node_info() {
  $items = array(
    'document' => array(
      'name' => t('Resource'),
      'base' => 'node_content',
      'description' => t('Upload and display files or attachments to share with others.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}

/**
 * Implements hook_views_api().
 */
function uwc_documents_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}