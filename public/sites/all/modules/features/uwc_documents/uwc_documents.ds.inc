<?php
/**
 * @file
 * uwc_documents.ds.inc
 */

/**
 * Implements hook_ds_field_settings_info().
 */
function uwc_documents_ds_field_settings_info() {
  $export = array();

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'comment|comment_node_document|default';
  $ds_fieldsetting->entity_type = 'comment';
  $ds_fieldsetting->bundle = 'comment_node_document';
  $ds_fieldsetting->view_mode = 'default';
  $ds_fieldsetting->settings = array(
    'links' => array(
      'weight' => '1',
      'label' => 'hidden',
      'format' => 'default',
    ),
    'post_date' => array(
      'weight' => '2',
      'label' => 'hidden',
      'format' => 'ds_post_date_time_ago_dynamic',
    ),
    'flag_inappropriate_comment' => array(
      'weight' => '4',
      'label' => 'hidden',
      'format' => 'default',
    ),
    'profile2' => array(
      'weight' => '0',
      'label' => 'hidden',
      'format' => 'compact_teaser',
    ),
    'rate_integration_1' => array(
      'weight' => '5',
      'label' => 'hidden',
      'format' => 'default',
    ),
  );
  $export['comment|comment_node_document|default'] = $ds_fieldsetting;

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'node|document|compact_teaser';
  $ds_fieldsetting->entity_type = 'node';
  $ds_fieldsetting->bundle = 'document';
  $ds_fieldsetting->view_mode = 'compact_teaser';
  $ds_fieldsetting->settings = array(
    'private_group_flag' => array(
      'weight' => '2',
      'label' => 'hidden',
      'format' => 'default',
    ),
    'author' => array(
      'weight' => '5',
      'label' => 'hidden',
      'format' => 'author',
    ),
    'post_date' => array(
      'weight' => '4',
      'label' => 'hidden',
      'format' => 'ds_post_date_rich_snippets_published_date',
      'formatter_settings' => array(
        'ft' => array(),
      ),
    ),
    'profile2' => array(
      'weight' => '0',
      'label' => 'hidden',
      'format' => 'compact_teaser',
    ),
    'field_resource_type' => array(
      'formatter_settings' => array(
        'ft' => array(
          'func' => 'theme_field',
          'classes' => 'type',
        ),
      ),
    ),
  );
  $export['node|document|compact_teaser'] = $ds_fieldsetting;

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'node|document|default';
  $ds_fieldsetting->entity_type = 'node';
  $ds_fieldsetting->bundle = 'document';
  $ds_fieldsetting->view_mode = 'default';
  $ds_fieldsetting->settings = array(
    'private_group_flag' => array(
      'weight' => '2',
      'label' => 'hidden',
      'format' => 'default',
    ),
    'links' => array(
      'weight' => '11',
      'label' => 'hidden',
      'format' => 'default',
    ),
    'comments' => array(
      'weight' => '13',
      'label' => 'above',
      'format' => 'default',
    ),
    'author' => array(
      'weight' => '4',
      'label' => 'hidden',
      'format' => 'author',
    ),
    'post_date' => array(
      'weight' => '0',
      'label' => 'hidden',
      'format' => 'ds_post_date_privatemsg_current_day',
    ),
    'flag_breakfast_recommendation' => array(
      'weight' => '12',
      'label' => 'hidden',
      'format' => 'default',
    ),
    'rate_integration_1' => array(
      'weight' => '3',
      'label' => 'hidden',
      'format' => 'default',
    ),
  );
  $export['node|document|default'] = $ds_fieldsetting;

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'node|document|full';
  $ds_fieldsetting->entity_type = 'node';
  $ds_fieldsetting->bundle = 'document';
  $ds_fieldsetting->view_mode = 'full';
  $ds_fieldsetting->settings = array(
    'private_group_flag' => array(
      'weight' => '1',
      'label' => 'hidden',
      'format' => 'default',
    ),
    'comments' => array(
      'weight' => '13',
      'label' => 'hidden',
      'format' => 'default',
    ),
    'author' => array(
      'weight' => '3',
      'label' => 'hidden',
      'format' => 'author_linked',
      'formatter_settings' => array(
        'ft' => array(),
        'field_delimiter' => '',
      ),
    ),
    'post_date' => array(
      'weight' => '2',
      'label' => 'hidden',
      'format' => 'ds_post_date_rich_snippets_published_date',
      'formatter_settings' => array(
        'ft' => array(),
      ),
    ),
    'flag_commons_follow_node' => array(
      'weight' => '10',
      'label' => 'hidden',
      'format' => 'default',
    ),
    'flag_breakfast_recommendation' => array(
      'weight' => '11',
      'label' => 'hidden',
      'format' => 'default',
      'formatter_settings' => array(
        'ft' => array(),
      ),
    ),
    'rate_integration_1' => array(
      'weight' => '9',
      'label' => 'hidden',
      'format' => 'default',
    ),
    'og_group_ref' => array(
      'formatter_settings' => array(
        'ft' => array(
          'lb' => 'Communities of Practice',
        ),
      ),
    ),
    'field_link' => array(
      'formatter_settings' => array(
        'ft' => array(
          'lb' => 'Links',
        ),
      ),
    ),
    'field_resource_type' => array(
      'formatter_settings' => array(
        'ft' => array(
          'func' => 'theme_field',
          'classes' => 'type',
        ),
      ),
    ),
  );
  $export['node|document|full'] = $ds_fieldsetting;

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'node|document|related_content';
  $ds_fieldsetting->entity_type = 'node';
  $ds_fieldsetting->bundle = 'document';
  $ds_fieldsetting->view_mode = 'related_content';
  $ds_fieldsetting->settings = array(
    'private_group_flag' => array(
      'weight' => '5',
      'label' => 'hidden',
      'format' => 'default',
    ),
    'author' => array(
      'weight' => '3',
      'label' => 'hidden',
      'format' => 'author_linked',
    ),
    'post_date' => array(
      'weight' => '2',
      'label' => 'hidden',
      'format' => 'ds_post_date_rich_snippets_published_date',
    ),
    'profile2' => array(
      'weight' => '0',
      'label' => 'hidden',
      'format' => 'related_content',
    ),
  );
  $export['node|document|related_content'] = $ds_fieldsetting;

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'node|document|search_result';
  $ds_fieldsetting->entity_type = 'node';
  $ds_fieldsetting->bundle = 'document';
  $ds_fieldsetting->view_mode = 'search_result';
  $ds_fieldsetting->settings = array(
    'private_group_flag' => array(
      'weight' => '1',
      'label' => 'hidden',
      'format' => 'default',
    ),
    'links' => array(
      'weight' => '9',
      'label' => 'hidden',
      'format' => 'default',
    ),
    'author' => array(
      'weight' => '3',
      'label' => 'hidden',
      'format' => 'author_linked',
    ),
    'post_date' => array(
      'weight' => '2',
      'label' => 'hidden',
      'format' => 'ds_post_date_rich_snippets_published_date',
      'formatter_settings' => array(
        'ft' => array(),
      ),
    ),
    'rate_integration_1' => array(
      'weight' => '8',
      'label' => 'hidden',
      'format' => 'default',
    ),
    'og_group_ref' => array(
      'formatter_settings' => array(
        'ft' => array(
          'lb' => 'Communities of Practice',
        ),
      ),
    ),
    'field_topics' => array(
      'formatter_settings' => array(
        'ft' => array(
          'lb-col' => TRUE,
        ),
      ),
    ),
    'field_resource_type' => array(
      'formatter_settings' => array(
        'ft' => array(
          'func' => 'theme_field',
        ),
      ),
    ),
  );
  $export['node|document|search_result'] = $ds_fieldsetting;

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'node|document|teaser';
  $ds_fieldsetting->entity_type = 'node';
  $ds_fieldsetting->bundle = 'document';
  $ds_fieldsetting->view_mode = 'teaser';
  $ds_fieldsetting->settings = array(
    'private_group_flag' => array(
      'weight' => '3',
      'label' => 'hidden',
      'format' => 'default',
    ),
    'author' => array(
      'weight' => '5',
      'label' => 'hidden',
      'format' => 'author_linked',
    ),
    'post_date' => array(
      'weight' => '1',
      'label' => 'hidden',
      'format' => 'ds_post_date_rich_snippets_published_date',
      'formatter_settings' => array(
        'ft' => array(),
      ),
    ),
    'comment_count' => array(
      'weight' => '11',
      'label' => 'hidden',
      'format' => 'default',
    ),
    'profile2' => array(
      'weight' => '0',
      'label' => 'hidden',
      'format' => 'compact_teaser',
    ),
    'rate_integration_1' => array(
      'weight' => '10',
      'label' => 'hidden',
      'format' => 'default',
    ),
    'og_group_ref' => array(
      'formatter_settings' => array(
        'ft' => array(
          'lb' => 'Communities of Practice',
        ),
      ),
    ),
    'field_resource_type' => array(
      'formatter_settings' => array(
        'ft' => array(
          'func' => 'theme_field',
          'classes' => 'type',
        ),
      ),
    ),
  );
  $export['node|document|teaser'] = $ds_fieldsetting;

  return $export;
}

/**
 * Implements hook_ds_layout_settings_info().
 */
function uwc_documents_ds_layout_settings_info() {
  $export = array();

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'comment|comment_node_document|default';
  $ds_layout->entity_type = 'comment';
  $ds_layout->bundle = 'comment_node_document';
  $ds_layout->view_mode = 'default';
  $ds_layout->layout = 'ds_2col';
  $ds_layout->settings = array(
    'regions' => array(
      'left' => array(
        0 => 'profile2',
      ),
      'right' => array(
        1 => 'links',
        2 => 'post_date',
        3 => 'comment_body',
        4 => 'flag_inappropriate_comment',
        5 => 'rate_integration_1',
      ),
    ),
    'fields' => array(
      'profile2' => 'left',
      'links' => 'right',
      'post_date' => 'right',
      'comment_body' => 'right',
      'flag_inappropriate_comment' => 'right',
      'rate_integration_1' => 'right',
    ),
    'classes' => array(
      'layout_class' => array(
        '' => '',
      ),
      'left' => array(
        'teaser-left' => 'teaser-left',
      ),
      'right' => array(
        'teaser-right' => 'teaser-right',
      ),
    ),
    'wrappers' => array(
      'left' => 'div',
      'right' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
  );
  $export['comment|comment_node_document|default'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|document|compact_teaser';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'document';
  $ds_layout->view_mode = 'compact_teaser';
  $ds_layout->layout = 'ds_2col';
  $ds_layout->settings = array(
    'regions' => array(
      'left' => array(
        0 => 'profile2',
      ),
      'right' => array(
        1 => 'field_resource_type',
        2 => 'private_group_flag',
        3 => 'title_field',
        4 => 'post_date',
        5 => 'author',
      ),
    ),
    'fields' => array(
      'profile2' => 'left',
      'field_resource_type' => 'right',
      'private_group_flag' => 'right',
      'title_field' => 'right',
      'post_date' => 'right',
      'author' => 'right',
    ),
    'classes' => array(
      'layout_class' => array(
        '' => '',
      ),
      'left' => array(
        'teaser-left' => 'teaser-left',
      ),
      'right' => array(
        'teaser-right' => 'teaser-right',
      ),
    ),
    'wrappers' => array(
      'left' => 'div',
      'right' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
  );
  $export['node|document|compact_teaser'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|document|default';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'document';
  $ds_layout->view_mode = 'default';
  $ds_layout->layout = 'ds_1col';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'post_date',
        1 => 'field_resource_type',
        2 => 'private_group_flag',
        3 => 'rate_integration_1',
        4 => 'author',
        5 => 'title_field',
        6 => 'og_group_ref',
        7 => 'field_topics',
        8 => 'body',
        9 => 'field_document_file',
        10 => 'field_assets',
        11 => 'links',
        12 => 'flag_breakfast_recommendation',
        13 => 'comments',
      ),
    ),
    'fields' => array(
      'post_date' => 'ds_content',
      'field_resource_type' => 'ds_content',
      'private_group_flag' => 'ds_content',
      'rate_integration_1' => 'ds_content',
      'author' => 'ds_content',
      'title_field' => 'ds_content',
      'og_group_ref' => 'ds_content',
      'field_topics' => 'ds_content',
      'body' => 'ds_content',
      'field_document_file' => 'ds_content',
      'field_assets' => 'ds_content',
      'links' => 'ds_content',
      'flag_breakfast_recommendation' => 'ds_content',
      'comments' => 'ds_content',
    ),
    'classes' => array(
      'layout_class' => array(
        '' => '',
      ),
    ),
    'wrappers' => array(
      'ds_content' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
  );
  $export['node|document|default'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|document|full';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'document';
  $ds_layout->view_mode = 'full';
  $ds_layout->layout = 'ds_1col';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'field_resource_type',
        1 => 'private_group_flag',
        2 => 'post_date',
        3 => 'title_field',
        4 => 'author',
        5 => 'body',
        6 => 'og_group_ref',
        7 => 'field_link',
        8 => 'field_document_file',
        9 => 'field_topics',
        10 => 'field_assets',
        11 => 'rate_integration_1',
        12 => 'flag_commons_follow_node',
        13 => 'flag_breakfast_recommendation',
        14 => 'group_node_tax',
        15 => 'comments',
      ),
    ),
    'fields' => array(
      'field_resource_type' => 'ds_content',
      'private_group_flag' => 'ds_content',
      'post_date' => 'ds_content',
      'title_field' => 'ds_content',
      'author' => 'ds_content',
      'body' => 'ds_content',
      'og_group_ref' => 'ds_content',
      'field_link' => 'ds_content',
      'field_document_file' => 'ds_content',
      'field_topics' => 'ds_content',
      'field_assets' => 'ds_content',
      'rate_integration_1' => 'ds_content',
      'flag_commons_follow_node' => 'ds_content',
      'flag_breakfast_recommendation' => 'ds_content',
      'group_node_tax' => 'ds_content',
      'comments' => 'ds_content',
    ),
    'classes' => array(
      'layout_class' => array(
        '' => '',
      ),
    ),
    'wrappers' => array(
      'ds_content' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
  );
  $export['node|document|full'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|document|related_content';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'document';
  $ds_layout->view_mode = 'related_content';
  $ds_layout->layout = 'ds_3col';
  $ds_layout->settings = array(
    'regions' => array(
      'left' => array(
        0 => 'profile2',
      ),
      'middle' => array(
        1 => 'title_field',
        2 => 'post_date',
        3 => 'author',
      ),
      'right' => array(
        4 => 'field_resource_type',
        5 => 'private_group_flag',
      ),
    ),
    'fields' => array(
      'profile2' => 'left',
      'title_field' => 'middle',
      'post_date' => 'middle',
      'author' => 'middle',
      'field_resource_type' => 'right',
      'private_group_flag' => 'right',
    ),
    'classes' => array(
      'layout_class' => array(
        '' => '',
      ),
    ),
    'wrappers' => array(
      'left' => 'div',
      'middle' => 'div',
      'right' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
  );
  $export['node|document|related_content'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|document|search_result';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'document';
  $ds_layout->view_mode = 'search_result';
  $ds_layout->layout = 'ds_1col';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'field_resource_type',
        1 => 'private_group_flag',
        2 => 'post_date',
        3 => 'author',
        4 => 'title_field',
        5 => 'body',
        6 => 'og_group_ref',
        7 => 'field_topics',
        8 => 'rate_integration_1',
        9 => 'links',
      ),
    ),
    'fields' => array(
      'field_resource_type' => 'ds_content',
      'private_group_flag' => 'ds_content',
      'post_date' => 'ds_content',
      'author' => 'ds_content',
      'title_field' => 'ds_content',
      'body' => 'ds_content',
      'og_group_ref' => 'ds_content',
      'field_topics' => 'ds_content',
      'rate_integration_1' => 'ds_content',
      'links' => 'ds_content',
    ),
    'classes' => array(
      'layout_class' => array(
        '' => '',
      ),
    ),
    'wrappers' => array(
      'ds_content' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
  );
  $export['node|document|search_result'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|document|teaser';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'document';
  $ds_layout->view_mode = 'teaser';
  $ds_layout->layout = 'ds_2col';
  $ds_layout->settings = array(
    'regions' => array(
      'left' => array(
        0 => 'profile2',
      ),
      'right' => array(
        1 => 'post_date',
        2 => 'field_resource_type',
        3 => 'private_group_flag',
        4 => 'title_field',
        5 => 'author',
        6 => 'body',
        7 => 'og_group_ref',
        8 => 'field_topics',
        9 => 'group_node_actions',
        10 => 'rate_integration_1',
        11 => 'comment_count',
      ),
    ),
    'fields' => array(
      'profile2' => 'left',
      'post_date' => 'right',
      'field_resource_type' => 'right',
      'private_group_flag' => 'right',
      'title_field' => 'right',
      'author' => 'right',
      'body' => 'right',
      'og_group_ref' => 'right',
      'field_topics' => 'right',
      'group_node_actions' => 'right',
      'rate_integration_1' => 'right',
      'comment_count' => 'right',
    ),
    'classes' => array(
      'layout_class' => array(
        '' => '',
      ),
      'left' => array(
        'teaser-left' => 'teaser-left',
      ),
      'right' => array(
        'teaser-right' => 'teaser-right',
      ),
    ),
    'wrappers' => array(
      'left' => 'div',
      'right' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
  );
  $export['node|document|teaser'] = $ds_layout;

  return $export;
}
