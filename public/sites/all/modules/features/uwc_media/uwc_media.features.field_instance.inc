<?php
/**
 * @file
 * uwc_media.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function uwc_media_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance: 'asset-audio-field_asset_audio'
  $field_instances['asset-audio-field_asset_audio'] = array(
    'bundle' => 'audio',
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'module' => 'jplayer',
        'settings' => array(
          'autoplay' => 0,
          'backgroundColor' => 0,
          'field_delimiter' => '',
          'mode' => 'single',
          'muted' => 0,
          'preload' => 'metadata',
          'repeat' => 'none',
          'solution' => 'html, flash',
          'volume' => 80,
        ),
        'type' => 'jplayer_player',
        'weight' => 0,
      ),
      'full' => array(
        'label' => 'hidden',
        'module' => 'jplayer',
        'settings' => array(
          'autoplay' => 0,
          'backgroundColor' => 0,
          'field_delimiter' => '',
          'mode' => 'playlist',
          'muted' => FALSE,
          'preload' => 'metadata',
          'repeat' => 'none',
          'solution' => 'html, flash',
          'volume' => 80,
        ),
        'type' => 'jplayer_player',
        'weight' => 0,
      ),
      'small' => array(
        'label' => 'hidden',
        'module' => 'jplayer',
        'settings' => array(
          'autoplay' => 0,
          'backgroundColor' => 0,
          'field_delimiter' => '',
          'mode' => 'single',
          'muted' => 0,
          'preload' => 'metadata',
          'repeat' => 'none',
          'solution' => 'html, flash',
          'volume' => 80,
        ),
        'type' => 'jplayer_player',
        'weight' => 0,
      ),
      'widget_search' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'asset',
    'field_name' => 'field_asset_audio',
    'label' => 'Audio',
    'required' => 1,
    'settings' => array(
      'description_field' => 1,
      'file_directory' => 'asset/audio',
      'file_extensions' => 'mp3 ogg mp4',
      'max_filesize' => '',
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'file',
      'settings' => array(
        'progress_indicator' => 'throbber',
      ),
      'type' => 'file_generic',
      'weight' => 1,
    ),
  );

  // Exported field_instance: 'asset-document-field_asset_document_desc'
  $field_instances['asset-document-field_asset_document_desc'] = array(
    'bundle' => 'document',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'module' => 'text',
        'settings' => array(
          'field_delimiter' => '',
        ),
        'type' => 'text_default',
        'weight' => 2,
      ),
      'full' => array(
        'label' => 'hidden',
        'module' => 'text',
        'settings' => array(
          'field_delimiter' => '',
        ),
        'type' => 'text_default',
        'weight' => 2,
      ),
      'small' => array(
        'label' => 'hidden',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 1,
      ),
      'widget_search' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 1,
      ),
    ),
    'entity_type' => 'asset',
    'field_name' => 'field_asset_document_desc',
    'label' => 'Description',
    'required' => 0,
    'settings' => array(
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'rows' => 4,
      ),
      'type' => 'text_textarea',
      'weight' => 2,
    ),
  );

  // Exported field_instance: 'asset-document-field_asset_document_file'
  $field_instances['asset-document-field_asset_document_file'] = array(
    'bundle' => 'document',
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'module' => 'file',
        'settings' => array(
          'field_delimiter' => '',
        ),
        'type' => 'file_default',
        'weight' => 1,
      ),
      'full' => array(
        'label' => 'hidden',
        'module' => 'file',
        'settings' => array(
          'field_delimiter' => '',
        ),
        'type' => 'file_default',
        'weight' => 1,
      ),
      'small' => array(
        'label' => 'hidden',
        'module' => 'file',
        'settings' => array(
          'field_delimiter' => '',
        ),
        'type' => 'file_default',
        'weight' => 0,
      ),
      'widget_search' => array(
        'label' => 'hidden',
        'module' => 'file',
        'settings' => array(
          'field_delimiter' => '',
        ),
        'type' => 'file_default',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'asset',
    'field_name' => 'field_asset_document_file',
    'label' => 'Document',
    'required' => 1,
    'settings' => array(
      'description_field' => 0,
      'file_directory' => 'asset/document',
      'file_extensions' => 'pdf doc xls docx xlsx odt',
      'max_filesize' => '',
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'file',
      'settings' => array(
        'progress_indicator' => 'throbber',
      ),
      'type' => 'file_generic',
      'weight' => 1,
    ),
  );

  // Exported field_instance: 'asset-image-field_asset_image'
  $field_instances['asset-image-field_asset_image'] = array(
    'bundle' => 'image',
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'module' => 'image',
        'settings' => array(
          'field_delimiter' => '',
          'image_link' => '',
          'image_style' => 'asset_image_full',
        ),
        'type' => 'image',
        'weight' => 0,
      ),
      'full' => array(
        'label' => 'hidden',
        'module' => 'image',
        'settings' => array(
          'field_delimiter' => '',
          'image_link' => '',
          'image_style' => 'asset_image_full',
        ),
        'type' => 'image',
        'weight' => 0,
      ),
      'small' => array(
        'label' => 'hidden',
        'module' => 'image',
        'settings' => array(
          'field_delimiter' => '',
          'image_link' => '',
          'image_style' => 'asset_image_small',
        ),
        'type' => 'image',
        'weight' => 0,
      ),
      'widget_search' => array(
        'label' => 'hidden',
        'module' => 'image',
        'settings' => array(
          'field_delimiter' => '',
          'image_link' => '',
          'image_style' => 'asset_image_widget_search',
        ),
        'type' => 'image',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'asset',
    'field_name' => 'field_asset_image',
    'label' => 'Image',
    'required' => 1,
    'settings' => array(
      'alt_field' => 0,
      'default_image' => 0,
      'file_directory' => 'assets/images',
      'file_extensions' => 'png gif jpg jpeg',
      'max_filesize' => '',
      'max_resolution' => '',
      'min_resolution' => '',
      'title_field' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'image',
      'settings' => array(
        'preview_image_style' => 'medium',
        'progress_indicator' => 'throbber',
      ),
      'type' => 'image_image',
      'weight' => 1,
    ),
  );

  // Exported field_instance: 'asset-image-field_asset_image_copyright'
  $field_instances['asset-image-field_asset_image_copyright'] = array(
    'bundle' => 'image',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'module' => 'text',
        'settings' => array(
          'field_delimiter' => '',
        ),
        'type' => 'text_default',
        'weight' => 1,
      ),
      'full' => array(
        'label' => 'hidden',
        'module' => 'text',
        'settings' => array(
          'field_delimiter' => '',
        ),
        'type' => 'text_default',
        'weight' => 1,
      ),
      'small' => array(
        'label' => 'hidden',
        'module' => 'text',
        'settings' => array(
          'field_delimiter' => '',
        ),
        'type' => 'text_default',
        'weight' => 1,
      ),
      'widget_search' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 2,
      ),
    ),
    'entity_type' => 'asset',
    'field_name' => 'field_asset_image_copyright',
    'label' => 'Copyright',
    'required' => 0,
    'settings' => array(
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'size' => 60,
      ),
      'type' => 'text_textfield',
      'weight' => 2,
    ),
  );

  // Exported field_instance: 'asset-image-field_asset_image_description'
  $field_instances['asset-image-field_asset_image_description'] = array(
    'bundle' => 'image',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'module' => 'text',
        'settings' => array(
          'field_delimiter' => '',
        ),
        'type' => 'text_default',
        'weight' => 2,
      ),
      'full' => array(
        'label' => 'hidden',
        'module' => 'text',
        'settings' => array(
          'field_delimiter' => '',
        ),
        'type' => 'text_default',
        'weight' => 2,
      ),
      'small' => array(
        'label' => 'hidden',
        'module' => 'text',
        'settings' => array(
          'field_delimiter' => '',
        ),
        'type' => 'text_default',
        'weight' => 2,
      ),
      'widget_search' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 1,
      ),
    ),
    'entity_type' => 'asset',
    'field_name' => 'field_asset_image_description',
    'label' => 'Description',
    'required' => 0,
    'settings' => array(
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'rows' => 5,
      ),
      'type' => 'text_textarea',
      'weight' => 3,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Audio');
  t('Copyright');
  t('Description');
  t('Document');
  t('Image');

  return $field_instances;
}
