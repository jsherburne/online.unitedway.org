<?php
/**
 * @file
 * uwc_discussions_via_email.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function uwc_discussions_via_email_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "feeds" && $api == "feeds_importer_default") {
    return array("version" => "1");
  }
}
