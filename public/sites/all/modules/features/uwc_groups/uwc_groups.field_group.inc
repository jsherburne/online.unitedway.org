<?php
/**
 * @file
 * uwc_groups.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function uwc_groups_field_group_info() {
  $export = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_node_actions|node|group|teaser';
  $field_group->group_name = 'group_node_actions';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'group';
  $field_group->mode = 'teaser';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Node Actions',
    'weight' => '9',
    'children' => array(
      0 => 'comment_count',
      1 => 'rate_integration_1',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'Node Actions',
      'instance_settings' => array(
        'classes' => 'group-node-actions field-group-div',
        'description' => '',
        'show_label' => '0',
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'fast',
      ),
      'formatter' => 'open',
    ),
  );
  $export['group_node_actions|node|group|teaser'] = $field_group;

  return $export;
}
