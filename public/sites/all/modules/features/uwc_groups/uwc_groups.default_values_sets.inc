<?php
/**
 * @file
 * uwc_groups.default_values_sets.inc
 */

/**
 * Implements hook_default_values_values().
 */
function uwc_groups_default_values_values() {
  $export = array();

  $values = new stdClass();
  $values->disabled = FALSE; /* Edit this to true to make a default values disabled initially */
  $values->api_version = 1;
  $values->name = 'group_type';
  $values->title = 'Group Type';
  $values->description = 'Various types available for groups.';
  $values->data = array(
    0 => array(
      'key' => 'issue_areas',
      'value' => 'Issue Areas',
      'weight' => '0',
    ),
    1 => array(
      'key' => 'functional_areas',
      'value' => 'Functional Areas',
      'weight' => '1',
    ),
  );
  $export['group_type'] = $values;

  return $export;
}
