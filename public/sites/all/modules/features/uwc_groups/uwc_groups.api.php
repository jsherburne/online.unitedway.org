<?php
/**
 * @file
 * Hooks provided by UWC: Groups module.
 */

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Define subpages that exist inside of every group.
 *
 * An array of subpages should be defined keyed by the sub-page slug and
 * defining the title to be displayed as the link.
 */
function hook_uwc_groups_subpage_info() {
  $subpages = array();

  $subpages['events'] = array(
    // title: The title to be displayed on the link to the page
    'title' => t('Events directory'),
    // path: where the link should point to, %gid will be replaced with actual id
    'path' => 'directory/events/%gid',
  );

  return $subpages;
}

/**
 * @} End of "addtogroup hooks".
 */
