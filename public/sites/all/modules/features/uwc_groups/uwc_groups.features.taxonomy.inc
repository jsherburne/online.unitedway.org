<?php
/**
 * @file
 * uwc_groups.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function uwc_groups_taxonomy_default_vocabularies() {
  return array();
}
