<?php
/**
 * @file
 * Installation and update functionality for uwc_groups.
 */

/**
 * Move the CoP subpage block to uwc_groups.
 */
function uwc_groups_update_7000() {
  features_revert_module('uwc_groups');
}

/**
 * Replace groups directory with separated listings.
 */
function uwc_groups_update_7001() {
  module_disable(array('commons_groups_directory'));
  drupal_uninstall_modules(array('commons_groups_directory'));
  
  features_revert_module('uwc_groups');
}

/**
 * Enable the variable module and update group landing page configurations.
 */
function uwc_groups_update_7002() {
  module_enable(array('variable'));

  features_revert_module('uwc_search');
  features_revert_module('uwc_groups');
}

/**
 * [#18512] Enable facet rewriting for CoP topics.
 */
function uwc_groups_update_7003() {
  features_revert_module('uwc_search');
}

/**
 * [#16048] Revert features to update OG permissions.
 */
function uwc_groups_update_7004() {
  $modules = array(
    'uwc_groups',
    'uwc_documents',
    'uwc_events',
    'uwc_news',
    'uwc_pages',
    'uwc_polls',
    'uwc_q_a',
  );

  foreach ($modules as $module) {
    features_revert_module($module);
  }
}

/**
 * [#18726] Enable rules for group subscription/follow relationship.
 */
function uwc_groups_update_7005() {
  // Enable the Rules module
  module_enable(array('rules'));

  // Ensure rules are enabled and put in place
  features_revert_module('uwc_groups');
}

/**
 * [#18726] Prevent administrative group members from being unsubscribed from a group.
 */
function uwc_groups_update_7006() {
  features_revert(array(
    'uwc_groups' => array('rules_config'),
  ));
}

/**
 * [#18385] Migrate group panelizer configuration into basic panel pages.
 */
function uwc_groups_update_7007() {
  // Disable commons handling of group pages
  module_disable(array('commons_groups_pages'));

  // Enable new landing page submodule
  module_enable(array('uwc_groups_pages'));

  // Ensure our updates are installed
  features_revert_module('uwc_groups');
  features_revert_module('uwc_groups_pages');
}

/**
 * Revert all groups to default og permissions.
 */
function uwc_groups_update_7008() {
  $group_type = 'node';
  $groups = og_get_all_group($group_type);

  foreach ($groups as $gid) {
    if (!og_is_group_default_access('node', $gid)) {
      $wrapper = entity_metadata_wrapper($group_type, $gid);
      $bundle = $wrapper->getBundle();

      if (!field_info_instance($group_type, OG_DEFAULT_ACCESS_FIELD, $bundle)) {
        return variable_set("og_is_group_default_access__{$group_type}__{$bundle}", TRUE);
      }
      else {
        // Revert to default group permissions
        $wrapper->{OG_DEFAULT_ACCESS_FIELD}->set(FALSE);
        $wrapper->save();
      }
    }
  }
}



/**
 * [#18972] Revert module after changes to views and feature overrides.
 */
function uwc_groups_update_7009() {
  features_revert_module('uwc_groups');
}

/**
 * [#24330] enable panels_flags, panels_rate, easy_breadcrumb, mini_panels modules.
 */
function uwc_groups_update_7010() {
  $modules = array(
    'panels_flags',
    'panels_rate',
    'easy_breadcrumb',
    'mini_panels',
  );

  module_enable($modules);
}

/**
 * [#24330] disable uwc_groups_pages.
 */
function uwc_groups_update_7011() {
  $modules = array('uwc_groups_pages');
  module_disable($modules);
}

/**
 * [#39954] disable commons_groups and commons_groups_pages.
 */
function uwc_groups_update_7012() {
  $modules = array('commons_groups_pages', 'commons_groups');
  module_disable($modules);
  drupal_uninstall_modules($modules);
}
