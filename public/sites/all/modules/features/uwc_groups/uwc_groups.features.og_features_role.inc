<?php
/**
 * @file
 * uwc_groups.features.og_features_role.inc
 */

/**
 * Implements hook_og_features_default_roles().
 */
function uwc_groups_og_features_default_roles() {
  $roles = array();

  // Exported OG Role: 'node:group:group leader'.
  $roles['node:group:group leader'] = array(
    'gid' => 0,
    'group_type' => 'node',
    'group_bundle' => 'group',
    'name' => 'group leader',
  );

  // Exported OG Role: 'node:group:topic leader'.
  $roles['node:group:topic leader'] = array(
    'gid' => 0,
    'group_type' => 'node',
    'group_bundle' => 'group',
    'name' => 'topic leader',
  );

  return $roles;
}
