<?php
/**
 * @file
 * uwc_groups.rules_defaults.inc
 */

/**
 * Implements hook_default_rules_configuration().
 */
function uwc_groups_default_rules_configuration() {
  $items = array();
  $items['rules_og_follow_and_join'] = entity_import('rules_config', '{ "rules_og_follow_and_join" : {
      "LABEL" : "OG Follow and Join",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "REQUIRES" : [ "og", "flag" ],
      "ON" : { "flag_flagged_commons_follow_group" : [] },
      "IF" : [
        { "og_user_has_permission" : {
            "permission" : "subscribe",
            "group" : [ "flagged-node" ],
            "account" : [ "flagging-user" ]
          }
        }
      ],
      "DO" : [
        { "og_subcribe_user" : { "user" : [ "flagging-user" ], "group" : [ "flagged-node" ] } }
      ]
    }
  }');
  $items['rules_og_unfollow_and_unsubscribe'] = entity_import('rules_config', '{ "rules_og_unfollow_and_unsubscribe" : {
      "LABEL" : "OG Unfollow and Unsubscribe",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "REQUIRES" : [ "og", "flag" ],
      "ON" : { "flag_unflagged_commons_follow_group" : [] },
      "IF" : [
        { "NOT og_user_has_permission" : {
            "permission" : "administer group",
            "group" : [ "flagged-node" ],
            "account" : [ "flagging-user" ]
          }
        }
      ],
      "DO" : [
        { "og_unsubscribe_user" : { "user" : [ "flagging-user" ], "group" : [ "flagged-node" ] } }
      ]
    }
  }');
  $items['uwc_groups_og_follow_and_join_moderated'] = entity_import('rules_config', '{ "uwc_groups_og_follow_and_join_moderated" : {
      "LABEL" : "OG Follow and Join: Moderated",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "REQUIRES" : [ "og", "uwc_groups", "flag" ],
      "ON" : { "flag_flagged_commons_follow_group" : [] },
      "IF" : [
        { "NOT og_user_has_permission" : {
            "permission" : "subscribe",
            "group" : [ "flagged-node" ],
            "account" : [ "flagging-user" ]
          }
        }
      ],
      "DO" : [
        { "uwc_subcribe_user_pending" : { "user" : [ "flagging-user" ], "group" : [ "flagged-node" ] } }
      ]
    }
  }');
  return $items;
}
