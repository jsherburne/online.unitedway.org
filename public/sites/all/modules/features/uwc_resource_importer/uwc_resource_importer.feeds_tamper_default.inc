<?php
/**
 * @file
 * uwc_resource_importer.feeds_tamper_default.inc
 */

/**
 * Implements hook_feeds_tamper_default().
 */
function uwc_resource_importer_feeds_tamper_default() {
  $export = array();

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'uw_news_importer-xpathparser_6-find_replace_regex';
  $feeds_tamper->importer = 'uw_news_importer';
  $feeds_tamper->source = 'xpathparser:6';
  $feeds_tamper->plugin_id = 'find_replace_regex';
  $feeds_tamper->settings = array(
    'find' => '/\\.[0-9]*/',
    'replace' => ' -0500',
    'limit' => '1',
    'real_limit' => '1',
  );
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = 'Find replace REGEX';
  $export['uw_news_importer-xpathparser_6-find_replace_regex'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'uw_news_importer-xpathparser_6-strtotime';
  $feeds_tamper->importer = 'uw_news_importer';
  $feeds_tamper->source = 'xpathparser:6';
  $feeds_tamper->plugin_id = 'strtotime';
  $feeds_tamper->settings = '';
  $feeds_tamper->weight = 1;
  $feeds_tamper->description = 'String to Unix timestamp';
  $export['uw_news_importer-xpathparser_6-strtotime'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'uw_news_importer-xpathparser_8-keyword_filter';
  $feeds_tamper->importer = 'uw_news_importer';
  $feeds_tamper->source = 'xpathparser:8';
  $feeds_tamper->plugin_id = 'keyword_filter';
  $feeds_tamper->settings = array(
    'words' => 'News',
    'word_boundaries' => 1,
    'exact' => 0,
    'case_sensitive' => 0,
    'invert' => 0,
    'word_list' => array(
      0 => '/\\b\\QNews\\E\\b/ui',
    ),
    'regex' => TRUE,
  );
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = 'Type Filter';
  $export['uw_news_importer-xpathparser_8-keyword_filter'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'uw_resource_import-xpathparser_1-find_replace';
  $feeds_tamper->importer = 'uw_resource_import';
  $feeds_tamper->source = 'xpathparser:1';
  $feeds_tamper->plugin_id = 'find_replace';
  $feeds_tamper->settings = array(
    'find' => 'Global Corporate Leader',
    'replace' => 'Profile',
    'case_sensitive' => 0,
    'word_boundaries' => 1,
    'whole' => 0,
    'regex' => TRUE,
    'regex_find' => '/\\bGlobal Corporate Leader\\b/i',
  );
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = 'Set "Global Corporate Leader" to "Profile"';
  $export['uw_resource_import-xpathparser_1-find_replace'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'uw_resource_import-xpathparser_1-find_replace_2';
  $feeds_tamper->importer = 'uw_resource_import';
  $feeds_tamper->source = 'xpathparser:1';
  $feeds_tamper->plugin_id = 'find_replace';
  $feeds_tamper->settings = array(
    'find' => 'Local United Way',
    'replace' => 'Profile',
    'case_sensitive' => 0,
    'word_boundaries' => 1,
    'whole' => 0,
    'regex' => TRUE,
    'regex_find' => '/\\bLocal United Way\\b/i',
  );
  $feeds_tamper->weight = 1;
  $feeds_tamper->description = 'Set "Local United Way" to "Profile"';
  $export['uw_resource_import-xpathparser_1-find_replace_2'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'uw_resource_import-xpathparser_1-find_replace_conference';
  $feeds_tamper->importer = 'uw_resource_import';
  $feeds_tamper->source = 'xpathparser:1';
  $feeds_tamper->plugin_id = 'find_replace';
  $feeds_tamper->settings = array(
    'find' => 'Conference',
    'replace' => 'Event Materials',
    'case_sensitive' => 0,
    'word_boundaries' => 1,
    'whole' => 0,
    'regex' => TRUE,
    'regex_find' => '/\\bConference\\b/i',
  );
  $feeds_tamper->weight = 6;
  $feeds_tamper->description = 'Set "Conference" to "Event Materials"';
  $export['uw_resource_import-xpathparser_1-find_replace_conference'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'uw_resource_import-xpathparser_1-find_replace_meeting';
  $feeds_tamper->importer = 'uw_resource_import';
  $feeds_tamper->source = 'xpathparser:1';
  $feeds_tamper->plugin_id = 'find_replace';
  $feeds_tamper->settings = array(
    'find' => 'Meeting',
    'replace' => 'Event Materials',
    'case_sensitive' => 0,
    'word_boundaries' => 1,
    'whole' => 0,
    'regex' => TRUE,
    'regex_find' => '/\\bMeeting\\b/i',
  );
  $feeds_tamper->weight = 7;
  $feeds_tamper->description = 'Set "Meeting" to "Event Materials"';
  $export['uw_resource_import-xpathparser_1-find_replace_meeting'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'uw_resource_import-xpathparser_1-find_replace_seminar';
  $feeds_tamper->importer = 'uw_resource_import';
  $feeds_tamper->source = 'xpathparser:1';
  $feeds_tamper->plugin_id = 'find_replace';
  $feeds_tamper->settings = array(
    'find' => 'Seminar',
    'replace' => 'Event Materials',
    'case_sensitive' => 0,
    'word_boundaries' => 1,
    'whole' => 0,
    'regex' => TRUE,
    'regex_find' => '/\\bSeminar\\b/i',
  );
  $feeds_tamper->weight = 5;
  $feeds_tamper->description = 'Set "Seminar" to "Event Materials"';
  $export['uw_resource_import-xpathparser_1-find_replace_seminar'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'uw_resource_import-xpathparser_1-find_replace_special_event';
  $feeds_tamper->importer = 'uw_resource_import';
  $feeds_tamper->source = 'xpathparser:1';
  $feeds_tamper->plugin_id = 'find_replace';
  $feeds_tamper->settings = array(
    'find' => 'Special Event',
    'replace' => 'Event Materials',
    'case_sensitive' => 0,
    'word_boundaries' => 1,
    'whole' => 0,
    'regex' => TRUE,
    'regex_find' => '/\\bSpecial Event\\b/i',
  );
  $feeds_tamper->weight = 4;
  $feeds_tamper->description = 'Set "Special Event" to "Event Materials"';
  $export['uw_resource_import-xpathparser_1-find_replace_special_event'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'uw_resource_import-xpathparser_1-find_replace_training';
  $feeds_tamper->importer = 'uw_resource_import';
  $feeds_tamper->source = 'xpathparser:1';
  $feeds_tamper->plugin_id = 'find_replace';
  $feeds_tamper->settings = array(
    'find' => 'Training',
    'replace' => 'Event Materials',
    'case_sensitive' => 0,
    'word_boundaries' => 1,
    'whole' => 0,
    'regex' => TRUE,
    'regex_find' => '/\\bTraining\\b/i',
  );
  $feeds_tamper->weight = 3;
  $feeds_tamper->description = 'Set "Training" to "Event Materials"';
  $export['uw_resource_import-xpathparser_1-find_replace_training'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'uw_resource_import-xpathparser_1-find_replace_webinar';
  $feeds_tamper->importer = 'uw_resource_import';
  $feeds_tamper->source = 'xpathparser:1';
  $feeds_tamper->plugin_id = 'find_replace';
  $feeds_tamper->settings = array(
    'find' => 'Webinar',
    'replace' => 'Event Materials',
    'case_sensitive' => 0,
    'word_boundaries' => 1,
    'whole' => 0,
    'regex' => TRUE,
    'regex_find' => '/\\bWebinar\\b/i',
  );
  $feeds_tamper->weight = 2;
  $feeds_tamper->description = 'Set "Webinar" to "Event Materials"';
  $export['uw_resource_import-xpathparser_1-find_replace_webinar'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'uw_resource_import-xpathparser_10-keyword_filter';
  $feeds_tamper->importer = 'uw_resource_import';
  $feeds_tamper->source = 'xpathparser:10';
  $feeds_tamper->plugin_id = 'keyword_filter';
  $feeds_tamper->settings = array(
    'words' => 'Profile
Tool
Event',
    'word_boundaries' => 1,
    'exact' => 0,
    'case_sensitive' => 0,
    'invert' => 0,
    'word_list' => array(
      0 => '/\\b\\QProfile\\E\\b/ui',
      1 => '/\\b\\QTool\\E\\b/ui',
      2 => '/\\b\\QEvent\\E\\b/ui',
    ),
    'regex' => TRUE,
  );
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = 'Type Filter';
  $export['uw_resource_import-xpathparser_10-keyword_filter'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'uw_resource_import-xpathparser_6-find_replace_regex';
  $feeds_tamper->importer = 'uw_resource_import';
  $feeds_tamper->source = 'xpathparser:6';
  $feeds_tamper->plugin_id = 'find_replace_regex';
  $feeds_tamper->settings = array(
    'find' => '/\\.[0-9]*/',
    'replace' => ' -0500',
    'limit' => '1',
    'real_limit' => '1',
  );
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = 'Find replace REGEX';
  $export['uw_resource_import-xpathparser_6-find_replace_regex'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'uw_resource_import-xpathparser_6-strtotime';
  $feeds_tamper->importer = 'uw_resource_import';
  $feeds_tamper->source = 'xpathparser:6';
  $feeds_tamper->plugin_id = 'strtotime';
  $feeds_tamper->settings = '';
  $feeds_tamper->weight = 1;
  $feeds_tamper->description = 'String to Unix timestamp';
  $export['uw_resource_import-xpathparser_6-strtotime'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'uw_resource_import-xpathparser_7-find_replace_regex';
  $feeds_tamper->importer = 'uw_resource_import';
  $feeds_tamper->source = 'xpathparser:7';
  $feeds_tamper->plugin_id = 'find_replace_regex';
  $feeds_tamper->settings = array(
    'find' => '/^\\//',
    'replace' => 'http://online.unitedway.org/',
    'limit' => '',
    'real_limit' => -1,
  );
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = 'Make URLs absolute';
  $export['uw_resource_import-xpathparser_7-find_replace_regex'] = $feeds_tamper;

  return $export;
}
