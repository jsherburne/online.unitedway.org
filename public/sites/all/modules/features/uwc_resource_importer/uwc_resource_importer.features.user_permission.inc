<?php
/**
 * @file
 * uwc_resource_importer.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function uwc_resource_importer_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'clear uw_news_importer feeds'.
  $permissions['clear uw_news_importer feeds'] = array(
    'name' => 'clear uw_news_importer feeds',
    'roles' => array(),
    'module' => 'feeds',
  );

  // Exported permission: 'clear uw_resource_import feeds'.
  $permissions['clear uw_resource_import feeds'] = array(
    'name' => 'clear uw_resource_import feeds',
    'roles' => array(),
    'module' => 'feeds',
  );

  // Exported permission: 'import uw_news_importer feeds'.
  $permissions['import uw_news_importer feeds'] = array(
    'name' => 'import uw_news_importer feeds',
    'roles' => array(),
    'module' => 'feeds',
  );

  // Exported permission: 'import uw_resource_import feeds'.
  $permissions['import uw_resource_import feeds'] = array(
    'name' => 'import uw_resource_import feeds',
    'roles' => array(),
    'module' => 'feeds',
  );

  // Exported permission: 'tamper uw_news_importer'.
  $permissions['tamper uw_news_importer'] = array(
    'name' => 'tamper uw_news_importer',
    'roles' => array(),
    'module' => 'feeds_tamper',
  );

  // Exported permission: 'tamper uw_resource_import'.
  $permissions['tamper uw_resource_import'] = array(
    'name' => 'tamper uw_resource_import',
    'roles' => array(),
    'module' => 'feeds_tamper',
  );

  // Exported permission: 'unlock uw_news_importer feeds'.
  $permissions['unlock uw_news_importer feeds'] = array(
    'name' => 'unlock uw_news_importer feeds',
    'roles' => array(),
    'module' => 'feeds',
  );

  // Exported permission: 'unlock uw_resource_import feeds'.
  $permissions['unlock uw_resource_import feeds'] = array(
    'name' => 'unlock uw_resource_import feeds',
    'roles' => array(),
    'module' => 'feeds',
  );

  return $permissions;
}
