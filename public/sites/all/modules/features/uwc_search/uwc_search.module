<?php
/**
 * @file
 * Code for the UW Connect: Search feature.
 */

include_once 'uwc_search.features.inc';

/**
 * Implements hook_block_info().
 */
function uwc_search_block_info() {
  $blocks['search_form'] = array(
    'info' => t('Keyword search form'),
    'cache' => DRUPAL_CACHE_GLOBAL,
  );

  return $blocks;
}

/**
 * Implements hook_block_view().
 */
function uwc_search_block_view($delta = '') {
  $block = array();

  switch ($delta) {
    case 'search_form':
			$block['subject'] = t('Search');
      $block['content'] = drupal_get_form('uwc_search_global_search_form');
      break;
  }

  return $block;
}

/**
 * Search form callback for the global search form.
 *
 * @TODO Extract module_invoke_all call into new function to add caching
 */
function uwc_search_global_search_form($form, &$form_state) {
  $searches = module_invoke_all('uwc_search_type_info');
  $search_type = 'content'; //default search defined in this module
  $search_options = array(); //for search dropdown
  $keyword = '';

  foreach ($searches as $key => $search) {
    $search_options[$key] = $search['label'];

    if(isset($_GET[$search['keyword']])){
      $keyword = check_plain($_GET[$search['keyword']]);
      $search_type = $key;
    }
  }

  //set up the keyword field
  $form['keyword'] = array(
    '#type' => 'textfield',
    '#title' => t('Keyword search'),
    '#title_display' => 'invisible',
    '#attributes' => array(
      //'placeholder' => t('Search'),
    ),
    '#default_value' => $keyword,
  );

  //set up the various types of searches
  $form['search_type'] = array(
    '#type' => 'select',
    '#title' => t('Type'),
    '#title_display' => 'invisible',
    '#options' => $search_options,
    '#default_value' => $search_type,
		'#weight' => -5,
  );

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    //'#value' => t('Search'),
  );

  //$form['#action'] = '/home';

  $form['#token'] = FALSE;
  return $form;
}

/**
 * Form submission handler for global search form.
 *
 * @see uwc_search_global_search_form().
 *
 * @TODO Extract module_invoke_all call into new function to add caching
 */
function uwc_search_global_search_form_submit($form, &$form_state) {
  $searches = module_invoke_all('uwc_search_type_info');
  $keyword = $form_state['values']['keyword'];

  //make sure we have a valid search type, or default to type defined in this module
  $type = check_plain($form_state['values']['search_type']);
  if(!isset($searches[$type])) {
    $type = 'content';
  }

  // Redirect to the search page with the keyword in the query string
  $form_state['redirect'] = array(
    $searches[$type]['path'],
    array(
      'query' => array(
        $searches[$type]['keyword'] => $keyword,
      ),
    ),
  );
}
