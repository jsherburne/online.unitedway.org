<?php
/**
 * @file
 * uwc_search.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function uwc_search_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "facetapi" && $api == "facetapi_defaults") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function uwc_search_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_default_search_api_index().
 */
function uwc_search_default_search_api_index() {
  $items = array();
  $items['default_node_index'] = entity_import('search_api_index', '{
    "name" : "Default node index",
    "machine_name" : "default_node_index",
    "description" : "An automatically created search index for indexing node data. Might be configured to specific needs.",
    "server" : "default",
    "item_type" : "node",
    "options" : {
      "index_directly" : 1,
      "cron_limit" : "50",
      "data_alter_callbacks" : {
        "search_api_alter_bundle_filter" : {
          "status" : 1,
          "weight" : "-10",
          "settings" : { "default" : "1", "bundles" : { "organization" : "organization" } }
        },
        "search_api_alter_node_status" : { "status" : 0, "weight" : "0", "settings" : [] },
        "search_api_alter_add_combined" : {
          "status" : 1,
          "weight" : "0",
          "settings" : { "fields" : { "search_api_combined_1" : {
                "name" : "Content Subtype",
                "imitate" : "field_event_type",
                "fields" : [
                  "field_group_type",
                  "field_resource_type",
                  "field_event_type",
                  "field_answer_type",
                  "field_discussion_type",
                  "field_news_type"
                ],
                "description" : "A @type combined of the following fields: Group Type, Resource Type, Event Type, Page Type, Poll Type, Answer type, Discussion Type, News Type."
              }
            }
          }
        },
        "search_api_alter_node_access" : { "status" : 1, "weight" : "0", "settings" : [] },
        "search_api_alter_add_viewed_entity" : { "status" : 0, "weight" : "0", "settings" : { "mode" : "full" } },
        "search_api_alter_add_url" : { "status" : 0, "weight" : "0", "settings" : [] },
        "search_api_alter_add_aggregation" : { "status" : 0, "weight" : "0", "settings" : [] },
        "search_api_alter_add_hierarchy" : { "status" : 0, "weight" : "0", "settings" : { "fields" : [] } }
      },
      "processors" : {
        "search_api_case_ignore" : {
          "status" : 1,
          "weight" : "0",
          "settings" : { "fields" : {
              "type" : true,
              "title" : true,
              "author" : true,
              "title_field" : true,
              "field_topics" : true,
              "og_group_ref" : true,
              "search_api_language" : true,
              "body:value" : true,
              "field_address:country" : true,
              "field_address:administrative_area" : true
            }
          }
        },
        "search_api_html_filter" : {
          "status" : 1,
          "weight" : "10",
          "settings" : {
            "fields" : { "title" : true, "body:value" : true },
            "title" : 0,
            "alt" : 1,
            "tags" : "h1 = 5\\r\\nh2 = 3\\r\\nh3 = 2\\r\\nstrong = 2\\r\\nb = 2\\r\\nem = 1.5\\r\\nu = 1.5"
          }
        },
        "search_api_tokenizer" : {
          "status" : 1,
          "weight" : "20",
          "settings" : {
            "fields" : { "body:value" : true },
            "spaces" : "[^\\\\p{L}\\\\p{N}]",
            "ignorable" : "[-]"
          }
        },
        "search_api_stopwords" : {
          "status" : 0,
          "weight" : "30",
          "settings" : {
            "fields" : { "title" : true, "body:value" : true },
            "file" : "",
            "stopwords" : "but\\r\\ndid\\r\\nthe this that those\\r\\netc"
          }
        },
        "search_api_highlighting" : {
          "status" : 0,
          "weight" : "35",
          "settings" : {
            "prefix" : "\\u003Cstrong\\u003E",
            "suffix" : "\\u003C\\/strong\\u003E",
            "excerpt" : 1,
            "excerpt_length" : "256",
            "highlight" : "always"
          }
        }
      },
      "fields" : {
        "type" : { "type" : "string" },
        "title" : { "type" : "text", "boost" : "5.0" },
        "status" : { "type" : "boolean" },
        "created" : { "type" : "date" },
        "changed" : { "type" : "date" },
        "author" : { "type" : "integer", "entity_type" : "user" },
        "comment_count" : { "type" : "integer" },
        "title_field" : { "type" : "text" },
        "field_topics" : { "type" : "list\\u003Cinteger\\u003E", "entity_type" : "taxonomy_term" },
        "field_radioactivity" : { "type" : "decimal" },
        "field_group_type" : { "type" : "integer", "entity_type" : "taxonomy_term" },
        "og_group_ref" : { "type" : "list\\u003Cinteger\\u003E", "entity_type" : "node" },
        "field_resource_type" : { "type" : "integer", "entity_type" : "taxonomy_term" },
        "field_event_type" : { "type" : "integer", "entity_type" : "taxonomy_term" },
        "field_news_type" : { "type" : "integer", "entity_type" : "taxonomy_term" },
        "field_discussion_type" : { "type" : "integer", "entity_type" : "taxonomy_term" },
        "search_api_language" : { "type" : "string" },
        "search_api_combined_1" : { "type" : "list\\u003Cinteger\\u003E" },
        "body:value" : { "type" : "text" },
        "body:summary" : { "type" : "text" },
        "og_group_ref:nid" : { "type" : "list\\u003Cinteger\\u003E" },
        "og_group_ref:title" : { "type" : "list\\u003Cstring\\u003E" },
        "og_group_ref:title_field" : { "type" : "list\\u003Cstring\\u003E" },
        "og_group_ref:field_group_type" : { "type" : "list\\u003Cinteger\\u003E", "entity_type" : "taxonomy_term" },
        "field_resource_type:name" : { "type" : "string" },
        "field_address:country" : { "type" : "string" },
        "field_address:administrative_area" : { "type" : "text" },
        "field_news_type:name" : { "type" : "string" },
        "field_discussion_type:name" : { "type" : "string" }
      }
    },
    "enabled" : "1",
    "read_only" : "0",
    "rdf_mapping" : []
  }');
  return $items;
}

/**
 * Implements hook_default_search_api_server().
 */
function uwc_search_default_search_api_server() {
  $items = array();
  $items['default'] = entity_import('search_api_server', '{
    "name" : "Solr Server (Overridden)",
    "machine_name" : "default",
    "description" : "Default Apache SOLR indexing server.",
    "class" : "search_api_solr_service",
    "options" : {
      "clean_ids_form" : [],
      "clean_ids" : false,
      "scheme" : "http",
      "host" : "66.111.111.180",
      "port" : 8984,
      "path" : "\\/solr\\/uwstage",
      "http_user" : "",
      "http_pass" : "",
      "excerpt" : 0,
      "retrieve_data" : 0,
      "highlight_data" : 0,
      "http_method" : "POST"
    },
    "enabled" : "1",
    "rdf_mapping" : []
  }');
  return $items;
}

/**
 * Implements hook_default_search_api_sort().
 */
function uwc_search_default_search_api_sort() {
  $items = array();
  $items['default_node_index__changed'] = entity_import('search_api_sort', '{
    "index_id" : "default_node_index",
    "field" : "changed",
    "name" : "Last updated",
    "enabled" : "1",
    "weight" : "0",
    "identifier" : "default_node_index__changed",
    "default_sort" : "0",
    "default_sort_no_terms" : "0",
    "default_order" : "desc",
    "options" : { "field_name" : "Date changed" },
    "rdf_mapping" : []
  }');
  $items['default_node_index__comment_count'] = entity_import('search_api_sort', '{
    "index_id" : "default_node_index",
    "field" : "comment_count",
    "name" : "Number of comments",
    "enabled" : "1",
    "weight" : "0",
    "identifier" : "default_node_index__comment_count",
    "default_sort" : "0",
    "default_sort_no_terms" : "0",
    "default_order" : "desc",
    "options" : { "field_name" : "Comment count" },
    "rdf_mapping" : []
  }');
  $items['default_node_index__created'] = entity_import('search_api_sort', '{
    "index_id" : "default_node_index",
    "field" : "created",
    "name" : "Date created",
    "enabled" : "1",
    "weight" : "0",
    "identifier" : "default_node_index__created",
    "default_sort" : "0",
    "default_sort_no_terms" : "1",
    "default_order" : "desc",
    "options" : { "field_name" : "Date created" },
    "rdf_mapping" : []
  }');
  $items['default_node_index__field_radioactivity'] = entity_import('search_api_sort', '{
    "index_id" : "default_node_index",
    "field" : "field_radioactivity",
    "name" : "Most viewed",
    "enabled" : "1",
    "weight" : "0",
    "identifier" : "default_node_index__field_radioactivity",
    "default_sort" : "0",
    "default_sort_no_terms" : "0",
    "default_order" : "desc",
    "options" : { "field_name" : "Radioactivity" },
    "rdf_mapping" : []
  }');
  $items['default_node_index__search_api_relevance'] = entity_import('search_api_sort', '{
    "index_id" : "default_node_index",
    "field" : "search_api_relevance",
    "name" : "Relevance",
    "enabled" : "1",
    "weight" : "0",
    "identifier" : "default_node_index__search_api_relevance",
    "default_sort" : "1",
    "default_sort_no_terms" : "0",
    "default_order" : "desc",
    "options" : { "field_name" : "search_api_relevance" },
    "rdf_mapping" : []
  }');
  return $items;
}
