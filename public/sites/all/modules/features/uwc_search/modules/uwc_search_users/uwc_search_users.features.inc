<?php
/**
 * @file
 * uwc_search_users.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function uwc_search_users_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "facetapi" && $api == "facetapi_defaults") {
    return array("version" => "1");
  }
  if ($module == "page_manager" && $api == "pages_default") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function uwc_search_users_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_default_search_api_index().
 */
function uwc_search_users_default_search_api_index() {
  $items = array();
  $items['user_profile_index'] = entity_import('search_api_index', '{
    "name" : "User Profile Index",
    "machine_name" : "user_profile_index",
    "description" : "Index of user profiles.",
    "server" : "default",
    "item_type" : "profile2",
    "options" : {
      "index_directly" : 1,
      "cron_limit" : "50",
      "fields" : {
        "pid" : { "type" : "integer" },
        "type" : { "type" : "string", "entity_type" : "profile2_type" },
        "created" : { "type" : "date" },
        "changed" : { "type" : "date" },
        "url" : { "type" : "uri" },
        "user" : { "type" : "integer", "entity_type" : "user" },
        "field_name_first" : { "type" : "string" },
        "field_name_last" : { "type" : "string" },
        "field_affiliations" : { "type" : "list\\u003Cinteger\\u003E", "entity_type" : "taxonomy_term" },
        "field_current_position" : { "type" : "text" },
        "field_local_united_way" : { "type" : "integer", "entity_type" : "node" },
        "field_personal_interests" : { "type" : "text" },
        "field_primary_role" : { "type" : "integer", "entity_type" : "node" },
        "field_skills" : { "type" : "list\\u003Cinteger\\u003E", "entity_type" : "taxonomy_term" },
        "field_topic_expertise" : { "type" : "list\\u003Cinteger\\u003E", "entity_type" : "taxonomy_term" },
        "search_api_language" : { "type" : "string" },
        "search_api_aggregation_1" : { "type" : "text" },
        "user:name" : { "type" : "text" },
        "user:mail" : { "type" : "text" },
        "user:og_membership" : { "type" : "list\\u003Cinteger\\u003E", "entity_type" : "og_membership" },
        "field_bio:value" : { "type" : "text" },
        "field_local_united_way:field_org_type" : { "type" : "string" },
        "field_local_united_way:field_org_bpm" : { "type" : "string" },
        "field_local_united_way:field_org_metro_size" : { "type" : "string" },
        "user:og_user_node:nid" : { "type" : "list\\u003Cinteger\\u003E" },
        "user:og_user_node:title" : { "type" : "list\\u003Cstring\\u003E" },
        "user:og_user_node:title_field" : { "type" : "list\\u003Cstring\\u003E" },
        "user:og_user_node:field_group_type" : { "type" : "list\\u003Cinteger\\u003E", "entity_type" : "taxonomy_term" },
        "field_professional_information:uwc_organization:field_org_type" : { "type" : "list\\u003Cstring\\u003E" },
        "field_local_united_way:field_address:country" : { "type" : "string" },
        "field_local_united_way:field_address:administrative_area" : { "type" : "string" },
        "field_local_united_way:field_address:locality" : { "type" : "string" },
        "field_local_united_way:field_address:postal_code" : { "type" : "string" },
        "field_professional_information:uwc_organization:field_address:country" : { "type" : "list\\u003Cstring\\u003E" },
        "field_professional_information:uwc_organization:field_address:administrative_area" : { "type" : "list\\u003Cstring\\u003E" }
      },
      "data_alter_callbacks" : {
        "search_api_alter_bundle_filter" : {
          "status" : 0,
          "weight" : "-10",
          "settings" : { "default" : "1", "bundles" : [] }
        },
        "search_api_alter_add_viewed_entity" : { "status" : 0, "weight" : "0", "settings" : { "mode" : "account" } },
        "search_api_alter_add_url" : { "status" : 0, "weight" : "0", "settings" : [] },
        "search_api_alter_add_aggregation" : {
          "status" : 1,
          "weight" : "0",
          "settings" : { "fields" : { "search_api_aggregation_1" : {
                "name" : "Full Name",
                "type" : "fulltext",
                "fields" : [ "field_name_first", "field_name_last" ],
                "description" : "A Fulltext aggregation of the following fields: First Name, Last Name."
              }
            }
          }
        }
      },
      "processors" : {
        "search_api_case_ignore" : {
          "status" : 1,
          "weight" : "0",
          "settings" : { "fields" : {
              "field_bio" : true,
              "field_name_first" : true,
              "field_name_last" : true,
              "user:name" : true,
              "user:mail" : true
            }
          }
        },
        "search_api_html_filter" : {
          "status" : 0,
          "weight" : "10",
          "settings" : {
            "fields" : { "field_bio" : true, "user:name" : true, "user:mail" : true },
            "title" : 0,
            "alt" : 1,
            "tags" : "h1 = 5\\r\\nh2 = 3\\r\\nh3 = 2\\r\\nstrong = 2\\r\\nb = 2\\r\\nem = 1.5\\r\\nu = 1.5"
          }
        },
        "search_api_tokenizer" : {
          "status" : 1,
          "weight" : "20",
          "settings" : {
            "fields" : { "field_bio" : true },
            "spaces" : "[^[:alnum:]]",
            "ignorable" : "[\\u0027]"
          }
        },
        "search_api_stopwords" : {
          "status" : 0,
          "weight" : "30",
          "settings" : {
            "fields" : { "field_bio" : true, "user:name" : true, "user:mail" : true },
            "file" : "",
            "stopwords" : "but\\r\\ndid\\r\\nthe this that those\\r\\netc"
          }
        },
        "search_api_highlighting" : {
          "status" : 0,
          "weight" : "35",
          "settings" : {
            "prefix" : "\\u003Cstrong\\u003E",
            "suffix" : "\\u003C\\/strong\\u003E",
            "excerpt" : 1,
            "excerpt_length" : "256",
            "highlight" : "always"
          }
        }
      }
    },
    "enabled" : "1",
    "read_only" : "0",
    "rdf_mapping" : []
  }');
  return $items;
}

/**
 * Implements hook_default_search_api_sort().
 */
function uwc_search_users_default_search_api_sort() {
  $items = array();
  $items['user_profile_index__field_name_first'] = entity_import('search_api_sort', '{
    "index_id" : "user_profile_index",
    "field" : "field_name_first",
    "name" : "First Name",
    "enabled" : "1",
    "weight" : "0",
    "identifier" : "user_profile_index__field_name_first",
    "default_sort" : "0",
    "default_sort_no_terms" : "0",
    "default_order" : "asc",
    "options" : { "field_name" : "First Name" },
    "rdf_mapping" : []
  }');
  $items['user_profile_index__field_name_last'] = entity_import('search_api_sort', '{
    "index_id" : "user_profile_index",
    "field" : "field_name_last",
    "name" : "Last Name",
    "enabled" : "1",
    "weight" : "0",
    "identifier" : "user_profile_index__field_name_last",
    "default_sort" : "1",
    "default_sort_no_terms" : "0",
    "default_order" : "asc",
    "options" : { "field_name" : "Last Name" },
    "rdf_mapping" : []
  }');
  return $items;
}
