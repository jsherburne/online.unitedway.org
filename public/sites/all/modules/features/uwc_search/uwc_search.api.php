<?php
/**
 * @file
 * Hooks provided by UWC: Communities of Practice module.
 */

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Define various search directories.
 *
 * An array of available searches.
 */
function hook_uwc_search_type_info() {
  $search = array();

  $search['content'] = array(
    // label: to be used in search dropdown
    'label' => t('Content'),
    // path: where the search should be directed
    'path' => 'search/content',
    // keyword: $_GET param name to be used to pass the search string to the new page
    'keyword' => 'keyword',
  );

  return $search;
}

/**
 * @} End of "addtogroup hooks".
 */
