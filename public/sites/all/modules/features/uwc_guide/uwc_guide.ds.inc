<?php
/**
 * @file
 * uwc_guide.ds.inc
 */

/**
 * Implements hook_ds_field_settings_info().
 */
function uwc_guide_ds_field_settings_info() {
  $export = array();

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'node|guide|compact_teaser';
  $ds_fieldsetting->entity_type = 'node';
  $ds_fieldsetting->bundle = 'guide';
  $ds_fieldsetting->view_mode = 'compact_teaser';
  $ds_fieldsetting->settings = array(
    'title' => array(
      'weight' => '2',
      'label' => 'hidden',
      'format' => 'default',
      'formatter_settings' => array(
        'link' => '1',
        'wrapper' => 'h3',
        'class' => '',
        'ft' => array(),
        'field_delimiter' => '',
      ),
    ),
    'author' => array(
      'weight' => '1',
      'label' => 'hidden',
      'format' => 'author_linked',
    ),
    'post_date' => array(
      'weight' => '0',
      'label' => 'hidden',
      'format' => 'ds_post_date_rich_snippets_published_date',
      'formatter_settings' => array(
        'ft' => array(
          'func' => 'theme_field',
        ),
        'field_delimiter' => '',
      ),
    ),
  );
  $export['node|guide|compact_teaser'] = $ds_fieldsetting;

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'node|guide|default';
  $ds_fieldsetting->entity_type = 'node';
  $ds_fieldsetting->bundle = 'guide';
  $ds_fieldsetting->view_mode = 'default';
  $ds_fieldsetting->settings = array(
    'title' => array(
      'weight' => '2',
      'label' => 'hidden',
      'format' => 'default',
      'formatter_settings' => array(
        'link' => '0',
        'wrapper' => 'H3',
        'class' => '',
        'ft' => array(),
        'field_delimiter' => '',
      ),
    ),
    'links' => array(
      'weight' => '8',
      'label' => 'hidden',
      'format' => 'default',
    ),
    'comments' => array(
      'weight' => '9',
      'label' => 'hidden',
      'format' => 'default',
    ),
    'author' => array(
      'weight' => '1',
      'label' => 'hidden',
      'format' => 'author_linked',
    ),
    'post_date' => array(
      'weight' => '0',
      'label' => 'hidden',
      'format' => 'ds_post_date_rich_snippets_published_date',
      'formatter_settings' => array(
        'ft' => array(),
        'field_delimiter' => '',
      ),
    ),
  );
  $export['node|guide|default'] = $ds_fieldsetting;

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'node|guide|full';
  $ds_fieldsetting->entity_type = 'node';
  $ds_fieldsetting->bundle = 'guide';
  $ds_fieldsetting->view_mode = 'full';
  $ds_fieldsetting->settings = array(
    'private_group_flag' => array(
      'weight' => '1',
      'label' => 'hidden',
      'format' => 'default',
    ),
    'title' => array(
      'weight' => '4',
      'label' => 'hidden',
      'format' => 'default',
      'formatter_settings' => array(
        'link' => '0',
        'wrapper' => 'h2',
        'class' => '',
        'ft' => array(),
        'field_delimiter' => '',
      ),
    ),
    'links' => array(
      'weight' => '12',
      'label' => 'hidden',
      'format' => 'default',
    ),
    'comments' => array(
      'weight' => '11',
      'label' => 'hidden',
      'format' => 'default',
    ),
    'author' => array(
      'weight' => '3',
      'label' => 'hidden',
      'format' => 'author_linked',
    ),
    'post_date' => array(
      'weight' => '2',
      'label' => 'hidden',
      'format' => 'ds_post_date_rich_snippets_published_date',
    ),
    'flag_commons_follow_node' => array(
      'weight' => '6',
      'label' => 'hidden',
      'format' => 'default',
    ),
    'flag_breakfast_recommendation' => array(
      'weight' => '7',
      'label' => 'hidden',
      'format' => 'default',
    ),
    'node_type' => array(
      'weight' => '0',
      'label' => 'hidden',
      'format' => 'default',
      'formatter_settings' => array(
        'ft' => array(
          'classes' => 'type',
        ),
        'field_delimiter' => '',
      ),
    ),
    'rate_integration_1' => array(
      'weight' => '5',
      'label' => 'hidden',
      'format' => 'default',
    ),
    'field_related_guides' => array(
      'formatter_settings' => array(
        'ft' => array(
          'func' => 'theme_ds_field_expert',
          'lb-col' => TRUE,
          'ow' => TRUE,
          'ow-el' => 'div',
          'ow-cl' => 'field-name-field-related-guides',
          'ow-def-cl' => FALSE,
          'ow-at' => '',
          'ow-def-at' => FALSE,
          'fi' => TRUE,
          'fi-el' => 'div',
          'fi-cl' => 'field-related-guide',
          'fi-at' => '',
          'fi-def-at' => FALSE,
          'fi-odd-even' => FALSE,
        ),
      ),
    ),
    'og_group_ref' => array(
      'formatter_settings' => array(
        'ft' => array(
          'lb' => 'Communities of practice',
        ),
      ),
    ),
  );
  $export['node|guide|full'] = $ds_fieldsetting;

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'node|guide|search_result';
  $ds_fieldsetting->entity_type = 'node';
  $ds_fieldsetting->bundle = 'guide';
  $ds_fieldsetting->view_mode = 'search_result';
  $ds_fieldsetting->settings = array(
    'private_group_flag' => array(
      'weight' => '1',
      'label' => 'hidden',
      'format' => 'default',
    ),
    'title' => array(
      'weight' => '4',
      'label' => 'hidden',
      'format' => 'default',
      'formatter_settings' => array(
        'link' => '0',
        'wrapper' => 'h3',
        'class' => '',
        'ft' => array(),
        'field_delimiter' => '',
      ),
    ),
    'links' => array(
      'weight' => '7',
      'label' => 'hidden',
      'format' => 'default',
    ),
    'author' => array(
      'weight' => '3',
      'label' => 'hidden',
      'format' => 'author_linked',
    ),
    'post_date' => array(
      'weight' => '2',
      'label' => 'hidden',
      'format' => 'ds_post_date_rich_snippets_published_date',
    ),
    'node_type' => array(
      'weight' => '0',
      'label' => 'hidden',
      'format' => 'default',
      'formatter_settings' => array(
        'ft' => array(
          'func' => 'theme_field',
        ),
        'field_delimiter' => '',
      ),
    ),
  );
  $export['node|guide|search_result'] = $ds_fieldsetting;

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'node|guide|teaser';
  $ds_fieldsetting->entity_type = 'node';
  $ds_fieldsetting->bundle = 'guide';
  $ds_fieldsetting->view_mode = 'teaser';
  $ds_fieldsetting->settings = array(
    'private_group_flag' => array(
      'weight' => '3',
      'label' => 'hidden',
      'format' => 'default',
    ),
    'title' => array(
      'weight' => '4',
      'label' => 'hidden',
      'format' => 'default',
      'formatter_settings' => array(
        'link' => '1',
        'wrapper' => 'h3',
        'class' => '',
        'ft' => array(),
        'field_delimiter' => '',
      ),
    ),
    'author' => array(
      'weight' => '5',
      'label' => 'hidden',
      'format' => 'author_linked',
    ),
    'post_date' => array(
      'weight' => '1',
      'label' => 'hidden',
      'format' => 'ds_post_date_rich_snippets_published_date',
    ),
    'comment_count' => array(
      'weight' => '10',
      'label' => 'hidden',
      'format' => 'default',
    ),
    'profile2' => array(
      'weight' => '0',
      'label' => 'hidden',
      'format' => 'compact_teaser',
    ),
    'node_type' => array(
      'weight' => '2',
      'label' => 'hidden',
      'format' => 'default',
      'formatter_settings' => array(
        'ft' => array(
          'func' => 'theme_field',
          'classes' => 'type',
        ),
        'field_delimiter' => '',
      ),
    ),
    'rate_integration_1' => array(
      'weight' => '9',
      'label' => 'hidden',
      'format' => 'default',
    ),
  );
  $export['node|guide|teaser'] = $ds_fieldsetting;

  return $export;
}

/**
 * Implements hook_ds_layout_settings_info().
 */
function uwc_guide_ds_layout_settings_info() {
  $export = array();

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|guide|compact_teaser';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'guide';
  $ds_layout->view_mode = 'compact_teaser';
  $ds_layout->layout = 'ds_1col';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'post_date',
        1 => 'author',
        2 => 'title',
      ),
    ),
    'fields' => array(
      'post_date' => 'ds_content',
      'author' => 'ds_content',
      'title' => 'ds_content',
    ),
    'classes' => array(
      'layout_class' => array(
        '' => '',
      ),
    ),
    'wrappers' => array(
      'ds_content' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
  );
  $export['node|guide|compact_teaser'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|guide|default';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'guide';
  $ds_layout->view_mode = 'default';
  $ds_layout->layout = 'ds_1col';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'post_date',
        1 => 'body',
        2 => 'author',
        3 => 'field_topics',
        4 => 'title',
        5 => 'field_related_guides',
        6 => 'field_subject_matter_experts',
        7 => 'field_content_cluster',
        8 => 'links',
        9 => 'comments',
      ),
    ),
    'fields' => array(
      'post_date' => 'ds_content',
      'body' => 'ds_content',
      'author' => 'ds_content',
      'field_topics' => 'ds_content',
      'title' => 'ds_content',
      'field_related_guides' => 'ds_content',
      'field_subject_matter_experts' => 'ds_content',
      'field_content_cluster' => 'ds_content',
      'links' => 'ds_content',
      'comments' => 'ds_content',
    ),
    'classes' => array(
      'layout_class' => array(
        '' => '',
      ),
    ),
    'wrappers' => array(
      'ds_content' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
  );
  $export['node|guide|default'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|guide|full';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'guide';
  $ds_layout->view_mode = 'full';
  $ds_layout->layout = 'panels-sidebar';
  $ds_layout->settings = array(
    'regions' => array(
      'preface' => array(
        0 => 'node_type',
        1 => 'private_group_flag',
        2 => 'post_date',
        3 => 'author',
        4 => 'title',
      ),
      'main' => array(
        5 => 'rate_integration_1',
        6 => 'flag_commons_follow_node',
        7 => 'flag_breakfast_recommendation',
        8 => 'group_node_tax',
        9 => 'body',
        10 => 'field_content_cluster',
        11 => 'comments',
        15 => 'og_group_ref',
        16 => 'field_topics',
      ),
      'sidebar' => array(
        12 => 'links',
        13 => 'uwc_experts_uwc_subject_matter_experts_eva',
        14 => 'field_related_guides',
      ),
    ),
    'fields' => array(
      'node_type' => 'preface',
      'private_group_flag' => 'preface',
      'post_date' => 'preface',
      'author' => 'preface',
      'title' => 'preface',
      'rate_integration_1' => 'main',
      'flag_commons_follow_node' => 'main',
      'flag_breakfast_recommendation' => 'main',
      'group_node_tax' => 'main',
      'body' => 'main',
      'field_content_cluster' => 'main',
      'comments' => 'main',
      'links' => 'sidebar',
      'uwc_experts_uwc_subject_matter_experts_eva' => 'sidebar',
      'field_related_guides' => 'sidebar',
      'og_group_ref' => 'main',
      'field_topics' => 'main',
    ),
    'classes' => array(
      'layout_class' => array(
        '' => '',
      ),
    ),
    'wrappers' => array(
      'preface' => 'div',
      'main' => 'div',
      'sidebar' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
  );
  $export['node|guide|full'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|guide|search_result';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'guide';
  $ds_layout->view_mode = 'search_result';
  $ds_layout->layout = 'ds_1col';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'node_type',
        1 => 'private_group_flag',
        2 => 'post_date',
        3 => 'author',
        4 => 'title',
        5 => 'body',
        6 => 'field_topics',
        7 => 'links',
      ),
    ),
    'fields' => array(
      'node_type' => 'ds_content',
      'private_group_flag' => 'ds_content',
      'post_date' => 'ds_content',
      'author' => 'ds_content',
      'title' => 'ds_content',
      'body' => 'ds_content',
      'field_topics' => 'ds_content',
      'links' => 'ds_content',
    ),
    'classes' => array(
      'layout_class' => array(
        '' => '',
      ),
    ),
    'wrappers' => array(
      'ds_content' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
  );
  $export['node|guide|search_result'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|guide|teaser';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'guide';
  $ds_layout->view_mode = 'teaser';
  $ds_layout->layout = 'ds_2col';
  $ds_layout->settings = array(
    'regions' => array(
      'left' => array(
        0 => 'profile2',
      ),
      'right' => array(
        1 => 'post_date',
        2 => 'node_type',
        3 => 'private_group_flag',
        4 => 'title',
        5 => 'author',
        6 => 'body',
        7 => 'field_topics',
        8 => 'group_node_actions',
        9 => 'rate_integration_1',
        10 => 'comment_count',
      ),
    ),
    'fields' => array(
      'profile2' => 'left',
      'post_date' => 'right',
      'node_type' => 'right',
      'private_group_flag' => 'right',
      'title' => 'right',
      'author' => 'right',
      'body' => 'right',
      'field_topics' => 'right',
      'group_node_actions' => 'right',
      'rate_integration_1' => 'right',
      'comment_count' => 'right',
    ),
    'classes' => array(
      'layout_class' => array(
        '' => '',
      ),
      'left' => array(
        'teaser-left' => 'teaser-left',
      ),
      'right' => array(
        'teaser-right' => 'teaser-right',
      ),
    ),
    'wrappers' => array(
      'left' => 'div',
      'right' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
  );
  $export['node|guide|teaser'] = $ds_layout;

  return $export;
}
