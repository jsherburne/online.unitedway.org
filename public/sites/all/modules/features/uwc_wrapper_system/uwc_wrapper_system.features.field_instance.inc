<?php
/**
 * @file
 * uwc_wrapper_system.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function uwc_wrapper_system_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance: 'node-wrapped_system-field_iframe_height'
  $field_instances['node-wrapped_system-field_iframe_height'] = array(
    'bundle' => 'wrapped_system',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'If left blank, the iFrame will be 640px tall.',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'number',
        'settings' => array(
          'decimal_separator' => '.',
          'field_delimiter' => '',
          'prefix_suffix' => TRUE,
          'scale' => 0,
          'thousand_separator' => ' ',
        ),
        'type' => 'number_integer',
        'weight' => 14,
      ),
      'full' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'ds_extras_field_template' => '',
    'entity_type' => 'node',
    'field_name' => 'field_iframe_height',
    'label' => 'Iframe Height',
    'required' => 0,
    'settings' => array(
      'max' => '',
      'min' => '',
      'prefix' => '',
      'suffix' => ' px',
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 0,
      'module' => 'number',
      'settings' => array(),
      'type' => 'number',
      'weight' => 11,
    ),
  );

  // Exported field_instance: 'node-wrapped_system-field_wrapped_system_url'
  $field_instances['node-wrapped_system-field_wrapped_system_url'] = array(
    'bundle' => 'wrapped_system',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'link',
        'settings' => array(
          'field_delimiter' => '',
        ),
        'type' => 'link_default',
        'weight' => 13,
      ),
      'full' => array(
        'label' => 'hidden',
        'module' => 'link_iframe_formatter',
        'settings' => array(
          'field_delimiter' => '',
          'height' => 640,
          'width' => 960,
        ),
        'type' => 'link_iframe_formatter_iframe',
        'weight' => 0,
      ),
      'teaser' => array(
        'label' => 'above',
        'module' => 'link',
        'settings' => array(
          'field_delimiter' => '',
        ),
        'type' => 'link_default',
        'weight' => 1,
      ),
    ),
    'ds_extras_field_template' => 'theme_ds_field_minimal',
    'entity_type' => 'node',
    'field_name' => 'field_wrapped_system_url',
    'label' => 'URL',
    'required' => 1,
    'settings' => array(
      'attributes' => array(
        'class' => '',
        'configurable_title' => 0,
        'rel' => '',
        'target' => 'default',
        'title' => '',
      ),
      'display' => array(
        'url_cutoff' => 80,
      ),
      'enable_tokens' => 0,
      'rel_remove' => 'default',
      'title' => 'none',
      'title_maxlength' => 128,
      'title_value' => '',
      'url' => 0,
      'user_register_form' => FALSE,
      'validate_url' => 1,
    ),
    'widget' => array(
      'active' => 0,
      'module' => 'link',
      'settings' => array(),
      'type' => 'link_field',
      'weight' => 1,
    ),
  );
  
  // Exported field_instance: 'node-wrapped_system-title_field'
  $field_instances['node-wrapped_system-title_field'] = array(
      'bundle' => 'wrapped_system',
      'default_value' => NULL,
      'deleted' => 0,
      'description' => '',
      'display' => array(
          'default' => array(
              'label' => 'above',
              'settings' => array(),
              'type' => 'hidden',
              'weight' => 12,
          ),
          'full' => array(
              'label' => 'above',
              'settings' => array(),
              'type' => 'hidden',
              'weight' => 0,
          ),
          'teaser' => array(
              'label' => 'above',
              'settings' => array(),
              'type' => 'hidden',
              'weight' => 0,
          ),
      ),
      'display_in_partial_form' => FALSE,
      'entity_type' => 'node',
      'field_name' => 'title_field',
      'label' => 'Title',
      'required' => 1,
      'settings' => array(
          'hide_label' => array(
              'entity' => 0,
              'page' => 0,
          ),
          'text_processing' => 0,
          'user_register_form' => FALSE,
      ),
      'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
      'size' => 60,
      ),
      'type' => 'text_textfield',
      'weight' => -5,
      ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('If left blank, the iFrame will be 640px tall.');
  t('Iframe Height');
  t('Title');
  t('URL');

  return $field_instances;
}
