<?php
/**
 * @file
 * uwc_wrapper_system.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function uwc_wrapper_system_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "ds" && $api == "ds") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function uwc_wrapper_system_node_info() {
  $items = array(
    'wrapped_system' => array(
      'name' => t('Wrapped System'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
