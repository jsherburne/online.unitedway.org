<?php
/**
 * @file
 * uwc_wrapper_system.ds.inc
 */

/**
 * Implements hook_ds_field_settings_info().
 */
function uwc_wrapper_system_ds_field_settings_info() {
  $export = array();

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'node|wrapped_system|full';
  $ds_fieldsetting->entity_type = 'node';
  $ds_fieldsetting->bundle = 'wrapped_system';
  $ds_fieldsetting->view_mode = 'full';
  $ds_fieldsetting->settings = array(
    'comments' => array(
      'weight' => '2',
      'label' => 'hidden',
      'format' => 'default',
    ),
    'flag_breakfast_recommendation' => array(
      'weight' => '1',
      'label' => 'hidden',
      'format' => 'default',
    ),
  );
  $export['node|wrapped_system|full'] = $ds_fieldsetting;

  return $export;
}

/**
 * Implements hook_ds_layout_settings_info().
 */
function uwc_wrapper_system_ds_layout_settings_info() {
  $export = array();

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|wrapped_system|full';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'wrapped_system';
  $ds_layout->view_mode = 'full';
  $ds_layout->layout = 'ds_1col';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'field_wrapped_system_url',
        1 => 'flag_breakfast_recommendation',
        2 => 'comments',
      ),
    ),
    'fields' => array(
      'field_wrapped_system_url' => 'ds_content',
      'flag_breakfast_recommendation' => 'ds_content',
      'comments' => 'ds_content',
    ),
    'classes' => array(
      'layout_class' => array(
        '' => '',
      ),
    ),
    'wrappers' => array(
      'ds_content' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
  );
  $export['node|wrapped_system|full'] = $ds_layout;

  return $export;
}
