<?php
/**
 * @file
 * System Analytics woopra.
 */

/**
 * Implements hook_help().
 */
function uwc_woopra_analytics_help($path, $arg) {
  switch ($path) {
    case 'admin/help#uwc-woopra-analytics':
      return '<p>' . t('UWC Woopra documentation.') . '</p>';
      break;

    case 'admin/config/system/uwc-woopra-analytics':
      return '<p>' . t('UWC Woopra Analytics Control Panel.') . '</p>';

      break;
  }
}

/**
 * Implements hook_menu().
 */
function uwc_woopra_analytics_menu() {
  $items = array();

  $items['admin/config/system/uwc-woopra-analytics'] = array(
    'title'            => 'Woopra Analytics',
    'description'      => 'Control panel setup',
    'page callback'    => 'drupal_get_form',
    'page arguments'   => array('uwc_woopra_analytics_control_panel_form'),
    'access arguments' => array('admin woopra analytics control panel'),
    'file'             => 'uwc_woopra_analytics.admin.inc',
  );

  return $items;
}

/**
 * Implements hook_permission().
 */
function uwc_woopra_analytics_permission() {
  return array(
    'admin woopra analytics control panel' => array(
      'title'       => t('Administer woopra analytics settings'),
      'description' => t('Give access to the Control Panel area.')
    ),
  );
}

/**
 * Implements hook_page_alter().
 */
function uwc_woopra_analytics_page_alter(&$page) {
  global $user;

  if (!path_is_admin(current_path()) && variable_get('uwc_woopra_analytics_switch', 0)) {
    //set default values for variables used in the woopra code
    $domain = variable_get('uwc_woopra_analytics_domain', $_SERVER['SERVER_NAME']);
    $woopra_identity = '';
    $woopra_click_handler = '';
    $woopra_group_info = '';
    $woopra_topic_info = '';    
    
    //gets profile information if the user is logged in
    if (0 < $user->uid) {
      
      //create identity template
      $identity = array(
        'name' => '',
        'email' => '',
        'company' => '',
      );
      //parse through the user profile to get profile information
      if (module_exists('profile2')) {
        $profile = profile2_load_by_user($user, 'main');
        //load all the name related fields 
        $title = isset($profile->field_personal_title[LANGUAGE_NONE][0]['value']) ? $profile->field_personal_title[LANGUAGE_NONE][0]['value'] : '' ;
        $firstname = isset($profile->field_name_first[LANGUAGE_NONE][0]['value']) ? $profile->field_name_first[LANGUAGE_NONE][0]['value'] : '' ;
        $lastname = isset($profile->field_name_last[LANGUAGE_NONE][0]['value']) ? $profile->field_name_last[LANGUAGE_NONE][0]['value'] : '';
        $suffix = isset($profile->field_name_suffix[LANGUAGE_NONE][0]['value']) ? $profile->field_name_suffix[LANGUAGE_NONE][0]['value'] : '';
        
        $identity['name'] = trim($title . ' ' . $firstname . ' ' . $lastname . ' ' . $suffix);
        $identity['email'] = $user->mail;
        if (isset($profile->field_local_united_way[LANGUAGE_NONE][0]['target_id']) &&
            $company = node_load($profile->field_local_united_way[LANGUAGE_NONE][0]['target_id'])) {
          $identity['company'] = $company->title;
        }
      }
      
      //create the woopra idenity block
      $woopra_identity = <<<EOF
//Get me the user info. It will be {$identity['email']}, {$identity['name']}, {$identity['company']}.
woopra.identify({
 	email:  '{$identity['email']}',
	name:	'{$identity['name']}',
	company: '{$identity['company']}',
	domain: '{$domain}'
});      
      
EOF;
      //create click_handler block if there is an identity set-up
      $woopra_click_handler = <<<EOF


function uwc_makeClickHandler(i) {
    "use strict";
    return function () {

        woopra.track("likeclick", {
            username: '{$identity['email']}',
            name: '{$identity['name']}',
            company: '{$identity['company']}',
            rateid: document.URL
        });
        this.innerHTML = i;
    };
}

var elems = document.getElementsByClassName("rate-button"), i;
for (i = 0; i < elems.length; i++) {
    elems[i].addEventListener("click", uwc_makeClickHandler(i));
}      
EOF;
    }//end of if(0 < $user->uid)

    //check if we are on a node page
    if ('node' == arg(0) && is_numeric(arg(1)) && $node = node_load(arg(1))) {
      
      //load group information for node
      $node_info = array(
        'path' => 'node/' . $node->nid,
        'alias' => url('node/' . $node->nid),
        'nid' => $node->nid,
        'nodetitle' => $node->title,
        'groups' => array(),
        'topics' => array(),   
      );
      if (isset ($node->og_group_ref[LANGUAGE_NONE])) {
        foreach ($node->og_group_ref[LANGUAGE_NONE] as $index => $item){
          if (isset($item['target_id']) && $group = node_load($item['target_id'])) {
            //load the group info into an array then convert it to json
            $node_info['groups'][$group->nid]['path'] = $node_info['path'];
            $node_info['groups'][$group->nid]['alias'] = $node_info['alias'];
            $node_info['groups'][$group->nid]['nid'] = $node_info['nid'];
            $node_info['groups'][$group->nid]['nodetitle'] = $node_info['nodetitle'];
            $node_info['groups'][$group->nid]['groupID'] = $group->nid;
            $node_info['groups'][$group->nid]['grouptitle'] = $group->title;
            $node_info['groups'][$group->nid]['sponsor'] = '';
            if (isset($group->field_group_sponsor_emt_[LANGUAGE_NONE][0]['value'])) { 
              $node_info['groups'][$group->nid]['sponsor'] = $group->field_group_sponsor_emt_[LANGUAGE_NONE][0]['value'];
            }
            $node_info['groups'][$group->nid] = json_encode($node_info['groups'][$group->nid], JSON_FORCE_OBJECT);
            
            //modify woopra group info block
            $woopra_group_info .= <<<EOF

//Get me group information for group {$group->nid}.
woopra.track('grouptitle',{$node_info['groups'][$group->nid]});

EOF;
          }  
        }        
      }//end of if (isset ($node->og_group_ref[LANGUAGE_NONE]))
      
      //load topic information for node
      if (isset($node->field_topics[LANGUAGE_NONE])) {
        foreach ($node->field_topics[LANGUAGE_NONE] as $index => $item){
          if (isset($item['tid']) && $topic = taxonomy_term_load($item['tid'])) {
            //load the topic info into an array then convert it to json
            $node_info['topics'][$topic->tid]['path'] = $node_info['path'];
            $node_info['topics'][$topic->tid]['alias'] = $node_info['alias'];
            $node_info['topics'][$topic->tid]['nid'] = $node_info['nid'];
            $node_info['topics'][$topic->tid]['nodetitle'] = $node_info['nodetitle'];
            $node_info['topics'][$topic->tid]['topicid'] = $topic->tid;
            $node_info['topics'][$topic->tid]['topic'] = $topic->name;
            $node_info['topics'][$topic->tid] = json_encode($node_info['topics'][$topic->tid], JSON_FORCE_OBJECT);
            
            //modify woopra topic info block
            $woopra_topic_info .= <<<EOF
        
//Get me topic information for topic {$topic->tid}.
woopra.track('topicarea',{$node_info['topics'][$topic->tid]});
        
EOF;
          }
        }//end of if (isset($node->field_topics[LANGUAGE_NONE]))
      }//end of if node page 
    }

    $script = <<<EOF
<!-- Start of Woopra Code -->
(function(){
    var t,i,e,n=window,o=document,a=arguments,s="script",r=["config","track","identify","visit","push","call"],c=function(){var t,i=this;for(i._e=[],t=0;r.length>t;t++)(function(t){i[t]=function(){return i._e.push([t].concat(Array.prototype.slice.call(arguments,0))),i}})(r[t])};for(n._w=n._w||{},t=0;a.length>t;t++)n._w[a[t]]=n[a[t]]=n[a[t]]||new c;i=o.createElement(s),i.async=1,i.src="//static.woopra.com/js/w.js",e=o.getElementsByTagName(s)[0],e.parentNode.insertBefore(i,e)
})("woopra");

/**
 * Get me the domain.
 */
woopra.config({
    domain: '{$domain}'
});

/**
 * Get me the user info.
 */
$woopra_identity

/**
 * Get me group information.
 */
$woopra_group_info

/**
 * Get me topic information.
 */
$woopra_topic_info

$woopra_click_handler

woopra.track();
<!-- End of Woopra Code -->
EOF;

    drupal_add_js($script, array('scope' => 'footer', 'type' => 'inline'));
  }
}

