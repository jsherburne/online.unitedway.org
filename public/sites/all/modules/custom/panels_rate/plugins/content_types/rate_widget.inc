<?php

// @file: Provides a Panels Pane to display widgets from the Rate module.

$plugin = array(
  'single' => TRUE,
  'title' => t('Rate widget'),
  'description' => t('Rate widget for a particular node.'),
  'category' => t('Miscellaneous'),
  'edit form' => 'panels_rate_rate_widget',
  'render callback' => 'panels_rate_rate_widget_render',
  'admin info' => 'panels_rate_rate_widget_info',
  'defaults' => array(),
  'all contexts' => TRUE,
);

/**
 * Edit form.
 */
function panels_rate_rate_widget($form, &$form_state) {
  $conf = $form_state['conf'];

  $form['node'] = array(
    '#type' => 'textfield',
    '#title' => t('Node'),
    '#description' => t('The node ID that we are rating (from Panels context).'),
    '#default_value' => $conf['node'],
  );

  $widgets = variable_get(RATE_VAR_WIDGETS, array());
  $options = array();
  foreach ($widgets as $id => $widget) {
    $options[$widget->name] = $widget->title;
  }

  $form['widget'] = array(
    '#type' => 'select',
    '#title' => t('Widget'),
    '#description' => t('The Widget to display'),
    '#options' => $options,
    '#default_value' => $conf['widget'],
  );

  return $form;
}

/**
 * Edit form submit function.
 */
function panels_rate_rate_widget_submit($form, &$form_state) {
  $form_state['conf'] = $form_state['values'];
}

/**
 * Render function.
 */
function panels_rate_rate_widget_render($subtype, $conf, $args, $contexts) {
  if (empty($contexts)) {
    return;
  }
  // Make sure we have the full node.
  $node = ctools_context_keyword_substitute($conf['node'], array(), $contexts);
  if (is_numeric($node)) {
    $node = node_load($node);
  }
  // Otherwise return empty.
  else {
    return;
  }

  // Render as a block.
  $block = new stdClass();
  $block->module = 'panels_rate';
  $block->delta = 'panels-rate-' . str_replace('-', '_', $conf['widget']);
  $block->content = rate_embed($node, $conf['widget']);
  return $block;
}

