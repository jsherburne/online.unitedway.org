<?php

// @file: Provides a Panels Pane to display links from the Flag module.

$plugin = array(
  'single' => TRUE,
  'title' => t('Flag link'),
  'description' => t('Flag link for a particular node.'),
  'category' => t('Miscellaneous'),
  'edit form' => 'panels_flags_flag_link',
  'render callback' => 'panels_flags_flag_link_render',
  'admin info' => 'panels_flags_flag_link_info',
  'defaults' => array(),
  'all contexts' => TRUE,
);

/**
 * Edit form.
 */
function panels_flags_flag_link($form, &$form_state) {
  $conf = $form_state['conf'];

  $form['node'] = array(
    '#type' => 'textfield',
    '#title' => t('Node'),
    '#description' => t('The node ID that we are flagging (from Panels context).'),
    '#default_value' => $conf['node'],
  );

  $flags = flag_get_flags();
  $options = array();
  foreach ($flags as $id => $flag) {
    $options[$flag->name] = $flag->name;
  }

  $form['flag'] = array(
    '#type' => 'select',
    '#title' => t('Flag'),
    '#description' => t('The flag link to display'),
    '#options' => $options,
    '#default_value' => $conf['flag'],
  );

  return $form;
}

/**
 * Edit form submit function.
 */
function panels_flags_flag_link_submit($form, &$form_state) {
  $form_state['conf'] = $form_state['values'];
}

/**
 * Render function.
 */
function panels_flags_flag_link_render($subtype, $conf, $args, $contexts) {
  if (empty($contexts)) {
    return;
  }
  // Make sure we have a numeric node ID.
  $node = ctools_context_keyword_substitute($conf['node'], array(), $contexts);
  if (!is_numeric($node)) {
    return;
  }

  // Render as a block.
  $block = new stdClass();
  $block->module = 'panels_flags';
  $block->delta = 'panels-flags-' . str_replace('-', '_', $conf['flag']);
  $block->content = flag_create_link($conf['flag'], $node);
  return $block;
}

