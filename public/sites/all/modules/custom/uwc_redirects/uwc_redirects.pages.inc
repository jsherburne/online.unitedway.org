<?php

/**
 * Redirects old content_id URLs to their new destination.
 */
function uwc_redirects_by_legacy_id($prefix, $old_id) {
  $nid = null;
  $feeds = array('uw_resource_import', 'uw_news_importer', 'uw_event_importer');
  if (is_numeric($old_id) && !empty($prefix)){
    // Lookup NID from GUID.
    $nid = db_query("SELECT entity_id
      FROM {feeds_item}
      WHERE guid = :old_id AND id IN (:feeds)", array(
        ':old_id' => strtoupper($prefix . $old_id), ':feeds' => $feeds,
      ))->fetchField();
  }
  // If not found, return 404.
  if (empty($nid)) {
    return MENU_NOT_FOUND;
  }
  
  //create a redirect so we can skip this query in the future
  if (module_exists('redirect')) {
    $redirect = new stdClass();
    redirect_object_prepare(
      $redirect,
      array(
        'source' => 'uwc_redirects/' . $prefix . '/' . $old_id,
        'source_options' => array(),
        'redirect' => 'node/' . $nid,
        'redirect_options' => array(),
        'language' => LANGUAGE_NONE, 
      )  
    );
    redirect_save($redirect);
  }

  // Redirect permanently.
  drupal_goto("node/$nid", array(), 301);
}