<?php
/**
 * @file
 * Display Suite slat layout configuration.
 */

function ds_profile() {
  return array(
    'label' => t('Profile'),
    'regions' => array(
      'header' => t('Header'),
      'aside' => t('Aside'),
      'main_content' => t('Content Main'),
      'left_content' => t('Content Left'),
      'right_content' => t('Content Right'),
      'footer' => t('Footer'),
    ),
  );
}
