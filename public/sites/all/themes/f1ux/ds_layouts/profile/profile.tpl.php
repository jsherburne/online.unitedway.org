<?php
/**
 * @file
 * profile.tpl.php
 */
?>
<div class="l-profile profile-top <?php print $classes; ?>"<?php print $attributes; ?>>
  <?php if ($header && $header != "&nbsp;"): ?>
    <div class="l-profile__header profile-preface"><?php print $header; ?></div>
  <?php endif; ?>
  <?php if (($aside && $aside != "&nbsp;") && ($main_content && $main_content != "&nbsp;")): ?>
  	<div class="l-profile__content-wrapper profile-content-wrapper">
  <?php endif; ?>

  <?php if ($aside && $aside != "&nbsp;"): ?>
    <div class="l-profile__aside profile-photo-box"><?php print $aside; ?></div>
  <?php endif; ?>

  <?php if ($main_content && $main_content != "&nbsp;"): ?>
    <div class="l-profile__content profile-description">
      <?php print $main_content; ?>

      <?php if ($left_content && $left_content != "&nbsp;"): ?>
        <div class="l-profile__content-left profile-description-left">
          <?php print $left_content; ?>
        </div>
      <?php endif; ?>

      <?php if ($right_content && $right_content != "&nbsp;"): ?>
        <div class="l-profile__content-right profile-description-right">
          <?php print $right_content; ?>
        </div>
      <?php endif; ?>
    </div>
  <?php endif; ?>

  <?php if (($aside && $aside != "&nbsp;") && ($main_content && $main_content != "&nbsp;")): ?>
  	</div>
  <?php endif; ?>

  <?php if ($footer && $footer != "&nbsp;"): ?>
    <div class="l-profile__footer profile-postscript"><?php print $footer; ?></div>
  <?php endif; ?>
</div>
