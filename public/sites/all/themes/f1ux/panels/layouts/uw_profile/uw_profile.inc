<?php
/**
 * Implements hook_panels_layouts().
 */
function f1ux_uw_profile_panels_layouts() {
  $items['uw_profile'] = array(
    'title' => t('UW Profile'),
    'category' => t('f1ux'),
    'icon' => 'uw_profile.png',
    'theme' => 'uw_profile',
    'admin css' => '../../../css/panels-admin/uw-profile.css',
    'regions' => array(
      'preface' => t('Preface'),
      'sidebar' => t('Sidebar'),
      'main' => t('Main column'),
      'postscript' => t('Postscript'),
    ),
  );

  return $items;
}
