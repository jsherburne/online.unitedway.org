<?php
/**
 * Implements hook_panels_layouts().
 */
function f1ux_sidebar_right_panels_layouts() {
  $items['sidebar_right'] = array(
    'title' => t('Preface and right sidebar [DEPRECATED]'),
    'category' => t('f1ux'),
    'icon' => 'sidebar_right.png',
    'theme' => 'sidebar_right',
    'admin css' => '../../../css/panels-admin/sidebar-right.css',
    'regions' => array(
      'preface' => t('Preface'),
      'main' => t('Main column'),
      'sidebar' => t('Sidebar'),
    ),
  );

  return $items;
}