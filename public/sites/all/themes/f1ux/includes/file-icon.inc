<?php
/**
 * @file
 * File icon-related functions.
 */

/**
 * Returns HTML for the select file icons.
 */
function f1ux_file_link($vars) {
  $file = $vars['file'];
  $icon_directory = $vars['icon_directory'];
  $url = file_create_url($file->uri);
  $icon = theme('file_icon', array('file' => $file, 'icon_directory' => $icon_directory));
  
  $allowable_ext = array('doc', 'docx', 'xls', 'xlsx', 'ppt', 'pptx', 'pdf');
  $pieces = explode('.', $url);
  $last = end($pieces);
  if (in_array($last, $allowable_ext)) {
    $icon = '<div class="icon ' . $last .'"></div>';
  }
  
  $options = array(
    'attributes' => array(
       'type' => $file->filemime . '; length=' . $file->filesize,
     ),
  );
  if (empty($file->description)) {
    $link_text = $file->filename;
  } else {
    $link_text = $file->description;
    $options['attributes']['title'] = check_plain($file->filename);
  }
  return '<div class="file">' . $icon . ' ' . l($link_text, $url, $options) . '</div>';
}