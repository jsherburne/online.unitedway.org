// Custom scripts file
// to load, uncomment the call to this file in f1ux.info

(function ($) {

  Drupal.behaviors.expandingformatterfix = {
    attach: function (context) {

      //Get height of expander
      var expanderheight = $('.expanding-formatter').height();


      // Set min-height
      $('.expanding-formatter').css('min-height', expanderheight).css('height', 'auto');
    }
  };

  // Update followers count.
  $(document).bind('flagGlobalAfterLinkUpdate', function (event, data) {
    if (data.flagName == 'commons_follow_user') {
      var $folowers = $('.field-name-user-follow-count > .field:first > span');
      var counter = $folowers.html();
      if(data.flagStatus == 'flagged') {
        counter++;
      }
      else {
        counter--;
      }

      $folowers.html(counter);
    }
  });
})(jQuery);
