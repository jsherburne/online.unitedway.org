<?php
/**
 * @file
 * Theme implementation to provide an HTML container for comments.
 */

// Render the comments and form first to see if we need headings.
$comments = render($content['comments']);
$comment_form = render($content['comment_form']);
?>
<section<?php print $attributes; ?>>
  <div class="comment-count-nolink"><?php print $comment_count; ?></div>
  <?php if ($node->type != 'forum'): ?>
    <?php print render($title_prefix); ?>
    <h2<?php print $title_attributes ?>><?php print t('Comments'); ?></h2>
    <?php print render($title_suffix); ?>
  <?php endif; ?>

  <?php if ($comments): ?>
  <?php print $comments; ?>
  <?php else: ?>
  <p>No comments yet, be the first to comment below!</p>
  <?php endif; ?>

  <?php if ($comment_form): ?>
    <h2<?php print $form_title_attributes ?>><?php print t('Add new comment'); ?></h2>
    <div class="comments-wrapper"><?php print $comment_form; ?></div>
  <?php endif; ?>
</section>
