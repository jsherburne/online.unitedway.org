<?php
$plugin = array(
  'title'     => t('Commons Origins site template'),
  'theme'     => 'commons_origins_site_template',
  'icon'      => 'commons_origins_site_template.png',
  'admin css' => 'commons_origins_site_template.css',
  'category'  => 'Commons Origins',
  'regions' => array(
    'sidebar_first'     => t('Sidebar first'),
    'sidebar_second'    => t('Sidebar second'),
    'content'           => t('Main content'),
    'highlighted'       => t('Highlighted'),
    'content_aside'     => t('Aside'),
    'secondary_content' => t('Secondary'),
    'tertiary_content'  => t('Tertiary'),
    'footer'            => t('Footer'),
    'leaderboard'       => t('Leaderboard'),
    'header'            => t('Header'),
    'menu_bar_first'    => t('Menu Bar first'),
    'menu_bar_second'   => t('Menu Bar second'),
    'help'              => t('Help'),
  ),
);
