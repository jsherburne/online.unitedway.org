<div class="header-wrapper">
  <div class="header-bar">
    <hgroup class="container connect">
      <?php if (!empty($content['header'])): ?>
        <?php print render($content['header']); ?>
      <?php endif; ?>

    </hgroup>
  </div>
  <div class="menu-bar-first">
      <?php if (!empty($content['menu_bar_first'])): ?>
        <div id="menu-bar-first" class="nav container clearfix">
          <?php print render($content['menu_bar_first']); ?>
        </div>
      <?php endif; ?>
	</div>
  <div class="menu-bar-second">
      <?php if (!empty($content['menu_bar_second'])): ?>
        <div id="menu-bar-second" class="nav container clearfix">
          <?php print render($content['menu_bar_second']); ?>
        </div>
      <?php endif; ?>
	</div>
</div>
<div<?php print $css_id ? " id=\"$css_id\"" : ''; ?> class="container <?php print $classes; ?>">

  <div id="columns" class="columns clearfix">
    <?php if (!empty($content['help'])): ?>
      <div class="region region-help">
        <?php print render($content['help']); ?>
      </div>
    <?php endif; ?>


    <div id="content-column" class="content-column" role="main">
      <div class="content-inner">

        <!-- region: Highlighted -->
        <?php if (!empty($content['highlighted'])): ?>
          <div class="region region-highlighted">
            <?php print render($content['highlighted']); ?>
          </div>
        <?php endif; ?>

        <?php if (!empty($content['secondary_content'])): ?>
          <div class="region region-secondary-content">
            <?php print render($content['secondary_content']); ?>
          </div>
        <?php endif; ?>

          <!-- region: Main Content -->
          <div id="content" class="region region-content">
            <?php print render($content['content']); ?>
          </div>



        <!-- region: Content Aside -->
        <?php if (!empty($content['content_aside'])): ?>
          <div class="region region-content-aside">
            <?php print render($content['content_aside']); ?>
          </div>
        <?php endif; ?>

      </div><!-- /end .content-inner -->
    </div><!-- /end #content-column -->

    <!-- regions: Sidebar first and Sidebar second -->
    <?php if (!empty($content['sidebar_first'])): ?>
      <div class="region region-sidebar-first sidebar">
        <?php print render($content['sidebar_first']); ?>
      </div>
    <?php endif; ?>

    <?php if (!empty($content['sidebar_second'])): ?>
      <div class="region region-sidebar-second sidebar">
        <?php print render($content['sidebar_second']); ?>
      </div>
    <?php endif; ?>
  </div><!-- /end #columns -->

  <!-- region: Tertiary Content -->
  <?php if (!empty($content['tertiary_content'])): ?>
    <div class="region region-tertiary-content">
      <?php print render($content['tertiary_content']); ?>
    </div>
  <?php endif; ?>

</div>
<div class="footer-wrapper">
	<!-- region: Footer -->
	<?php if (!empty($content['footer'])): ?>
		<!--<footer id="footer" role="contentinfo" class="container">-->
		<div>
			<?php print render($content['footer']); ?>
		</div>
		<!--</footer>-->
	<?php endif; ?>
</div>