#!/bin/bash

VAGRANT_CORE_FOLDER="/vagrant"

if [[ -f "${VAGRANT_CORE_FOLDER}/public/sites/default/settings.vm.php" ]]; then
  if [[ ! -f "${VAGRANT_CORE_FOLDER}/public/sites/default/settings.php" || -w "${VAGRANT_CORE_FOLDER}/public/sites/default/settings.php" ]]; then
    echo 'Copying settings file'
    cd ${VAGRANT_CORE_FOLDER}/public/sites/default && cp settings.vm.php settings.php >/dev/null
  fi
fi

if [[ -f "${VAGRANT_CORE_FOLDER}/public/htaccess.dev" ]]; then
  echo 'Copying .htaccess'
  cd ${VAGRANT_CORE_FOLDER}/public && cp htaccess.dev .htaccess > /dev/null
fi

if [[ ! -d "/home/vagrant/.drush" ]]; then
  echo 'Creating drush directory'
  mkdir ~/.drush
fi

# echo 'Copying aliases'
# find "${VAGRANT_CORE_FOLDER}" -maxdepth 1 -name "*.aliases.drushrc.php" -exec su - vagrant -c "cp {} ~/.drush/" \;

if [[ ! -f "/home/vagrant/.drush/drush.ini" ]]; then
  echo 'Creating drush settings'
  echo 'memory_limit = 512M' > ~/.drush/drush.ini
fi


if [[ ! -f "/home/vagrant/.s3cfg" ]]; then
  echo 'Creating s3cfg settings'
  cp ${VAGRANT_CORE_FOLDER}/s3cfg /home/vagrant/.s3cfg
fi

#Let's just forget about the whole database stuff until we figure out why
#drush aliases aren't working in vagrant
#TODO: Figure out why drush aliases for stage and live don't work in vagrant anymore

#echo 'Downloading database'
#if [[ -f "/home/vagrant/live.dump.sql" ]]; then
#  rm /home/vagrant/live.dump.sql
#fi
#if [[ -f "/home/vagrant/dump.sql" ]]; then
#  rm /home/vagrant/dump.sql
#fi
#cd ${VAGRANT_CORE_FOLDER}/public && drush @uwo.live sql-dump > /home/vagrant/live.dump.sql
#removing string `tput: unknown terminal "unknown"` from first line of sql dump
#tail -n +2 /home/vagrant/live.dump.sql > /home/vagrant/dump.sql
#echo 'Installing database'
#drush sqlc < /home/vagrant/dump.sql
#rm /home/vagrant/live.dump.sql
#rm /home/vagrant/dump.sql

#s3cmd get s3://uwonline/uwonline.stage.sql.gz /home/vagrant/uwonline.stage.sql.gz --force
#gunzip /home/vagrant/uwonline.stage.sql.gz --force
#echo 'Installing database'
#cd ${VAGRANT_CORE_FOLDER}/public && drush sqlc < /home/vagrant/uwonline.stage.sql