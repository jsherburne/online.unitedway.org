#!/bin/bash

VAGRANT_CORE_FOLDER="/vagrant"

yum -y --enablerepo epel-testing install s3cmd

# simulate live environment absolute paths
ln -fns $VAGRANT_CORE_FOLDER /var/www/current

ln -fns $VAGRANT_CORE_FOLDER/private /var/www/private
ln -fns $VAGRANT_CORE_FOLDER/private /var/www/private/simplesaml_session_db

#use our version of vagrant.conf for nginx
echo 'Using  /vagrant/server_config/nginx/vagrant.conf'
ln -fns $VAGRANT_CORE_FOLDER/server_config/nginx/vagrant.conf /etc/nginx/conf.d/vagrant.conf

#use our version of default.vcl for varnish
echo 'Using /vagrant/server_config/varnish/default.vm.vcl'
ln -fns $VAGRANT_CORE_FOLDER/server_config/varnish/default.vm.vcl /etc/varnish/default.vcl
ln -fns $VAGRANT_CORE_FOLDER/puppet/manifests/files/varnish/secret /etc/varnish/secret

#use our version of limits.conf for /etc/security
echo 'Using /vagrant/server_config/security/limits.conf'
#ln -fns $VAGRANT_CORE_FOLDER/server_config/security/limits.conf /etc/security/limits.conf
if [[ ! -f "/etc/security/limits.d/vagrant.limits.conf" ]]; then
  echo 'Creating vagrant.limits.conf file'
  cp $VAGRANT_CORE_FOLDER/server_config/security/limits.conf /etc/security/limits.d/vagrant.limits.conf
fi

#install xhprof
echo 'Install xhprof'
yum install php54-devel.x86_64 -y
pecl install xhprof-beta
yum install graphviz -y

if [[ ! -f "/etc/php.d/xhprof.ini" ]]; then
  echo 'Creating xhprof.ini file'
  echo '[xhprof]' > /etc/php.d/xhprof.ini
  echo 'extension=xhprof.so' >> /etc/php.d/xhprof.ini
  echo 'xhprof.output_dir="/var/tmp/xhprof"' >> /etc/php.d/xhprof.ini 
fi

if [[ ! -d "/var/tmp/xhprof" ]]; then
  echo 'Creating xhprof directory'
  mkdir /var/tmp/xhprof
  chmod 777 /var/tmp/xhprof
fi

#service nginx restart
#service varnish restart
#service memcached restart
#service php-fpm restart
